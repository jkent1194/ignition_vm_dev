
# -----------------------------------------------------------------------------------------------------------------------
#
# Creates a new unique string that can be used as a unique identifier
#
# -----------------------------------------------------------------------------------------------------------------------
def generateUid():

	from java.util import UUID
	uid = UUID.randomUUID().toString()

	return uid
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Creates a new event log dictionary object
#
# -----------------------------------------------------------------------------------------------------------------------
def createEventLogObject(eventSource):

	# create a standard event log object for use in a script
	eventLogObj = {}
	
	eventLogObj['event_source'] = str(eventSource)
	eventLogObj['input_values'] = ''
	eventLogObj['output_values'] = ''
	eventLogObj['script_output'] = ''
	eventLogObj['event_result'] = 'ERROR'
	eventLogObj['event_duration'] = ''
	
	return eventLogObj