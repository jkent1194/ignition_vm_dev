


# -----------------------------------------------------------------------------------------------------------------------
#
# <Aruguments> 	- newEventObj <dictionary> 	- 	a dictionary object containing the necessary properties of the new event to
#												create in teh database
#
# <Returns>		- success <bool> 			- 	returns a true or false result indicating a successful insert
#
# -----------------------------------------------------------------------------------------------------------------------
def insertNewEvent(newEventObj):

	result = False
	resultData = {}
	
	database = 'MANUFACTURING_DATA'
	
	# create a unique identifier for this event log record
	generated_UID = shared.ent.event_logging.db_sql.events.generateUid()
	
	# prepare the query that will insert the new event record
	preparedQuery = """
	
	
		INSERT INTO [dbo].[ent_event_log_t]
			   (
				   [event_id]
				   ,[event_date]
				   ,[event_source]
				   ,[input_values]
				   ,[output_values]
				   ,[script_output]
				   ,[event_result]
				   ,[event_duration]
			   )
		 VALUES
			   (
				   '""" + generated_UID + """'
				   , GETDATE()
				   ,'""" + str(newEventObj['event_source']) + """'
				   ,'""" + str(newEventObj['input_values']) + """'
				   ,'""" + str(newEventObj['output_values']) + """'
				   ,'""" + str(newEventObj['script_output']) + """'
				   ,'""" + str(newEventObj['event_result']) + """'
				   ,'""" + str(newEventObj['event_duration']) + """'
			   )
					
	"""
		
	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)
		
		result = True
		
	except:
	
		print "Insert new event log record failed to execute."
		
		print preparedQuery
			
	return result	





# -----------------------------------------------------------------------------------------------------------------------
#
#	Purges the old event log records from the database.  
#
# <Returns>		- success <bool> 			- 	returns a true or false result indicating a successful processing
#
# -----------------------------------------------------------------------------------------------------------------------
def purgeOldEvents():

	result = ''
	resultData = {}
	
	database = 'MANUFACTURING_DATA'
	
	# prepare the query that will insert the new event record
	preparedQuery = """
	
		DELETE 
		FROM [dbo].[ent_event_log_t]
		WHERE DATEDIFF(DAY, event_date, GETDATE()) > 30
					
	"""
		
	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)
		
		result = True
		
	except:
	
		print "Event log purging failed to execute."
		
		print preparedQuery
			
	return result	





# -----------------------------------------------------------------------------------------------------------------------
#
# Creates a new unique string that can be used as a unique identifier
#
# -----------------------------------------------------------------------------------------------------------------------
def generateUid():

	from java.util import UUID
	uid = UUID.randomUUID().toString()

	return uid
