

# -------------------------------------------------------------------------------------------------------	
# deletes all old shift period and idle period records form the database that are older than 31 days
# -------------------------------------------------------------------------------------------------------	
def clearOldShiftPeriodRecords():

	deleteShiftsResult = shared.ent.shifts.db_sql.shifts.deleteOldShifts()
	deleteOldIdlePeriodsResult = shared.ent.shifts.db_sql.shift_idle_periods.deleteOldShiftIdlePeriods()
	
	if deleteShiftsResult == False:
	
		print "failed to delete old shift period records"
		
	if deleteOldIdlePeriodsResult == False:
	
		print "failed to delete old shift idle period records"