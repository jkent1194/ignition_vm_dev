import string
import sys
import java.lang.Exception as jException
import shared.ent.assets.interface.expected_completions as expectedCompletions
import shared.ent.shifts.db_sql.shifts as db
import shared.ent.assets.interface.OEE as OEE
import shared.ent.common.locks.interface as lock


LINE_LOCK_PREFIX = "shiftGen_line_"
FUTURE_SHIFT_DAYS = 5 # N.B. a value of 1 will only generate the current day's shifts; recommended value >= 3


# add shift encoded time to date
def addTime(date, time):
	parts = string.split(time, ":")
	hours = int(parts[0])
	minutes = int(parts[1])
	return system.date.addHours(system.date.addMinutes(date, minutes), hours)
	

# check if two shifts are the same in all values
def sameShift(a, b):
	log = system.util.getLogger("shift_generation")

	# number of keys doesn't match
	if len(a) != len(b):
		return False
	
	try:
		# check each key
		for x in a:
		
			# keys don't match
			if x not in b:
				log.info("diff:  " + str(x) + " not in b")
				return False
			
			# types don't match
	#		if type(a[x]) != type(b[x]):
	#			log.info("diff:  " + str(x) + " is type " + str(type(a[x]))  + " in a. " + str(x) + " is type " + str(type(b[x]))  + " in b.")
	#			return False
			
			# check dictionary recursively (idle periods)
			if type(a[x]) == dict:
				if not sameShift(a[x], b[x]):
					return False
			
			# values don't match
			elif a[x] != b[x]:
				log.info("diff:  " + str(a[x]) + "  vs  " + str(b[x]))
				return False
	
	except:
		return False
	
	# all possible differences checked
	return True


def getOldLineShiftPeriods(lineID, currentTime):

	query = """
	SELECT 
		P.shift_date as shift_date,
		P.shift_code as shift_code,
		P.overtime_start_date as overtime_start_date,
		P.start_date as p_start_date,
		P.end_date as p_end_date,
		P.overtime_end_date as overtime_end_date,
		P.day_of_week as day_of_week,
		P.enabled as p_enabled,
		IP.start_date as ip_start_date,
		IP.end_date as ip_end_date,
		IP.period_id as period_id,
		IP.enabled as ip_enabled
	FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_periods_t] P
		LEFT JOIN [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_idle_periods_t] IP
			ON P.line_id = IP.line_id
				AND P.shift_date = IP.shift_date
				AND P.shift_code = IP.shift_code
				--AND (IP.end_date >= ? OR IP.enabled = 'False')
	WHERE
		P.line_id = ?
		AND P.shift_date >= CAST(? AS DATE)
		AND P.end_date >= ?
	ORDER BY	
		P.shift_date ASC,
		P.shift_code ASC,
		IP.period_id ASC
	"""

	result = system.db.runPrepQuery(query, database = "MANUFACTURING_DATA", args = [lineID, currentTime, currentTime])

	currentDate = None
	currentShift = None

	i = -1
	shifts = []

	for rowIndex in range(result.getRowCount()):

		if currentDate != result.getValueAt(rowIndex, "shift_date") or currentShift != result.getValueAt(rowIndex, "shift_code"):
			i += 1
			shifts.append({"shift_date":system.date.parse(result.getValueAt(rowIndex, "shift_date"), "yyyy-MM-dd"),
						   "shift_code":result.getValueAt(rowIndex, "shift_code"),
						   "overtime_start_date":system.date.parse(system.date.format(result.getValueAt(rowIndex, "overtime_start_date"), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss"),
						   "start_date":system.date.parse(system.date.format(result.getValueAt(rowIndex, "p_start_date"), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss"),
						   "end_date":system.date.parse(system.date.format(result.getValueAt(rowIndex, "p_end_date"), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss"),
						   "overtime_end_date":system.date.parse(system.date.format(result.getValueAt(rowIndex, "overtime_end_date"), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss"),
						   "day_of_week":result.getValueAt(rowIndex, "day_of_week"),
						   "enabled":result.getValueAt(rowIndex, "p_enabled"),
						   "idle_periods":{}})
			currentDate = result.getValueAt(rowIndex, "shift_date")
			currentShift = result.getValueAt(rowIndex, "shift_code")
			
		if result.getValueAt(rowIndex, "period_id") is not None:
			shifts[i]["idle_periods"][result.getValueAt(rowIndex, "period_id")] = {"start_date":system.date.parse(system.date.format(result.getValueAt(rowIndex, "ip_start_date"), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss"),
																				   "end_date":system.date.parse(system.date.format(result.getValueAt(rowIndex, "ip_end_date"), "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss"),
																				   "enabled":result.getValueAt(rowIndex, "ip_enabled")}
	
	return shifts


def getNewLineShiftPeriods(lineID, currentTime):

	definitionQuery = """
	SELECT json_definition
	FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_definitions_t] DEF
		JOIN [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_line_shift_associations_t] LSA 
			ON DEF.id = LSA.shift_def_id
	WHERE
		LSA.line_id = ?
	"""

	shiftDefDS = system.db.runPrepQuery(definitionQuery, database = "MANUFACTURING_DATA", args = [lineID])
	shiftDef = None

	if shiftDefDS.getRowCount() > 0:
		shiftDef = system.util.jsonDecode(shiftDefDS.getValueAt(0,0))
	else:
		return []
		
	i = 0
	newShifts = []
	
	# create shifts for the next 14 days
	for date in [system.date.midnight(system.date.addDays(currentTime, x)) for x in range(FUTURE_SHIFT_DAYS)]:
		
		# get definition for respective day
		dayDef = shiftDef["schedule"]["days_of_week"][str(system.date.getDayOfWeek(date))]
#		print(date)

		# generate all 3 shifts
		for dayCode in range(3):
	#		print(dayCode)
	#		print(dayDef)
	
			# don't add completed shifts
			if addTime(date, dayDef["shifts"][str(dayCode)]["shift_end"]) > currentTime:
			
				newShifts.append({"shift_date":date,
								  "shift_code":dayCode,
								  "overtime_start_date":addTime(date, dayDef["shifts"][str(dayCode)]["overtime_start"]),
								  "start_date":addTime(date, dayDef["shifts"][str(dayCode)]["shift_start"]),
								  "end_date":addTime(date, dayDef["shifts"][str(dayCode)]["shift_end"]),
								  "overtime_end_date":addTime(date, dayDef["shifts"][str(dayCode)]["overtime_end"]),
								  "day_of_week":system.date.getDayOfWeek(date),
								  "enabled":str(dayDef["shifts"][str(dayCode)]["enabled"]) == "True" and str(dayDef["enabled"]) == "True",
								  "idle_periods":{}})
								  
				# account for 3rd shift inter-day funny business
				if newShifts[i]["start_date"] > newShifts[i]["end_date"]:
					newShifts[i]["start_date"] = system.date.addDays(newShifts[i]["start_date"], -1)
				if newShifts[i]["overtime_start_date"] > newShifts[i]["end_date"]:
					newShifts[i]["overtime_start_date"] = system.date.addDays(newShifts[i]["overtime_start_date"], -1)
					
				# generate idle periods
				for idleID in dayDef["shifts"][str(dayCode)]["idle_periods"]:
					
					idleEnabled = str(dayDef["shifts"][str(dayCode)]["idle_periods"][idleID]["enabled"]) == "True" and newShifts[i]["enabled"]
					idleStart = addTime(date, dayDef["shifts"][str(dayCode)]["idle_periods"][idleID]["period_start"])
					idleEnd = addTime(date, dayDef["shifts"][str(dayCode)]["idle_periods"][idleID]["period_end"])
					
					# account for 3rd shift inter-day funny business
					if idleEnabled and idleStart > newShifts[i]["end_date"]:
						idleStart = system.date.addDays(idleStart, -1)
					if idleEnabled and idleEnd > newShifts[i]["end_date"]:
						idleEnd = system.date.addDays(idleEnd, -1)
						
#					print(str(idleEnabled) + "  " + str(idleStart))
					# ignore idle periods that are enabled and completed
#					if not idleEnabled or idleEnd > currentTime:
					newShifts[i]["idle_periods"][int(idleID)] = {"start_date":idleStart,
																 "end_date":idleEnd,
																 "enabled":idleEnabled}
				# next shift index
				i += 1
				
	return newShifts


def getShiftIUD(newShifts, oldShifts):
	
	log = system.util.getLogger("shift_generation")
	
	insertShifts = []
	updateShifts = []
	deleteShifts = []
	
	newI = 0
	oldI = 0
	
	while newI < len(newShifts) or oldI < len(oldShifts):
		
		#log.info("diff: " + str(newShifts[newI]) + "  VS  " + oldShifts[oldI])
		
		if oldI >= len(oldShifts):
			insertShifts.append(newShifts[newI])
			newI += 1
		
		elif newI >= len(newShifts):
			deleteShifts.append(oldShifts[oldI])
			oldI += 1
		
		elif newShifts[newI]['shift_date'] > oldShifts[oldI]['shift_date']:
			deleteShifts.append(oldShifts[oldI])
			oldI += 1
		
		elif newShifts[newI]['shift_date'] < oldShifts[oldI]['shift_date']:
			insertShifts.append(newShifts[newI])
			newI += 1
			
		else:
			if newShifts[newI]['shift_code'] > oldShifts[oldI]['shift_code']:
				deleteShifts.append(oldShifts[oldI])
				oldI += 1
				
			elif newShifts[newI]['shift_code'] < oldShifts[oldI]['shift_code']:
				insertShifts.append(newShifts[newI])
				newI += 1
				
			else:
				if not sameShift(newShifts[newI], oldShifts[oldI]):
					updateShifts.append({"new":newShifts[newI], "old":oldShifts[oldI]})
					
				oldI += 1
				newI += 1
				
	return {"insert":insertShifts,
			"update":updateShifts,
			"delete":deleteShifts}


# done = False
# diff = []

# while not done:
	# if len(shifts) == 0 and len(newShifts) == 0:
		# done = True 
	# elif len(shifts) == 0:
		# diff.append(newShifts.pop(0))
	# elif len(newShifts) == 0:
		# diff.append(shifts.pop(0))
	# else:
		# if shifts[0]["shift_date"] == newShifts[0]["shift_date"] and shifts[0]["shift_code"] == newShifts[0]["shift_code"]:
			# if not sameShift(shifts[0], newShifts[0]):
				# diff.append(newShifts.pop(0))
				# a = shifts.pop(0)
			# else:
				# a = newShifts.pop(0)
				# a = shifts.pop(0)
		# elif shifts[0]["shift_date"] > newShifts[0]["shift_date"]:
			# print "a"
			# print(str(shifts[0]["shift_date"]) + "  vs  " + str(newShifts[0]["shift_date"]))
			# diff.append(newShifts.pop(0))
		# elif shifts[0]["shift_date"] < newShifts[0]["shift_date"]:
			# print "b"
			# print(str(shifts[0]["shift_date"]) + "  vs  " + str(newShifts[0]["shift_date"]))
			# diff.append(shifts.pop(0))
		# elif shifts[0]["shift_code"] > newShifts[0]["shift_code"]:
			# print "c"
			# diff.append(newShifts.pop(0))
		# elif shifts[0]["shift_code"] < newShifts[0]["shift_code"]:
			# print "d"
			# diff.append(shifts.pop(0))


def executeShiftIUD(lineID, insert, update, delete, taktTimes):
	
	log = system.util.getLogger("shift_generation")
	st = None
	et = None
	
	for d in delete:
		st = system.date.now()
		log.info("starting delete: lineID=" + str(lineID) + " shift_date=" + str(d['shift_date']) + " shift_code=" + str(d['shift_code']) + 
						 "  started: " + str(st))
		updateShift(lineID, oldShift = d)
		et = system.date.now()
		log.info("delete: lineID=" + str(lineID) + " shift_date=" + str(d['shift_date']) + " shift_code=" + str(d['shift_code']) + 
				 "  started: " + str(st) + "  ended: " + str(et))
	
	for u in update:
		st = system.date.now()
		log.info("update: lineID=" + str(lineID) + " shift_date=" + str(u['new']['shift_date']) + " shift_code=" + str(u['new']['shift_code']) + 
				 "  started: " + str(st))
		updateShift(lineID, newShift = u['new'], oldShift = u['old'], taktTimes = taktTimes)
		et = system.date.now()
		log.info("update: lineID=" + str(lineID) + " shift_date=" + str(u['new']['shift_date']) + " shift_code=" + str(u['new']['shift_code']) + 
				 "  started: " + str(st) + "  ended: " + str(et))
	
	for i in insert:
		st = system.date.now()
		log.info("insert: lineID=" + str(lineID) + " shift_date=" + str(i['shift_date']) + " shift_code=" + str(i['shift_code']) + 
				 "  started: " + str(st))
		updateShift(lineID, newShift = i, taktTimes = taktTimes)
		et = system.date.now()
		log.info("insert: lineID=" + str(lineID) + " shift_date=" + str(i['shift_date']) + " shift_code=" + str(i['shift_code']) + 
				 "  started: " + str(st) + "  ended: " + str(et))


def updateShift(lineID, newShift = None, taktTimes = None, oldShift = None):
	
	log = system.util.getLogger("shift_generation")
	
	tx = system.db.beginTransaction(database = "MANUFACTURING_ASSETS")
	
	try:
		if oldShift is not None:
			if oldShift["enabled"]:
				# delete old shift expected completions
				returnCode = expectedCompletions.deleteShiftExpectedCompletions(lineID, None, None, oldShift, tx)
				if returnCode:
					log.error("Error deleting old expected completions")
					raise Exception
				
		#else:
		if newShift is not None and newShift["enabled"]:
			# delete possible conflicting expected completions
			returnCode = expectedCompletions.deleteShiftExpectedCompletions(lineID, None, None, newShift, tx)
			if returnCode:
				log.error("Error deleting new expected completions")
				raise Exception

		if oldShift is not None:
			# delete old shift
			returnCode = db.deleteShiftPeriods(lineID, oldShift['shift_date'], oldShift['shift_code'], tx)
			if returnCode:
				log.error("Error deleting old shift")
				raise Exception
			
		# insert new shift
		if newShift is not None:
			returnCode = db.insertShiftPeriods(lineID, newShift, tx)
			if returnCode:
				log.error("Error inserting new shift")
				raise Exception
		
		# generate Expected Completions
		if newShift is not None:
			returnCode = expectedCompletions.generateShiftExpectedCompletions(lineID, None, None, newShift, taktTimes, tx)
			if returnCode:
				log.error("Error inserting new expected completions")
				raise Exception
			
		system.db.commitTransaction(tx)
	
	except Exception, e:
		if tx is not None:
			system.db.rollbackTransaction(tx)
		log.warn("Exception in updateShift")
		err = sys.exc_info()
		#log.warn(' | '.join(err))
		log.warn(str(err[0]) + ' | ' + str(err[1]))
	except jException, e:
		if tx is not None:
			system.db.rollbackTransaction(tx)
		log.warn("jException in updateShift")
		log.warn(str(e.cause) + "   " + str(e))
		
	finally:
		if tx is not None:
			system.db.closeTransaction(tx)
		

def getLineTaktTimes(lineID):
	
	oeeCells = expectedCompletions.getLineOEECells(lineID)
	
	taktTimes = {}
	
	for oeeCell in oeeCells:
		taktTimes[oeeCell['cell_id']] = OEE.getCellTaktTime(oeeCell['cell_id'])
		
	return taktTimes


def generateLineShifts(lineID):
	
	log = system.util.getLogger("shift_generation")
	
	lockOwner = "generateLineShifts"
	code = lock.getResourceLock(LINE_LOCK_PREFIX + str(lineID),
								lockOwner,
								uniqueOwner = False,
								createResource = True,
								retry = True,
								retryDelayInit = 10,
								retryDelayFactor = 10,
								retryDelayGrowth = "add",
								retryDelayMax = 60,
								retryLimit = 10)
	
	if code >= 0:
		try:
			log.info("starting shift generation for line " + str(lineID))
			
			currentTime = system.date.now()
			
			newShifts = getNewLineShiftPeriods(lineID, currentTime)
		#			log.info("line " + str(lineID) + " new shifts: " + str(newShifts))
			
			oldShifts = getOldLineShiftPeriods(lineID, currentTime)
		#			log.info("line " + str(lineID) + " new shifts: " + str(oldShifts))
			
			diff = getShiftIUD(newShifts, oldShifts)
			log.info("line " + str(lineID) + " diff shifts: " + str(diff))
			
			taktTimes = getLineTaktTimes(lineID)
			
			executeShiftIUD(lineID,
							insert = diff['insert'],
							update = diff['update'],
							delete = diff['delete'],
							taktTimes = taktTimes)
							
			for cell in taktTimes:
				expectedCompletions.rebuildFutureShiftExpectedCompletions(cell, taktTimes[cell])
			
		finally:
			lock.releaseResourceLock(LINE_LOCK_PREFIX + str(lineID), lockOwner)
	
	else:
		log.warn("could not obtain shift generation lock for line " + str(lineID))


