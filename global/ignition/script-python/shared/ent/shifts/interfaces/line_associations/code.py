# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 04/15/2020
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	


import shared.ent.shifts.db_sql.line_associations as associations

	
	
# -----------------------------------------------------------------------------------------------------------------------
#
#	Creates/updates an association between a line and a shift definition
#
# -----------------------------------------------------------------------------------------------------------------------
def upsertLineAssociation(lineId, shiftDefId, updatedBy):

	result = associations.upsertLineAssociation(lineId, shiftDefId, updatedBy)
	
	return result
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all shift definition records that are associated with at least one asset in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllAssociations(outputType = '1'):

	result, resultData = associations.getAllAssociations(outputType)
			
	return result, resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all shift definition records that are associated with at least one asset in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllAssociations_noDiag(outputType = '1'):

	result, resultData = associations.getAllAssociations(outputType)
			
	return resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all lines associated with a shift definition record in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getLinesByShiftDef(shiftDefId, outputType = '1'):

	result, resultData = associations.getLinesByShiftDef(shiftDefId, outputType)
			
	return result, resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all lines associated with a shift definition record in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getLinesByShiftDef_noDiag(shiftDefId, outputType = '1'):

	result, resultData = associations.getLinesByShiftDef(shiftDefId, outputType)
			
	return resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up a shift definition record by an associated line in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getShiftDefByLine(lineId, outputType = '1'):

	result, resultData = associations.getShiftDefByLine(lineId, outputType)

	return result, resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up a shift definition record by an associated line in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getShiftDefByLine_noDiag(lineId, outputType = '1'):

	result, resultData = associations.getShiftDefByLine(lineId, outputType)

	return resultData



# -----------------------------------------------------------------------------------------------------------------------
#
#	Deletes an association between a line and a shift definition, by line id
#
# -----------------------------------------------------------------------------------------------------------------------
def deleteAssociationByLine(lineId):

	result = associations.deleteAssociationByLine(lineId)
			
	return result



# -----------------------------------------------------------------------------------------------------------------------
#
#	Deletes all associations between lines and a shift definition, by shift def id
#
# -----------------------------------------------------------------------------------------------------------------------
def deleteAssociationsByShiftDef(shiftDefId):

	result = associations.deleteAssociationsByShiftDef(shiftDefId)
			
	return result



# -----------------------------------------------------------------------------------------------------------------------
#
# Checks to see if a shift definition is associated with any lines the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def checkIfUsed_ShiftDef(shiftDefId):

	querySuccessful, result = associations.checkIfUsed_ShiftDef(shiftDefId)
			
	return querySuccessful, result




# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all lines that are NOT associated with a shift definition record in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getLinesWithoutShiftDefs(outputType = '1'):

	result, resultData = associations.getLinesWithoutShiftDefs(outputType)
		
	return result, resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all lines that are NOT associated with a shift definition record in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getLinesWithoutShiftDefs_noDiag(outputType = '1'):

	result, resultData = associations.getLinesWithoutShiftDefs(outputType)
		
	return resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all lines in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllLines(outputType = '1'):

	result, resultData = shared.ent.shifts.db_sql.line_associations.getAllLines(outputType)
			
	return result, resultData
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all lines in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllLines_noDiag(outputType = '1'):

	result, resultData = shared.ent.shifts.db_sql.line_associations.getAllLines(outputType)
			
	return resultData


