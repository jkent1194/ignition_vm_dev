




# -----------------------------------------------------------------------------------------------------------------------
#
# Get all sites from database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllSites(outputType = '1'):

	result, resultData = shared.ent.assets.db_sql.sites.getAllSites(outputType)
	
	return result, resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Get all sites from database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllSites_noDiag(outputType = '1'):

	result, resultData = shared.ent.assets.db_sql.sites.getAllSites(outputType)
	
	return resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Get all areas from database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllAreasBySite(siteId, outputType = '1'):

	result, resultData, shared.ent.assets.db_sql.areas.getAllAreasBySite(siteId, outputType)
	
	return result, resultData
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Get all areas from database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllAreasBySite_noDiag(siteId, outputType = '1'):

	result, resultData = shared.ent.assets.db_sql.areas.getAllAreasBySite(siteId, outputType)
	
	return resultData
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Get all lines from database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllLinesByArea(areaId, outputType = '1'):

	result, resultData = shared.ent.assets.db_sql.lines.getAllLinesByArea(areaId, outputType)
	
	return result, resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Get all lines from database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllLinesByArea_noDiag(areaId, outputType = '1'):

	result, resultData = shared.ent.assets.db_sql.lines.getAllLinesByArea(areaId, outputType)
	
	return resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Get all cells from database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllCellsByLine(lineId, outputType = '1'):

	result, resultData = shared.ent.assets.db_sql.cells.getAllCellsByLine(lineId, outputType)
	
	return result, resultData
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Get all cells from database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllCellsByLine_noDiag(lineId, outputType = '1'):

	result, resultData = shared.ent.assets.db_sql.cells.getAllCellsByLine(lineId, outputType)
	
	return resultData