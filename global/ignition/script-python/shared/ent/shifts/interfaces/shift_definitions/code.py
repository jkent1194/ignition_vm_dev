# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 09/20/2019
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	



import shared.ent.shifts.db_sql.shift_definitions as shiftDefs
	
	
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up a shift definition record by it's id in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getShiftDefById(shiftDefId):

	result, recordDictObj = shiftDefs.getShiftDefById(shiftDefId)
			
	return result, recordDictObj



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all shift definition records in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllShiftDefs(outputType = '1'):

	result, resultData = shiftDefs.getAllShiftDefs(outputType)
			
	return result, resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all shift definition records in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllShiftDefs_noDiag(outputType = '1'):

	result, resultData = shiftDefs.getAllShiftDefs(outputType)
			
	return resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all shift definition names in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllShiftDefNames(outputType = '1'):

	result, resultData = shiftDefs.getAllShiftDefNames(outputType)
			
	return result, resultData
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all shift definition names in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllShiftDefNames_noDiag(outputType = '1'):

	result, resultData = shiftDefs.getAllShiftDefNames(outputType)
			
	return resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Checks for a matching shift definition record
#
# -----------------------------------------------------------------------------------------------------------------------
def checkExistsShiftDef_byName(shiftDefName):

	result = shiftDefs.checkExistsShiftDef_byName(shiftDefName)
			
	return result	
	
		
		
# -----------------------------------------------------------------------------------------------------------------------
#
# Checks to see if a shift def name has NOT already been used
#
# -----------------------------------------------------------------------------------------------------------------------
def checkDoesNotExistShiftDef_byName(shiftDefName):

	result = shiftDefs.checkDoesNotExistShiftDef_byName(shiftDefName)
			
	return result	
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Updates an existing shift definition's name in teh database
#
# -----------------------------------------------------------------------------------------------------------------------
def updateShiftDefName(shiftDefId, newName):

	propsToUpdate = {'shift_def_name': str(newName)}
	
	result = shiftDefs.updateShiftDef_Dyn(shiftDefId, propsToUpdate)
			
	return result		
	
	
	
## -----------------------------------------------------------------------------------------------------------------------
##
## deletes future shifts and shift idle periods for a given line
##
## -----------------------------------------------------------------------------------------------------------------------
#def deleteFuturePeriods_byLine(lineId):
#
#	result = False
#	
#	deletedShiftPeriods = shared.ent.shifts.db_sql.shifts.deleteFutureShifts_byLine(lineId)
#	
#	deletedShiftIdlePeriods = shared.ent.shifts.db_sql.shift_idle_periods.deleteFutureShiftIdlePeriods_byLine(lineId)
#	
#	if deletedShiftPeriods and deletedShiftIdlePeriods:
#	
#		result = True
#		
#	return result
	