# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 09/20/2019
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	



	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# <Aruguments> 	- newShiftDefObj <dictionary> 	- 	a dictionary object containing the necessary properties of the new shift def to
#													create
#
# <Returns>		- success <bool> 			- 	returns a true or false result indicating a successful insert
#
# -----------------------------------------------------------------------------------------------------------------------
def insertNewShiftDef(newShiftDefObj):

	result = False
	resultData = {}
	insertedId = ''
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.shifts.db_sql.config.getTablePrefix()
	
	
	preparedQuery = """
	
		INSERT	[dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_t]
		(
			[json_definition]
			,[shift_def_name]
			,[last_updated]
			,[last_updated_by]
		)
		
		VALUES
		(
			'""" + str(newShiftDefObj['json_definition']) + """',
			'""" + str(newShiftDefObj['shift_def_name']) + """',
			GETDATE(),
			'""" + str(newShiftDefObj['last_updated_by']) + """'
		)
					
	"""

	# send the query to the database
	try:
	
		insertedId = system.db.runUpdateQuery(preparedQuery, database, getKey=1)
	
		if insertedId != 0:
		
			result = True
		
	except:
	
		print "Insert new shift definition record failed to execute."
		
		print preparedQuery
			
	return result, insertedId
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Updates an existing shift definition in the databse
#
# <Aruguments> 	- shiftDefId - the id of the shift def to update
# <Aruguments> 	- props(dictionary) - the cart properties to update
#
# <Returns>		- result (bool) - Indicates the successful update of the db record
#
# -----------------------------------------------------------------------------------------------------------------------
def updateShiftDef_Dyn(shiftDefId, propsToUpdate):

	result = False
	resultData = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.shifts.db_sql.config.getTablePrefix()
	

	# generate the "set" protion of the update query string from the list of properties to update
	setString = ''
	propsToUpdateCount = 0
	
	for prop in propsToUpdate:
	
#		print prop
#		print type(prop)
#		
#		print propsToUpdate[prop]
	
		setString += """,""" + str(prop) + """ = '""" + str(propsToUpdate[prop]) + """'"""
		propsToUpdateCount += 1


	# complete the query string
	preparedQuery = """

		UPDATE [dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_t]
   
		SET 
			[last_updated] = CURRENT_TIMESTAMP """ + setString + """

		WHERE id = '""" + str(shiftDefId) + """'
	
		"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
	else:
	
		# validate the length of the result data
		if resultData == 1:
			
			result = True
			
	return result		

	

# -----------------------------------------------------------------------------------------------------------------------
#
# Checks for a matching shift definition record
#
# <Aruguments> 	- shiftDefObj - The shift definition record of interest
#
# <Returns>		- result (bool) - Indicates the existance of the record in the db
#				- recordDictObj (PyDictionary) - The record data
#
# -----------------------------------------------------------------------------------------------------------------------
def checkExistsShiftDef(shiftDefObj):

	result = ''
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.shifts.db_sql.config.getTablePrefix()
	
	preparedQuery = """
	
		SELECT	TOP 1 
		
		*

		FROM [dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_t]

		WHERE 
	
			id = """ + str(shiftDefObj['id']) + """
				
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) == 1:
	
		result = True
		
	else:

		
		result = False
			
	return result
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Checks for a matching shift definition record
#
# <Aruguments> 	- shiftDefName - The shift definition name to check for
#
# -----------------------------------------------------------------------------------------------------------------------
def checkExistsShiftDef_byName(shiftDefName):

	result = False
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.shifts.db_sql.config.getTablePrefix()
	
	preparedQuery = """
	
		SELECT COUNT(*)

		FROM [dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_t]

		WHERE 
	
			shift_def_name = '""" + str(shiftDefName) + """'
				
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
	if len(resultData) == 1:
	
		if resultData[0][0] == 1:
	
			result = True
			
	return result	
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Checks to see if a shift def name has NOT already been used
#
# <Aruguments> 	- shiftDefName - The shift definition name to check for
#
# -----------------------------------------------------------------------------------------------------------------------
def checkDoesNotExistShiftDef_byName(shiftDefName):

	result = False
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.shifts.db_sql.config.getTablePrefix()
	
	preparedQuery = """
	
		SELECT COUNT(*)

		FROM [dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_t]

		WHERE 
	
			shift_def_name = '""" + str(shiftDefName) + """'
				
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
	if len(resultData) == 1:
	
		if resultData[0][0] == 0:
	
			result = True
			
	return result		
	
	

# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up a shift definition record by it's id in the Ignition database
#
# <Aruguments> 	- shiftDefId - The shift definition's id
#
# <Returns>		- result (bool) - Indicates the existance of the record in the db
#				- recordDictObj (PyDictionary) - The record data
#
# -----------------------------------------------------------------------------------------------------------------------
def getShiftDefById(shiftDefId):

	result = False
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.shifts.db_sql.config.getTablePrefix()
	
	preparedQuery = """
	
		SELECT
		
		*

		FROM [dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_t]

		WHERE 
	
			id = """ + str(shiftDefId) + """
				
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) == 1:
	
		try:
		
			# convert the dataset to a dictionary object
			recordDictObj = shared.ent.shifts.db_sql.misc.convertDatasetSingleRow(resultData)
		
			result = True
	
		except:
		
			print "a shift definition record was found for the id: " + str(shiftDefId) + ", but, the json could not be decoded from the record."

	
	else:
	
		print "query execution failed while attemtping to find a shift definition record for the id: " + str(shiftDefId)
		
		result = False
			
	return result, recordDictObj



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all shift definition records in the Ignition database
#
# <Aruguments> 	- outputType - whether the results should be return as a dictionary, or dataset.  "1" for dictioanry, anything else will return a dataset
#
# <Returns>		- result (bool) - Indicates the existance of the record in the db
#				- recordDictObj (PyDictionary) - The record data
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllShiftDefs(outputType = '1'):

	result = False
	resultData = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.shifts.db_sql.config.getTablePrefix()
	
	preparedQuery = """
	
		SELECT
		
		*

		FROM [dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_t]
			
		ORDER BY shift_def_name ASC
		
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) > 0:
	
		try:

			if outputType == '1':
		
				# convert the dataset to a dictionary object
				resultData = shared.ent.shifts.db_sql.misc.convertDatasetMultiRow(resultData)
			
				result = True

			else:

				result = True
	
		except:
		
			print "a shift definition records were found, but, the json could not be decoded from the records."

	else:
	
		print "query execution failed while attemtping to find a shift definition records."
		
		result = False
			
	return result, resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all shift definition names in the Ignition database
#
# <Aruguments> 	- outputType - whether the results should be return as a dictionary, or dataset.  "1" for dictioanry, anything else will return a dataset
#
# <Returns>		- result (bool) - Indicates the existance of the record in the db
#				- recordDictObj (PyDictionary) - The record data
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllShiftDefNames(outputType = '1'):

	result = False
	resultData = []
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.shifts.db_sql.config.getTablePrefix()
	
	preparedQuery = """
	
		SELECT
		
		id,
		shift_def_name,
		shift_def_description,
		last_updated_by,
		last_updated

		FROM [dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_t]
			
		ORDER BY shift_def_name ASC
		
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
	else:
	
		result = True

			
	# validate the length of the result data
	if result:
	
		if outputType == '1':
		
			# convert the dataset to a dictionary object
			resultData = shared.ent.shifts.db_sql.misc.convertDatasetMultiRow(resultData)
			
	return result, resultData

		
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Moves a shift definition record to the database table that keeps a history of shift definitions and their modifications
#
# <Aruguments> 	- shiftDefObj (dictionary) - the shift def record to move
#				- lastUpdatedBy (string) - the user name of the person who requested the move
#
# <Returns>		- result (bool) - Indicates the successful move of the db record
#
# -----------------------------------------------------------------------------------------------------------------------
def moveShiftDefToArchive(shiftDefObj, lastUpdatedBy):

	queryResult = False
	result = False
	resultData = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.shifts.db_sql.config.getTablePrefix()
	

	# complete the query string
	preparedQuery = """
		
		begin transaction

			-- this transaction moves a stack record from the working table to the history table

			-- step 1, create the new history record
			
			INSERT INTO [dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_archive_t]
			(
				[shift_def_name],
				[shift_def_description],
				[json_definition],
				[last_updated],
				[last_updated_by]
			)
			VALUES
			(
				'""" + str(shiftDefObj['shift_def_name']) +  """'
				,'""" + str(shiftDefObj['shift_def_description']) +  """'
				,'""" + str(shiftDefObj['json_definition']) +  """'

				,CURRENT_TIMESTAMP
				,'""" + str(lastUpdatedBy) +  """'
			)
			
			-- step 2, delete the record from the active shift definitions table
			
			DELETE FROM [dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_t]
			WHERE id = '""" + str(shiftDefObj['id']) +  """'
		
		commit transaction
		
	"""
		
	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		# print preparedQuery
		
		
	# validate the length of the result data
	if resultData >= 1:
		
		result = True
		
	else:

		print "Failed to move the shift def record for shift def id: " + str(shiftDefObj['id'])
		
		print preparedQuery 
		
	return result
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Updates a shift definition record in the database table, and adds a versioned copy to the history of shift definitions
#
# <Aruguments> 	- shiftDefObj (dictionary) - the shift def record to move
#				- lastUpdatedBy (string) - the user name of the person who requested the move
#
# <Returns>		- result (bool) - Indicates the successful move of the db record
#
# -----------------------------------------------------------------------------------------------------------------------
def saveUpdateShiftDef(newShiftDefObj, previousShiftDefObj):

	queryResult = False
	result = False
	resultData = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.shifts.db_sql.config.getTablePrefix()
	

	# complete the query string
	preparedQuery = """
		
		begin transaction

			-- this transaction moves a stack record from the working table to the history table

			-- step 1, create the new history record
			
			INSERT INTO [dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_archive_t]
			(
				[shift_def_name],
				[shift_def_description],
				[json_definition],
				[last_updated],
				[last_updated_by]
			)
			VALUES
			(
				'""" + str(previousShiftDefObj['shift_def_name']) +  """'
				,'""" + str(previousShiftDefObj['shift_def_description']) +  """'
				,'""" + str(previousShiftDefObj['json_definition']) +  """'

				,CURRENT_TIMESTAMP
				,'""" + str(newShiftDefObj['last_updated_by']) +  """'
			)
			
			-- step 2, update the record in the active shift definitions table
			
			UPDATE [dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_t]
					
			SET 
			
				[shift_def_description] = '""" + str(newShiftDefObj['shift_def_description']) + """'
				,[json_definition] = '""" + str(newShiftDefObj['json_definition']) + """'
				,[last_updated] = GETDATE()
				,[last_updated_by] = '""" + str(newShiftDefObj['last_updated_by']) +  """'
				
			WHERE 
			
				id = '""" + str(newShiftDefObj['id']) + """'
		
		commit transaction
		
	"""
		
	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		# print preparedQuery
		
		
	# validate the length of the result data
	if resultData >= 1:
		
		result = True
		
	else:

		print "Failed to move the shift def record for shift def id: " + str(shiftDefObj['id'])
		
		print preparedQuery 
		
	return result



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up a shift definition record by it's id in the Ignition database
#
# <Aruguments> 	- shiftDefId - The shift definition's id
#
# <Returns>		- result (bool) - Indicates the existance of the record in the db
#				- recordDictObj (PyDictionary) - The record data
#
# -----------------------------------------------------------------------------------------------------------------------
def getArchivedShiftDefById(shiftDefId):

	result = False
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.shifts.db_sql.config.getTablePrefix()
	
	preparedQuery = """
	
		SELECT
		
		*

		FROM [dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_archive_t]

		WHERE 
	
			id = """ + str(shiftDefId) + """
				
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) == 1:
	
		try:
		
			# convert the dataset to a dictionary object
			recordDictObj = shared.ent.shifts.db_sql.misc.convertDatasetSingleRow(resultData)
		
			result = True
	
		except:
		
			print "a shift definition record was found for the id: " + str(shiftDefId) + ", but, the json could not be decoded from the record."

	
	else:
	
		print "query execution failed while attemtping to find a shift definition record for the id: " + str(shiftDefId)
		
		result = False
			
	return result, recordDictObj



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up shift definition archived versions by the shift def name
#
# <Aruguments> 	- shiftDefName - The shift definition's name
#
# <Returns>		- result (bool) - Indicates the existance of the record in the db
#				- recordDictObj (PyDictionary) - The record data
#
# -----------------------------------------------------------------------------------------------------------------------
def getArchivedShiftDefsByName(shiftDefName):

	result = False
	resultData = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.shifts.db_sql.config.getTablePrefix()
	
	preparedQuery = """
	
		SELECT
		
		*

		FROM [dbo].[""" + tablePrefix + """ent_assets_v2_shift_definitions_archive_t]

		WHERE 
	
			shift_def_name = '""" + str(shiftDefName) + """'
			
		ORDER BY last_updated DESC
	
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) > 0:
	
		result = True
	
	else:
	
		print "query execution failed while attemtping to find archived shift definition records for shift def name: " + str(shiftDefName)
		
		result = False
			
	return result, resultData