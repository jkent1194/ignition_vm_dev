
# -----------------------------------------------------------------------------------------------------------------------
#
# Get the active shift data for a specific line
#
# <Arguments>	- lineId (string)		- the line for which the active shift data is desired
#
#				- outputType (string) 	- Indicates whether a dictionary object, or a dataset is desired "1" for dictionary, 
#											any other input value will return a dataset
#
# <Returns>		- result (bool) - Indicates the existance of at least one record in the db
#				- data object 	- resulting data in either a dictionary object or a dataset object, as selected by the input argument
#
# -----------------------------------------------------------------------------------------------------------------------
def getActiveShiftDataByLine(lineId, outputType):

	result = ''
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	
	preparedQuery = """
	SELECT 
		 [line_id]
		,[shift_date]
		,[day_of_week]
		,[shift_code]
		,[overtime_start_date]
		,[start_date]
		,[end_date]
		,[overtime_end_date]
		,[active_idle_period_id]
		,[active_idle_period_start_date]
		,[active_idle_period_end_date]
	FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_active_shifts_v]
	WHERE [line_id] = '""" + str(lineId) + """'
	"""
#	preparedQuery = """
#
#		SELECT 
#		      
#		    [line_id]
#		    ,[shift_date]
#			,[day_of_week]
#			,[shift_code]
#			,convert(varchar, [overtime_start_date], 20) overtime_start_date
#			,convert(varchar, [start_date], 20) start_date
#			,convert(varchar, [end_date], 20) end_date
#			,convert(varchar, [overtime_end_date], 20) overtime_end_date
#			,[active_idle_period_id]
#			,convert(varchar, [active_idle_period_start_date], 20) start_date
#			,convert(varchar, [active_idle_period_end_date], 20) start_date
#		
#		FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_active_shifts_v]
#
#		WHERE [line_id] = '""" + str(lineId) + """'
#
#	"""
	
	# print preparedQuery

	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) == 1:
		
		result = True
		
		# convert the dataset to a dictionary object
		recordDictObj = shared.ent.assets.db_sql.misc.datasetToDictionary(resultData, single = True)
		
	elif len(resultData) > 1:
	
		print "too many active shift records were found for line id: " + str(lineId)
			
		result = False
		
	else:

		# print "no shifts were found for line id: " + str(lineId)
			
		result = False
		
	
	if outputType == '1':
	
		return result, recordDictObj
	
	else:
	
		return result, resultData
		
		
		


	
