
import shared.ent.common.dates.utc.util.convertTimestampToUTC as convertTimestampToUTC

def insertShiftPeriods(lineID, shift, tx = None):
	
	log = system.util.getLogger("shift_generation")
	
	args = [
		lineID, 
		shift['shift_date'], 
		shift['overtime_start_date'], 
		shift['start_date'], 
		shift['end_date'], 
		shift['overtime_end_date'], 
		shift['shift_code'], 
		shift['day_of_week'], 
		shift['enabled'],
		convertTimestampToUTC(shift['overtime_start_date']), 
		convertTimestampToUTC(shift['start_date']), 
		convertTimestampToUTC(shift['end_date']), 
		convertTimestampToUTC(shift['overtime_end_date']), 
	]
	
	idleSQL = """
	INSERT INTO [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_idle_periods_t]
		([line_id], [shift_date], [start_date], [end_date], [shift_code], [day_of_week], [period_id], [enabled], [last_updated],
		[start_date_utc], [end_date_utc], [last_updated_utc])
    VALUES
	"""
	
	idleArgs = []
	
	for idleID in shift['idle_periods']:
	
		if len(idleArgs) == 0:
			idleSQL += "(?, ?, ?, ?, ?, ?, ?, ?, GETDATE(), ?, ?, GETUTCDATE())"
		else:
			idleSQL += """,
			(?, ?, ?, ?, ?, ?, ?, ?, GETDATE(), ?, ?, GETUTCDATE())"""
		
		idleArgs.append(lineID)
		idleArgs.append(shift['shift_date'])
		idleArgs.append(shift['idle_periods'][idleID]['start_date'])
		idleArgs.append(shift['idle_periods'][idleID]['end_date'])
		idleArgs.append(shift['shift_code'])
		idleArgs.append(shift['day_of_week'])
		idleArgs.append(idleID)
		idleArgs.append(shift['idle_periods'][idleID]['enabled'])
		idleArgs.append(convertTimestampToUTC(shift['idle_periods'][idleID]['start_date']))
		idleArgs.append(convertTimestampToUTC(shift['idle_periods'][idleID]['end_date']))
		
	idleSQL += ";"
	
	insert = """
	INSERT INTO [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_periods_t]
		([line_id], [shift_date], [overtime_start_date], [start_date], [end_date], [overtime_end_date], [shift_code], [day_of_week], [enabled], [last_updated], 
		[overtime_start_date_utc], [start_date_utc], [end_date_utc], [overtime_end_date_utc], [last_updated_utc])
	VALUES
		(?, ?, ?, ?, ?, ?, ?, ?, ?, GETDATE(), ?, ?, ?, ?, GETUTCDATE());
	"""
	
	if len(idleArgs) > 0:
		insert += idleSQL
		args.extend(idleArgs)
		
	try:
		system.db.runPrepUpdate(insert,
								database = "MANUFACTURING_ASSETS",
								args = args,
								tx = tx)
	
	except:
		log.warn("Exception in insertShiftPeriods")
		return 1
	
	return 0


def deleteShiftPeriods(lineID, shiftDate, shiftCode, tx = None):
		
	log = system.util.getLogger("shift_generation")
	
	delete = """
	DELETE 
	FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_periods_t]
	WHERE
		line_id = ?
		AND shift_date = CAST(? AS DATE)
		AND shift_code = ?;
	DELETE
	FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_idle_periods_t]
	WHERE
		line_id = ?
		AND shift_date = CAST(? AS DATE)
		AND shift_code = ?;
	"""
	
	try:
		system.db.runPrepUpdate(delete,
								database = "MANUFACTURING_ASSETS",
								args = [lineID, shiftDate, shiftCode, lineID, shiftDate, shiftCode],
								tx = tx)
	
	except:
		log.warn("Exception in deleteShiftPeriods")
		return 1
	
	return 0
