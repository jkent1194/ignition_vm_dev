# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 04/13/2020
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	



	
	
# -----------------------------------------------------------------------------------------------------------------------
#
#	Creates/updates an association between a line and a shift definition
#
# -----------------------------------------------------------------------------------------------------------------------
def upsertLineAssociation(lineId, shiftDefId, updatedBy):

	result = False
	resultData = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	
	preparedQuery = """

		IF NOT EXISTS (
		                SELECT * 
		                FROM [ent_assets_v2_line_shift_associations_t]
		                WHERE line_id = '""" + str(lineId) + """'
		            )

		    INSERT INTO [ent_assets_v2_line_shift_associations_t] (
		        line_id,
		        shift_def_id,
		        last_updated,
		        last_updated_by
		    )
		    VALUES(
		        '""" + str(lineId) + """',
		        '""" + str(shiftDefId) + """',
		        GETDATE(),
		        '""" + str(updatedBy) + """'
		    )

		ELSE

		    UPDATE [ent_assets_v2_line_shift_associations_t]
		    SET shift_def_id = '""" + str(shiftDefId) + """',
		        last_updated = GETDATE(),
		        last_updated_by = '""" + str(updatedBy) + """'
		    WHERE line_id = '""" + str(lineId) + """'

	"""

	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)

	except:
	
		print "Database query failed to execute."
		print preparedQuery
		
	else:
	
		print resultData
		
		if resultData == 1:
		
			result = True
			
	return result
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all shift definition records that are associated with at least one asset in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllAssociations(outputType = '1'):

	result = False
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	
	preparedQuery = """
	
		SELECT
		
		l.line_id,
		l.line_identifier,
		sd.id as shift_def_id,
		sd.shift_def_name

		FROM [dbo].[ent_assets_v2_line_shift_associations_t] lsd

		JOIN [dbo].[ent_assets_v2_lines_t] l ON (lsd.line_id = l.line_id)

		JOIN [dbo].[ent_assets_v2_shift_definitions_t] sd ON (lsd.shift_def_id = sd.id)

		ORDER BY l.line_identifier ASC
			
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
	else:
	
		result = True
		
		
	# validate the length of the result data
	if result:

		if outputType == '1':
	
			# convert the dataset to a dictionary object
			recordDictObj = shared.ent.shifts.db_sql.misc.convertDatasetMultiRow(resultData)

			resultData = recordDictObj
			
	return result, resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all lines associated with a shift definition record in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getLinesByShiftDef(shiftDefId, outputType = '1'):

	result = False
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	
	preparedQuery = """
	
		SELECT 
			s.site_id
			,s.site_identifier
			
			,a.area_id
			,a.area_identifier
			
			,l.line_id
			,l.line_identifier
			
			,ls.shift_def_id
		
		FROM dbo.ent_assets_v2_lines_t l
		
			JOIN dbo.ent_assets_v2_areas_t a ON a.area_id = l.area_id
		
			JOIN dbo.ent_assets_v2_sites_t s ON s.site_id = a.site_id
		
			LEFT JOIN dbo.ent_assets_v2_line_shift_associations_t ls ON l.line_id = ls.line_id
		
		WHERE ls.shift_def_id = '""" + str(shiftDefId) + """'
				
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
	else:
	
		result = True
		
	# validate the length of the result data
	if result:

		if outputType == '1':
	
			# convert the dataset to a dictionary object
			recordDictObj = shared.ent.shifts.db_sql.misc.convertDatasetMultiRow(resultData)

			resultData = recordDictObj
			
	return result, resultData



# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up a shift definition record by an associated line in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getShiftDefByLine(lineId, outputType = '1'):

	result = False
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	
	preparedQuery = """
	
		SELECT
		
		sd.id
		,sd.shift_def_name
		,sd.shift_def_description
		,sd.json_definition

		FROM [dbo].[ent_assets_v2_line_shift_associations_t] ls
		
		JOIN [dbo].[ent_assets_v2_shift_definitions_t] sd ON (ls.shift_def_id = sd.id)

		WHERE ls.line_id = '""" + str(lineId) + """'
				
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
	else:
	
		result = True
		
		
	# validate the length of the result data
	if result:
	
		if len(resultData) == 1:
	
			if outputType == '1':
			
				# convert the dataset to a dictionary object
				recordDictObj = shared.ent.shifts.db_sql.misc.convertDatasetSingleRow(resultData)
	
				resultData = recordDictObj
		else:
		
			result = False

	return result, resultData



# -----------------------------------------------------------------------------------------------------------------------
#
#	Deletes an association between a line and a shift definition, by line id
#
# -----------------------------------------------------------------------------------------------------------------------
def deleteAssociationByLine(lineId):

	result = False
	resultData = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()	
	
	preparedQuery = """

		DELETE FROM [ent_assets_v2_line_shift_associations_t]
		 
		WHERE line_id = '""" + str(lineId) + """'

	"""

	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)

	except:
	
		print "Database query failed to execute."
		print preparedQuery
		
	else:
		
		if resultData == 1:
		
			result = True
			
	return result



# -----------------------------------------------------------------------------------------------------------------------
#
#	Deletes all associations between lines and a shift definition, by shift def id
#
# -----------------------------------------------------------------------------------------------------------------------
def deleteAssociationsByShiftDef(shiftDefId):

	result = False
	resultData = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	
	preparedQuery = """

		DELETE FROM [ent_assets_v2_line_shift_associations_t]
		 
		WHERE shift_def_id = '""" + str(shiftDefId) + """'

	"""

	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)

	except:
	
		print "Database query failed to execute."
		print preparedQuery
		
	else:
		
		if resultData == 1:
		
			result = True
			
	return result



# -----------------------------------------------------------------------------------------------------------------------
#
# Checks to see if a shift definition is associated with any lines the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def checkIfUsed_ShiftDef(shiftDefId):

	result = False
	resultData = ''
	querySuccessful = False
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	
	preparedQuery = """
	
		SELECT
		
		COUNT(*)

		FROM [dbo].[ent_assets_v2_line_shift_associations_t]

		WHERE shift_def_id = '""" + str(shiftDefId) + """'
				
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery

	else:
	
		querySuccessful = True
		
	# validate the length of the result data
	if querySuccessful:
	
		if resultData[0][0] > 0:
		
			result = True
			
	return querySuccessful, result




# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all lines that are NOT associated with a shift definition record in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getLinesWithoutShiftDefs(outputType = '1'):

	result = False
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	
	preparedQuery = """
	
		SELECT 

			s.site_id
			,s.site_identifier

			,a.area_id
			,a.area_identifier

			,l.line_id
			,l.line_identifier

			,ls.shift_def_id

		FROM dbo.ent_assets_v2_lines_t l

			JOIN dbo.ent_assets_v2_areas_t a ON a.area_id = l.area_id

			JOIN dbo.ent_assets_v2_sites_t s ON s.site_id = a.site_id

			LEFT JOIN dbo.ent_assets_v2_line_shift_associations_t ls ON l.line_id = ls.line_id

		WHERE ls.shift_def_id IS NULL
						
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
	else:
	
		result = True
		
		
	# validate the length of the result data
	if result:

		if outputType == '1':
	
			# convert the dataset to a dictionary object
			recordDictObj = shared.ent.shifts.db_sql.misc.convertDatasetMultiRow(resultData)

			resultData = recordDictObj
		
	return result, resultData
	
	
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all lines in the Ignition database
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllLines(outputType = '1'):

	result = False
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	
	preparedQuery = """
	
		SELECT 

			s.site_id
			,s.site_identifier

			,a.area_id
			,a.area_identifier

			,l.line_id
			,l.line_identifier

			,ls.shift_def_id

		FROM dbo.ent_assets_v2_lines_t l

			JOIN dbo.ent_assets_v2_areas_t a ON a.area_id = l.area_id

			JOIN dbo.ent_assets_v2_sites_t s ON s.site_id = a.site_id

			LEFT JOIN dbo.ent_assets_v2_line_shift_associations_t ls ON l.line_id = ls.line_id
						
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
	else:
	
		result = True
		
		
	# validate the length of the result data
	if result:

		if outputType == '1':
	
			# convert the dataset to a dictionary object
			recordDictObj = shared.ent.shifts.db_sql.misc.convertDatasetMultiRow(resultData)

			resultData = recordDictObj
		
	return result, resultData
