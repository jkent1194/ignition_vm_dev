# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 10/12/2019
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	





# -----------------------------------------------------------------------------------------------------------------------
#
# Looks up all of the line Id's in the Ignition database
#
# <Returns>		- result (bool) - Indicates the existance of the record in the db
#				- lineIds (List) - The line ids found in teh database in a List
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllLineIds():

	result = False
	resultData = {}
	recordDictObj = {}
	lineIds = []
	
	database = shared.ent.shifts.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.shifts.db_sql.config.getTablePrefix()
	
	preparedQuery = """
	
		SELECT [line_id]
      
			FROM [dbo].[""" + tablePrefix + """ent_assets_v2_lines_t]
				
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) > 0:
	
		try:
		
			# convert the dataset to a dictionary object
			recordDictObj = shared.ent.shifts.db_sql.misc.convertDatasetMultiRow(resultData)
		
			result = True
			
			# print recordDictObj
			
			for line in recordDictObj:
			
				newLineId =  int(line['line_id'])
				
				# print newLineId
				
				lineIds.append(newLineId)
	
		except:
		
			print "Failed to convert the data to a list of line Id's"
			
			print recordDictObj

	
	else:
	
		print "There were no line ID's returned by the query."
		
		print preparedQuery
		
		result = False
			
	return result, lineIds
		
		
		
		
	


	
	

	
