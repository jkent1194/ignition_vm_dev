



# -----------------------------------------------------------------------------------------------------------------------
#
# Get all ofthe active shift idle period data for a specific line
#
# <Arguments>	- lineId (string)		- the line for which the active shift idle period data is desired
#
#				- outputType (string) 	- Indicates whether a dictionary object, or a dataset is desired "1" for dictionary, 
#											any other input value will return a dataset
#
# <Returns>		- result (bool) - Indicates the existance of at least one record in the db
#				- data object 	- resulting data in either a dictionary object or a dataset object, as selected by the input argument
#
# -----------------------------------------------------------------------------------------------------------------------
def getActiveShiftIdlePeriodsByLine(lineId, activeShiftCode, outputType):

	result = False
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	
	preparedQuery = """

		SELECT

		lines.line_id
		,aip.shift_date
		,aip.day_of_week
		,aip.shift_code
		,aip.period_id
		,aip.start_date
		,aip.end_date
		,aip.enabled


		FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_lines_t] lines

		LEFT JOIN (

					SELECT 

						*
					
					FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_idle_periods_t] idle_periods

					WHERE -- idle_periods.start_date <= GETDATE() 
					
						idle_periods.enabled = 'True'

						AND idle_periods.shift_date = (SELECT sp.[shift_date] FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_periods_t] sp WHERE overtime_start_date <= GETDATE()
															AND GETDATE() <= overtime_end_date
															AND sp.enabled = 1
															AND sp.line_id = idle_periods.line_id)

					) AS aip ON (aip.line_id = lines.line_id)

		WHERE lines.line_id = '""" + str(lineId) + """'
		
			AND aip.shift_date IS NOT NULL
			
			AND aip.shift_code = '""" + str(activeShiftCode) + """'

		ORDER BY aip.start_date ASC

	"""
	
	# print preparedQuery

	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
		result = True
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		

	
	if outputType == '1':
	
		# convert the dataset to a dictionary object
		recordDictObj = shared.ent.assets.db_sql.misc.datasetToDictionary(resultData)
	
		return result, recordDictObj
	
	else:
	
		return result, resultData	



