

# ----------------------------------------------------------------------------------------------	
#
# Funciton description: Returns a boolean result indicating whether or not this Ignition tag provider
#						resides in the production environment.
#
# ----------------------------------------------------------------------------------------------	
def getEnvironmentIsProd():

	isProd = system.tag.readBlocking(['[default]Environment/isProd'])[0].value
	
	# print isProd
	
	# if the tag cannot be read, always assume this is living in the PROD environment
	if isProd != True and isProd != False:
	
		isProd = True

	return isProd
	
	
