"""
Author: Andrew Gifford
Date: 02/20/2020

File Description: Contains utility methods to retrieve and store user preferences
"""
# ----------------------------------------------------------------------------------------------	
#
# Author: Andrew Gifford
# Date: 02/20/2020
#
# File Description: Contains utility methods to retrieve and store user preferences
#
# ----------------------------------------------------------------------------------------------


import shared.ent.user.db_sql.preference as db_sql
import shared.ent.user.services.preference as util


# -----------------------------------------------------------------------------------------------------------------------
#
# Retrieves all user preference value/object(s) for a given application
#
# <Aruguments> 	- userName (string) - a string that specifies which user's preferences are retrieve
#				- appName (string) - a string that specifies which app the preferences are for
#
# <Returns>     - value (dictionary) - returns a dictionary of all a user's preferences for the given app
#
#	NOTE:  	for preferences that have defaults and no entry for the given user, the default entry will be used
#
# -----------------------------------------------------------------------------------------------------------------------
def getUserPreferences(userName, appName):
	
	code, result = db_sql.queryAllPreferences(appName, userName)
	
	if code < 0:
		return None
	
	code, resultDefaults = db_sql.queryAllPreferences(appName)
			
	if code < 0:
		return None
											
	preferences = {}
	
	for i in range(result.getRowCount()):
		preferences[result.getValueAt(i, 'preference_name')] = util.decode(result.getValueAt(i, 'preference_value'),
																		   result.getValueAt(i, 'preference_type'))
	
	for i in range(resultDefaults.getRowCount()):
		
		defaultName = resultDefaults.getValueAt(i, 'preference_name')
		defaultType = resultDefaults.getValueAt(i, 'preference_type')
		defaultValue = resultDefaults.getValueAt(i, 'preference_value')
		
		if defaultName not in preferences:
			preferences[defaultName] = util.decode(defaultValue,
												   defaultType)
	
	return preferences


# -----------------------------------------------------------------------------------------------------------------------
#
# Retrieves a user preference value/object
#
# <Aruguments> 	- userName (string) - a string that specifies which user's preference to retrieve
#				- appName (string) - a string that specifies which app the preference is for
#				- preferenceName (string) - a string that specifies the name of the preference to retrieve
#
# <Returns>     - value (object) - returns the preference as it was stored
#
#	NOTE:  	will return the default preference if no preference is stored for the given user or None if the preference 
#			does not exist
#
# -----------------------------------------------------------------------------------------------------------------------
def getUserPreference(userName, appName, preferenceName):
	"""Return a user's preference for a specific item 
	(or the default if no preference is set for the user).
	"""
	
	code, result = db_sql.queryPreference(appName, preferenceName, userName)
	
	if code < 0:
		return None
	
	if result.getRowCount() == 0:
		return getDefaultPreference(appName, preferenceName)
	else:
		return util.decode(result.getValueAt(0, 'preference_value'),
						   result.getValueAt(0, 'preference_type'))


# -----------------------------------------------------------------------------------------------------------------------
#
# Retrieves the default preference value/object
#
# <Aruguments> 	- appName (string) - a string that specifies which app the preferences are for
#				- preferenceName (string) - a string that specifies the name of the preference to retrieve
#
# <Returns>     - value (object) - returns the preference as it was stored
#
#	NOTE:  	if no default preference is found, None will be returned
#
# -----------------------------------------------------------------------------------------------------------------------
def getDefaultPreference(appName, preferenceName):
	"""Return the default preference, or None if there is no default"""
			
	code, result = db_sql.queryPreference(appName, preferenceName)
	
	if code < 0:
		return None
	
	if result.getRowCount() == 0:
		return None
	else:
		return util.decode(result.getValueAt(0, 'Preference_Value'),
						   result.getValueAt(0, 'Preference_Type'))
	#!!!!!!!!!!!!!
	

# -----------------------------------------------------------------------------------------------------------------------
#
# Stores a user preference value/object
#
# <Aruguments> 	- userName (string) - a string that specifies the user associated with the preference
#				- appName (string) - a string that specifies which app the preference is for
#				- preferenceName (string) - a string that specifies the name of the preference to store
#				- preferenceValue (object) - the object to be stored
#				- preferenceType (string) - the type of object being stored
#
# <Returns>     - returnCode (int) - 0 : no change
#									 1 : insert successful
#									-1 : insert failed
#									 2 : update successful
#									-2 : update failed
#
# -----------------------------------------------------------------------------------------------------------------------
def setUserPreference(userName, appName, preferenceName, preferenceValue, preferenceType="literal"):
	"""Set or change user preference."""
	
	encodedValue, preferenceType = util.encode(preferenceValue, preferenceType)
	
	set = db_sql.preferenceSet(appName, preferenceName, encodedValue, preferenceType, userName)
	
	if set == 0:
		return db_sql.insertPreference(appName, preferenceName, encodedValue, preferenceType, userName)
		
	elif set == 1:
		return db_sql.updatePreference(appName, preferenceName, encodedValue, preferenceType, userName)
	
	else:
		return 0


# -----------------------------------------------------------------------------------------------------------------------
#
# Stores the default preference value/object
#
# <Aruguments> 	- appName (string) - a string that specifies which app the preference is for
#				- preferenceName (string) - a string that specifies the name of the preference to store
#				- preferenceValue (object) - the object to be stored
#				- preferenceType (string) - the type of object being stored
#
# <Returns>     - returnCode (int) - 0 : no change
#									 1 : insert successful
#									-1 : insert failed
#									 2 : update successful
#									-2 : update failed
#
# -----------------------------------------------------------------------------------------------------------------------
def setDefaultPreference(appName, preferenceName, preferenceValue, preferenceType=None):
	"""Set or change default preference."""
	
	encodedValue, preferenceType = util.encode(preferenceValue, preferenceType)
	
	set = db_sql.preferenceSet(appName, preferenceName, encodedValue, preferenceType)
	
	if set == 0:
		return db_sql.insertPreference(appName, preferenceName, encodedValue, preferenceType)
		
	elif set == 1:
		return db_sql.updatePreference(appName, preferenceName, encodedValue, preferenceType)
	
	else:
		return 0

