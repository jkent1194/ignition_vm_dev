import shared.ent.user.db_sql.role as db

#SERVER = "livefactory"

def addUserRole(userId, role):
	
	hub = db.getHubServer()
	
	system.util.sendRequest(project = "ENT_ActiveDirectory_Services",
							messageHandler = "updateRole",
							payload = {'action':"add", 'user':userId, 'role':role},
							remoteServer = hub)


def removeUserRole(userId, role):
	
	hub = db.getHubServer()
	
	system.util.sendRequest(project = "ENT_ActiveDirectory_Services",
							messageHandler = "updateRole",
							payload = {'action':"remove", 'user':userId, 'role':role},
							remoteServer = hub)

