# ----------------------------------------------------------------------------------------------	
#
# Author: Andrew Gifford
# Date: 02/20/2020
#
# File Description: Contains helper methods to the preference interface
#
# ----------------------------------------------------------------------------------------------


import ast
from com.inductiveautomation.ignition.common import TypeUtilities


#def _getType(value):
#	"""Returns the type of the supplied value for use by codec methods."""
#	
#	pType = str(type(value)).split("'")[1]
#	
#	if pType == "list" and len(value):
#		subType = str(type(value[0])).split("'")[1]
#		
#		if pType + "_" + subType not in encodeMethods:
#			subType = None
#		else:
#			for x in value:
#				if subType != str(type(x)).split("'")[1]:
#					subType = None
#					break
#		
#		if subType is not None:
#			pType += "_" + subType
#	
#	return pType


# -----------------------------------------------------------------------------------------------------------------------
#
# Encodes the given value with given type into a string
#
# <Aruguments> 	- value (object) - the object to be encoded
#				- type (string) - the datatype of the value object
#
# <Returns>     - encodedValue (string) - returns the encoded string
#				- type (string)			- returns the type used for encoding
#
#	NOTE:  	if no method is specified for the given type, the type will be overwritten as 'literal'
#
# -----------------------------------------------------------------------------------------------------------------------
def encode(value, type):
	
	if type in encodeMethods:
		return encodeMethods[type](value), type
	else:
		return _encodeLiteral(value), "literal"

		
def _encodeLiteral(value):
	rep = repr(value)
	if rep.find("<ObjectWrapper>: ") == 0:
		rep = rep.lstrip("<ObjectWrapper>: ")
	return rep
	
def _encodeJson(value):
	return repr(TypeUtilities.pyToGson(value))


encodeMethods = {
#	"str":_encodeString,
#	"int":_encodeInt,
#	"int_list":_encodeIntList,
#	"str_list":_encodeStringList,
#	"float":_encodeFloat,
	"literal":_encodeLiteral,
	"json":_encodeJson
}


# -----------------------------------------------------------------------------------------------------------------------
#
# Decodes the given string value into an object of given type
#
# <Aruguments> 	- value (string) - the object to be decoded
#				- type (string) - the output datatype of the value string
#
# <Returns>     - decodedValue (object) - returns the decoded object
#
#	NOTE:  	if no method is specified for the given type, the type is interpretted as a string
#
# -----------------------------------------------------------------------------------------------------------------------
def decode(value, type):
	if type in decodeMethods:
		return decodeMethods[type](value)
	else:
		return str(value)

#def _decodeString(value):
#	return str(value)
#	
#def _decodeInt(value):
#	return int(value)
#	
#def _decodeFloat(value):
#	return float(value)
#	
#def _decodeIntList(value):
#	return [int(x) for x in value.split(',')]
#	
#def _decodeStringList(value):
#	return value.split(',')
	
def _decodeLiteral(value):
	return ast.literal_eval(value)

def _decodeJson(value):
	return TypeUtilities.gsonToPy(value)

decodeMethods = {
#	"str":_decodeString,
#	"int":_decodeInt,
#	"int_list":_decodeIntList,
#	"str_list":_decodeStringList,
#	"float":_decodeFloat,
	"literal":_decodeLiteral,
	"json":_decodeJson
}#!!!!!!!!!!!!!