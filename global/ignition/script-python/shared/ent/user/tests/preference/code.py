import unittest
from shared.ent.user.interface import preference
from shared.ent.user.tests import stubs

#print dir(unittest)

class stubbedTestCase(unittest.TestCase):
	def setUp(self):
		self.normalImport = {}
		self.normalImport['db_sql'] = preference.db_sql
		preference.db_sql = stubs.db_sql.preference
	
	def testUnitTest1(self):
		user = 'testUser1'
		app = 'testApp1'
		pref = 'testPreference1'
		self.assertEqual(preference.getUserPreference(user, app, pref), {1:'1','2':1})
		
	def testUnitTest2(self):
		user = 'testUser2'
		app = 'testApp1'
		pref = 'testPreference1'
		self.assertEqual(preference.getUserPreference(user, app, pref), {1:'1','2':1})
		
	def tearDown(self):
		preference.db_sql = self.normalImport['db_sql']


def runTests():
	suite = getSuite()

	runner = unittest.TextTestRunner()

	runner.run(suite)


def getSuite():
	return unittest.makeSuite(stubbedTestCase,'test')