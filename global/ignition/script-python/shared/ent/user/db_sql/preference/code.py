# ----------------------------------------------------------------------------------------------	
#
# Author: Andrew Gifford
# Date: 02/20/2020
#
# File Description: Contains database methods to retrieve and store user preferences 
#                   in the ent_user_preference_t table
#
# ----------------------------------------------------------------------------------------------


db = "MANUFACTURING_DATA"
preferenceTable = "ent_user_preference_t"
defaultUserName = "default"


# -----------------------------------------------------------------------------------------------------------------------
#
# Queries the database for a given user preference
#
# <Aruguments> 	- appName (string) - a string that specifies which app the preference is for
#				- preferenceName (string) - a string that specifies the name of the preference to query
#				- userName (string) - a string that specifies the user associated with the preference
#
# <Returns>     - returnCode (int) 	  - 0 : successful query
#									   -1 : error querying database
#				- resultSet (dataset) - returns the results dataset from the query
#
#	NOTE:  	if no userName is supplied, the default preference will be queried
#
# -----------------------------------------------------------------------------------------------------------------------
def queryPreference(appName, preferenceName, userName=defaultUserName):

	query = """
	SELECT Preference_Type, Preference_Value
	FROM [dbo].[{preference_table}]
	WHERE 
			[user_name] = ?
		AND [app_name] = ?
		AND [preference_name] = ?
	""".format(preference_table = preferenceTable)
	
	try:
		result = system.db.runPrepQuery(query,
										database = db,
										args = [userName, appName, preferenceName])
		
		return 0, result
		
	except:
		return -1, None


# -----------------------------------------------------------------------------------------------------------------------
#
# Queries the database for all preferences for a given user
#
# <Aruguments> 	- appName (string) - a string that specifies which app the preferences are for
#				- userName (string) - a string that specifies the user associated with the preferences
#
# <Returns>     - returnCode (int) 	  - 0 : successful query
#									   -1 : error querying database
#				- resultSet (dataset) - returns the results dataset from the query
#
#	NOTE:  	if no userName is supplied, the default preferences will be queried
#
# -----------------------------------------------------------------------------------------------------------------------
def queryAllPreferences(appName, userName=defaultUserName):
	
	query = """
	SELECT preference_name, preference_type, preference_value
	FROM [dbo].[{preference_table}]
	WHERE 
			[user_name] = ?
		AND [app_name] = ?
	""".format(preference_table = preferenceTable)
	
	try:
		result = system.db.runPrepQuery(query,
										database = db,
										args = [userName, appName])
										
		return 0, result
	
	except:
		return -1, None


# -----------------------------------------------------------------------------------------------------------------------
#
# Queries the database checking if a preference is stored for a given user and if the stored value is the same as the 
#	supplied value
#
# <Aruguments> 	- appName (string) - a string that specifies which app the preference is for
#				- preferenceName (string) - a string that specifies the name of the preference to check
#				- encodedValue (string) - the preference value to compare against
#				- preferenceType (string) - the preference datatype to compare against
#				- userName (string) - a string that specifies the user associated with the preference
#
# <Returns>     - returnCode (int) 	  - 0 : record not found
#									    1 : record found; different value
#									    2 : record found; same value
#
#	NOTE:  	if no userName is supplied, the default preference will be queried
#
# -----------------------------------------------------------------------------------------------------------------------
def preferenceSet(appName, preferenceName, encodedValue, preferenceType, userName=defaultUserName):
	
	query = """
	SELECT Preference_Type, Preference_Value
	FROM [dbo].[{preference_table}]
	WHERE 
			[user_name] = ?
		AND [app_name] = ?
		AND [preference_name] = ?
	""".format(preference_table = preferenceTable)
	
	result = system.db.runPrepQuery(query,
									database = db,
									args = [userName, appName, preferenceName])
	if result.getRowCount() == 0:				
		return 0
	elif result.getValueAt(0,'Preference_Value') == encodedValue and result.getValueAt(0, 'Preference_Type') == preferenceType:
		return 2
	else:
		return 1


# -----------------------------------------------------------------------------------------------------------------------
#
# Inserts a new preference record into the database
#
# <Aruguments> 	- appName (string) - a string that specifies which app the preference is for
#				- preferenceName (string) - a string that specifies the name of the preference
#				- encodedValue (string) - the preference value
#				- preferenceType (string) - the preference datatype
#				- userName (string) - a string that specifies the user associated with the preference
#
# <Returns>     - returnCode (int) - 1 : insert successful
#									-1 : insert failed
#
#	NOTE:  	if no userName is supplied, the default preference will be inserted
#
# -----------------------------------------------------------------------------------------------------------------------
def insertPreference(appName, preferenceName, encodedValue, preferenceType, userName=defaultUserName):
	
	try:
		insert = """
		INSERT INTO [dbo].[{preference_table}]
		(user_name, app_name, preference_name, preference_type, preference_value)
		VALUES (?, ?, ?, ?, ?)
		""".format(preference_table = preferenceTable)
		
		system.db.runPrepUpdate(insert,
								database = db,
								args = [userName, appName, preferenceName, preferenceType, encodedValue])
		
		return 1
		
	except:
		return -1


# -----------------------------------------------------------------------------------------------------------------------
#
# Updates the value (and type) in a preference record in the database
#
# <Aruguments> 	- appName (string) - a string that specifies which app the preference is for
#				- preferenceName (string) - a string that specifies the name of the preference
#				- encodedValue (string) - the preference value
#				- preferenceType (string) - the preference datatype
#				- userName (string) - a string that specifies the user associated with the preference
#
# <Returns>     - returnCode (int) - 2 : update successful
#									-2 : update failed
#
#	NOTE:  	if no userName is supplied, the default preference will be updated
#
# -----------------------------------------------------------------------------------------------------------------------
def updatePreference(appName, preferenceName, encodedValue, preferenceType, userName=defaultUserName):
	
	try:
		update = """
		UPDATE [dbo].[{preference_table}]
		SET preference_type = ?,
			preference_value = ?
		WHERE
				user_name = ?
			AND app_name = ?
			AND preference_name = ?
		""".format(preference_table = preferenceTable)
		
		system.db.runPrepUpdate(update,
								database = db,
								args = [preferenceType, encodedValue, userName, appName, preferenceName])
										
		return 2
	
	except:
		return -2
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Deletes a preference record from the database
#
# <Aruguments> 	- appName (string) - a string that specifies which app the preference is for
#				- preferenceName (string) - a string that specifies the name of the preference
#				- userName (string) - a string that specifies the user associated with the preference
#
# <Returns>     - returnCode (int) - 3 : delete successful
#									 0 : no record
#									-3 : delete failed
#
#	NOTE:  	if no userName is supplied, the default preference will be delete
#
# -----------------------------------------------------------------------------------------------------------------------
def deletePreference(appName, preferenceName, userName=defaultUserName):
	
	try:
		update = """
		DELETE FROM [dbo].[{preference_table}]
		WHERE
				user_name = ?
			AND app_name = ?
			AND preference_name = ?
		""".format(preference_table = preferenceTable)
		
		ret = system.db.runPrepUpdate(update,
									  database = db,
									  getKey = 0,
									  args = [userName, appName, preferenceName])
		if ret == 1:
			return 3
		elif ret > 1:
			return -4
		else:
			return 0
	
	except:
		return -3