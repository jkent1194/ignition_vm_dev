

def getHubServer():
	
	query = """
	SELECT property_value
	FROM [dbo].[ent_app_property_t]
	WHERE [app_name] = 'all'
		AND [property_name] = 'hubServerName'
	"""
	
	server = system.db.runScalarQuery(query, database = "MANUFACTURING_DATA")
	
	return server

