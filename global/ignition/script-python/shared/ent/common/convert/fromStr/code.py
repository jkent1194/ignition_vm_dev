# ----------------------------------------------------------------------------------------------	
#
# Author: Janardan Kortikere
# Date: 03/25/2020
#
# File Description: Contains functions to handle conversions from string.
#
# ----------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
#
# Funtion for converting string to int.
#
# <Aruguments> 	- inStr (string) 
#				- defaultValue
#				
#
# <Returns>		- intVal (int) 
#
#				
# -----------------------------------------------------------------------------------------------------------------------			
def toInt(inStr, defaultValue):
	
	returnValue = defaultValue
	try:
		returnValue = int(inStr)
	except ValueError:
		return defaultValue
		
	return returnValue