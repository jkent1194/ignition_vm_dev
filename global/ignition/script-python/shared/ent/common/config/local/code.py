import os
 
 
def getBasePath():
	return os.getcwd().split(":")[0] + ":/HMI/config/"


def getConfigValue(configName):
	basepath = getBasePath()

	path = basepath + configName

	value = None

	if system.file.fileExists(path):
		value = system.file.readFileAsString(path, "UTF-8")

	return value


def setConfigValue(configName, value):
	basepath = getBasePath()

	path = basepath + configName

	if not system.file.fileExists(basepath):
		os.makedirs(basepath)

	system.file.writeFile(path, value, False, "UTF-8")
