import sys, traceback
# ----------------------------------------------------------------------------------------------	
#
# Author: Janardan Kortikere
# Date: 01/21/2020
#
# File Description: Contains functions to call MES REST API
#
# ----------------------------------------------------------------------------------------------	


# ----------------------------------------------------------------------------------------------------------------------
#
# Get the URL prefix for calling the MES Rest API
#
# <Aruguments> 	- None
#
# <Returns>		- urlPrefix (string) 	- Returns a prefix for API URL based on environment
#
# -----------------------------------------------------------------------------------------------------------------------
def getMESRestAPIUrlPrefix():

	envIsProd = shared.ent.environment.constants.getEnvironmentIsProd()

	if envIsProd:
		return "http://meswebapp/mes/rest/"
	else:
		return "http://meswebappdev/mes/rest/"

# ----------------------------------------------------------------------------------------------------------------------
#
# Calls MES API
#
# <Aruguments> 	- url (string) 		- url without prefix
#				- dataName (string) - Name of the data output from API
#
# <Returns>		- apiCallWasSuccess (boolean) - True or False
#				- apiData (dictionary) 	- Returns a dictionary object
# -----------------------------------------------------------------------------------------------------------------------
def callMESRestAPI(url, dataName):
	
	# start a script execution timer
	executionTime = system.date.now() 
	
	# variable declarations
	completeUrl = ""
	apiCallWasSuccess = False	

	resultCode = ""
	log = ""

	resultsJson = ""
	decodedJson = {}
	
	apiData = {}

	# add prefix to URL
	completeUrl = getMESRestAPIUrlPrefix() + url

	log = addToLog(log, "Calling API using URL - : " + completeUrl)
	
	# Make the API call.
	try:
		
		resultsJson = system.net.httpGet(completeUrl)
		
		# Convert the JSON string into a Python object. In this case, it results in a Dictionary.
		decodedJson = system.util.jsonDecode(resultsJson)
		
		apiCallWasSuccess = True
		
		log = addToLog(log, "The API call was successful.")
		
	except:
		
		decodedJson = {}
		
		log = writeDetailedErrorLog(url, log)
		
	# --------------------------------------------------------------------------------------------------------------------------	
	# Once the API call is successfully executed, and a JSON result was received, 
	#	work with the returned JSON data from the API call.
	# --------------------------------------------------------------------------------------------------------------------------

	# Verify there is only one top level "object" in the decoded JSON dictionary object, and that the API call executed.
	if apiCallWasSuccess and len(decodedJson) == 1:
	
		log, resultCode, apiData = parseJson(decodedJson, log, dataName)
		
		if resultCode != "success":
			apiCallWasSuccess = False

	endTime = system.date.now()
	timeElapsed = system.date.millisBetween(executionTime, endTime)

	log = addToLog(log, "API execution time: " + str(timeElapsed))
	
	insertEventLog(url, apiData, log, resultCode, timeElapsed)
	
	# return the success flag and api data
	return apiCallWasSuccess, apiData
	

# ----------------------------------------------------------------------------------------------------------------------
#
# Prase Json from MES API call
#
# <Aruguments> 	- decodedJson (dictionary) 		- data from MES API call
#				- log (string) - log of events till now
#				- dataName (string) - Name of the data output from API
#
# <Returns>		- log (string) - log of events till now
#				- resultCode (string) - SUCCESS or ERROR returned by MES API
#				- apiData (dictionary) 	- Returns a dictionary object
# -----------------------------------------------------------------------------------------------------------------------
def parseJson(decodedJson, log, dataName):
	

	payload = {}	
	data = {}	
	resultCode = ""
	message = ""	
	apiData = {}		

	if "payload" in decodedJson:
	
		# payload exists in decoded JSON dictionary object, get child objects
		payload = decodedJson["payload"]
		
		resultCode = str(payload["result"])
		
		message = str(payload["message"])
		
		data = payload["data"]
		
		log = addToLog(log, "Message from API - " + resultCode + " " + message)
		
	elif "status" in decodedJson:
	
		# status exists in decoded JSON dictionary object, get message
		message = str(decodedJson["status"])
		
		if "success" in message.lower():
			resultCode = "success"
		else:
			resultCode = "error"
			
		log = addToLog(log, "Message from API - " + message)
		
	else:
	
		message = "Returned JSON Output is invalid"
		
		resultCode = "error"
		
		log = addToLog(log, message)		
			
	# read api data if present
	if resultCode.lower() == "success" and len(dataName) > 0 and dataName in data:
		
		try:
					
			apiData = data[dataName]
							
		except:
						
			log = addToLog(log, "Failed to parse the apiData object in the response JSON.")
				
	
	# return the success flag and api data
	return log, resultCode, apiData
	

# ----------------------------------------------------------------------------------------------------------------------
#
# Appends string to log message with nextline
#
# <Aruguments> 	- log (string) 		- current log string
#				- newText (string) - New text to be added
#
# <Returns>		- log (string) - log message with new text and nextline
#				
# -----------------------------------------------------------------------------------------------------------------------

def addToLog(log, newText):
	
		log = log + str(newText) + '\n'		
		return log

# ----------------------------------------------------------------------------------------------------------------------
#
# Inserts api call event data into event log table.
#
# <Aruguments> 	- url (string) 		- URL of API call without prefix
#				- apiData (dictionary) - Data returned from API call and parsing json
#				- log (string) 		- log message describing steps during call
#				- resultCode (string) 		- Success or Failure
#				- timeElapsed (int) 		- duration of API call
#
# <Returns>		- None
#				
# -----------------------------------------------------------------------------------------------------------------------	
def insertEventLog(url, apiData, log, resultCode, timeElapsed):

	apiName = url.split("/")[0]
	
	newEventRecord = {}

	newEventRecord['event_source'] = 'MES API Call - ' + apiName
	newEventRecord['input_values'] = url
	newEventRecord['output_values'] = str(apiData).replace("'", " ")
	newEventRecord['script_output'] = log
	newEventRecord['event_result'] = resultCode.upper()
	newEventRecord['event_duration'] = timeElapsed

	# create the new event record
	shared.ent.event_logging.db_sql.events.insertNewEvent(newEventRecord)

# ----------------------------------------------------------------------------------------------------------------------
#
# Writes detailed error log.
#
# <Aruguments> 	- url (string) 		- URL of API call without prefix
#				- log (string) 		- log message
#
# <Returns>		- log (string) 		- log message
#				
# -----------------------------------------------------------------------------------------------------------------------	
def writeDetailedErrorLog(url, log):
	
	exc_type, exc_value, exc_traceback = sys.exc_info()
	logError = 	str(traceback.format_exception(exc_type, exc_value, exc_traceback))
	
	apiName = url.split("/")[0]
	
	apiLogLevelsJson = shared.ent.common.db_sql.appProperties.getAppProperty('mesAPI', 'mesAPILogLevels')
	apiLogLevelsDict = system.util.jsonDecode(apiLogLevelsJson)
	mesApiLogLevelsDict = apiLogLevelsDict["mesAPILogLevels"]
	
	# get log level of current api
	if apiName in mesApiLogLevelsDict:
		apiLogLevel = str(mesApiLogLevelsDict[apiName])
		
		# if log level has DEBUG, log the error in Diagnostic logs
		if "DEBUG" in apiLogLevel.upper():
			logger = system.util.getLogger("APICallLogger")
			logger.error(logError)
			
	log = addToLog(log, "The API call failed.")
	log = addToLog(log, logError)
	
	return log
