# ----------------------------------------------------------------------------------------------	
#
# Author: Janardan Kortikere
# Date: 01/21/2020
#
# File Description: Contains functions that call MES REST API for stacks and units
#
# ----------------------------------------------------------------------------------------------	

# ----------------------------------------------------------------------------------------------------------------------
#
# Calls MES API to get AMR stack move data
#
# <Aruguments> 	- baseboard (string) 		- Id of Baseboard on which stack is present
#				
#
# <Returns>		- apiCallWasSuccess (boolean) - True or False
#				- amrStackData (dictionary) 	- Returns a dictionary object
# -----------------------------------------------------------------------------------------------------------------------
def getAmrStackMoveData(baseboardId):
	
	apiCallWasSuccess = False
	amrStackData = {}
	
	url = "amrStackMoveDetailsByBaseboard/get/" + str(baseboardId)
	
	apiCallWasSuccess, amrStackData = shared.ent.common.api.mes.callMESRestAPI(url, "amr_stack_move_data")
	
	return apiCallWasSuccess, amrStackData

# ----------------------------------------------------------------------------------------------------------------------
#
# Calls MES API to find a location to which Unit Id can be transferred.
#
# <Aruguments> 	- unitId (string) 		- Unit Id which has to be transferred.
#				
#
# <Returns>		- apiCallWasSuccess (boolean) - True or False
#				- locationData (dictionary) 	- Returns a dictionary object
# -----------------------------------------------------------------------------------------------------------------------	
def findUIDLocation(unitId):

	apiCallWasSuccess = False
	locationData = {}

	url = "locationFind/get/" + str(unitId)

	apiCallWasSuccess, locationData = shared.ent.common.api.mes.callMESRestAPI(url, "location_data")

	return apiCallWasSuccess, locationData
	
# ----------------------------------------------------------------------------------------------------------------------
#
# Calls MES API to transfer Unit
#
# <Aruguments> 	- unitId (string) 		- Unit Id being transferred
#				- toLocation (string)	- Destination location
#
# <Returns>		- apiCallWasSuccess (boolean) - True or False
#				- apiData (dictionary) 	- Returns empty dictionary object
# -----------------------------------------------------------------------------------------------------------------------	
def transferUID(unitId, toLocation):
	
		apiCallWasSuccess = False
		apiData = {}
	
		url = "uidTransfer/move/" + str(unitId) + "/" + str(toLocation)
	
		apiCallWasSuccess, apiData = shared.ent.common.api.mes.callMESRestAPI(url, "")
	
		return apiCallWasSuccess, apiData
		
# ----------------------------------------------------------------------------------------------------------------------
#
# Calls MES API to get AMR stack move data
#
# <Aruguments> 	- baseboard (string) 		- Id of Baesboard on which stack is being moved
#				- toLocation (string)		- Destination location			
#
# <Returns>		- apiCallWasSuccess (boolean) - True or False
#				- apiData (dictionary) 	- Returns empty dictionary object
# -----------------------------------------------------------------------------------------------------------------------		
def moveStackToLocation(baseboardId, toLocation):
		
		apiCallWasSuccess = False
		apiData = {}
	
		url = "moveStackToLocation/move/" + str(baseboardId) + "/" + str(toLocation)
	
		apiCallWasSuccess, apiData = shared.ent.common.api.mes.callMESRestAPI(url, "")
	
		return apiCallWasSuccess, apiData
