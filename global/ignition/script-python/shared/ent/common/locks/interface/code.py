# ----------------------------------------------------------------------------------------------	
#
# Author: Andrew Gifford
# Date: 2020/3/9
#
# File Description: simple mutex lock interface
#
#		NOTE: This interface only ensures that one 'process' may have a lock a one time.
#				Deadlocks are not monitored.
#				Locked resources must be explicitly released (dead 'processes' can retain locks)
#
# ----------------------------------------------------------------------------------------------	

import time
import shared.ent.common.locks.db_sql as db


# -----------------------------------------------------------------------------------------------------------------------
#
# Attempts to get lock on a resource
#
# <Aruguments> 	- resource (string) - the name of the resource to be released
#				- owner (string) - the name of the owner holding the lock
#				- timeout (int) - time in seconds before the lock will timeout (no timeout by default)
#
# <Returns>  	- returnCode (int) -	0 : resource lock successfully obtained
#										1 : resource lock already owned
#										2 : resource lock successfully obtained (previous lock timed out)
#									   -1 : resource lock owned by someone other than the supplied owner
#									   -5 : resource not defined
#									 -999 : unknown error
#				- lockOwner (string) - if owned by another, returns the owner's name
#
# -----------------------------------------------------------------------------------------------------------------------
def getResourceLock(resource, owner, timeout=0, uniqueOwner=True, createResource=False, retry=False, retryDelayInit=10, retryDelayFactor=1.5, retryDelayGrowth="mult", retryDelayMax=None, retryLimit=10):
	
	code, currentOwner = db.checkAndSetLock(resource, owner, timeout)
	
	if code == -5 and createResource:
		defineResource(resource)
		code, currentOwner  = db.checkAndSetLock(resource, owner, timeout)
		
	if code == 1 and not uniqueOwner:
		code = -1
	
	if code < 0 and retry:
		
		attempt = 0
		delay = retryDelayInit
		while code < 0 and attempt < retryLimit:
		
			attempt += 1
			time.sleep(delay)
			
			code, currentOwner  = db.checkAndSetLock(resource, owner, timeout)	
			if code == 1 and not uniqueOwner:
				code = -1
			
			if retryDelayGrowth == "mult":
				delay *= retryDelayFactor
			else:
				delay += retryDelayFactor
				
			if retryDelayMax is not None and delay > retryDelayMax:
				delay = retryDelayMax
	
	return code, currentOwner 


# -----------------------------------------------------------------------------------------------------------------------
#
# Attempts to get renew a lock on a resource
#
# <Aruguments> 	- resource (string) - the name of the resource
#				- owner (string) - the name of the owner holding the lock
#				- timeout (int) - time in seconds before the lock will timeout (no timeout by default)
#				- allowTimeout (boolean) - determines if a timed-out lock can be renewed
#
# <Returns>  	- returnCode (int) -	0 : resource lock successfully refreshed
#										3 : resource lock successfully refreshed (lock timed out)
#									   -1 : resource lock owned by someone other than the supplied owner
#									   -2 : resource not locked
#									   -3 : resource lock failed to refresh (lock timed out)
#									   -4 : resource lock owned by someone else (lock timed out)
#									   -5 : resource not defined
#									 -999 : unknown error
#				- lockOwner (string) - if owned by another, returns the owner's name
#
# -----------------------------------------------------------------------------------------------------------------------
def refreshResourceLock(resource, owner, timeout, allowTimeout=1):
	return db.updateResourceLockTimeout(resource, owner, timeout, allowTimeout = allowTimeout)


# -----------------------------------------------------------------------------------------------------------------------
#
# Releases owner's lock on a resource
#
# <Aruguments> 	- resource (string) - the name of the resource to be released
#				- owner (string) - the name of the owner holding the lock
#
# <Returns>  	- returnCode (int) -	0 : resource lock successfully released
#										1 : resource not locked
#									   -1 : resource lock owned by someone other than the supplied owner
#									   -5 : resource not defined
#									 -999 : unknown error
#				- lockOwner (string) - if owned by another, returns the owner's name
#
# -----------------------------------------------------------------------------------------------------------------------
def releaseResourceLock(resource, owner):
	return db.releaseLock(resource, owner)


# -----------------------------------------------------------------------------------------------------------------------
#
# Creates Resource, allowing locks to be obtained for the resource
#
# <Aruguments> 	- resource (string) - the name of the resource to be created
#
# <Returns>  	- returnCode (int) -	0 : resource successfully created
#										1 : resource already defined 
#									 -999 : unknown error
#
# -----------------------------------------------------------------------------------------------------------------------
def defineResource(resource):
	return db.addResource(resource)


# -----------------------------------------------------------------------------------------------------------------------
#
# Destroys resource
#
# <Aruguments> 	- resource (string) - the name of the resource to be destroyed
#
# <Returns>  	- returnCode (int) -	0 : resource successfully destroyed
#										1 : resource not defined 
#									 -999 : unknown error
#
#		NOTE: Should only be used to clean up resources that are no longer used
#
# -----------------------------------------------------------------------------------------------------------------------
def deleteResource(resource):
	return db.removeResource(resource)