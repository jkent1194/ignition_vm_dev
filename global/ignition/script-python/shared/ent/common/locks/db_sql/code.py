

import sys


lockDB = "MANUFACTURING_DATA"


def checkAndSetLock(resource, owner, timeout = 0, lockValue=1):
	
	checkArgs = [resource]
	
	checkQuery = """
	SELECT  [resource_name],
			[lock_status],
			[lock_owner],
			CASE 
				WHEN [lock_timeout_timestamp] IS NULL THEN CAST('True' AS BIT)
				WHEN [lock_timeout_timestamp] >= CURRENT_TIMESTAMP THEN CAST('True' AS BIT)
				ELSE CAST('False' AS BIT)
			END AS Valid
	FROM [dbo].[ent_resource_lock_t] WITH (UPDLOCK)
	WHERE resource_name = ?
	"""
	
	setArgs = [lockValue, owner]
	
	lockTimeout = ""
	if timeout <= 0:
		lockTimeout = "NULL"
	else:
		lockTimeout = "DATEADD(second, ?, CURRENT_TIMESTAMP)"
		setArgs.append(timeout)
		
	setArgs.append(resource)
	
	setQuery = """
	UPDATE [dbo].[ent_resource_lock_t]
	SET lock_status = ?,
		lock_owner = ?,
		lock_timeout_timestamp = {timeout}
	WHERE resource_name = ?
	""".format(timeout = lockTimeout)
	
	tx = None
	
	try:
		tx = system.db.beginTransaction(database = lockDB)
		
		current = system.db.runPrepQuery(checkQuery, database = lockDB, tx = tx, args = checkArgs)
		
		if current.getRowCount() == 0:
			# resource not defined
			return -5, None
			
		else:
			if current.getValueAt(0, 'lock_status') > 0:
				if str(current.getValueAt(0, 'lock_owner')) == str(owner) and current.getValueAt(0, 'valid'):
					#resource already locked by requester
					return 1, owner
				elif not current.getValueAt(0, 'valid'):
					# previous lock timed out
					system.db.runPrepUpdate(setQuery, database = lockDB, tx = tx, args = setArgs)
					system.db.commitTransaction(tx)
					return 2, None
				else:
					# resource locked by another
					return -1, current.getValueAt(0, 'lock_owner')
				
			else:
				# resource available
				system.db.runPrepUpdate(setQuery, database = lockDB, tx = tx, args = setArgs)
				system.db.commitTransaction(tx)
				return 0, None
				
	except:
		# could not obtain lock
		err = sys.exc_info()
		return -999, str(err[0]) + str([1])
		
	finally:
		try:
			system.db.closeTransaction(tx)
		except:
			pass


def updateResourceLockTimeout(resource, owner, timeout=0, lockValue=1, allowTimeout=1):
	
	checkArgs = [resource]
	
	checkQuery = """
	SELECT  [resource_name],
			[lock_status],
			[lock_owner],
			CASE 
				WHEN [lock_timeout_timestamp] IS NULL THEN CAST('True' AS BIT)
				WHEN [lock_timeout_timestamp] >= CURRENT_TIMESTAMP THEN CAST('True' AS BIT)
				ELSE CAST('False' AS BIT)
			END AS Valid
	FROM [dbo].[ent_resource_lock_t] WITH (UPDLOCK)
	WHERE resource_name = ?
	"""
	
	setArgs = []
	
	lockTimeout = ""
	if timeout <= 0:
		lockTimeout = "NULL"
	else:
		lockTimeout = "DATEADD(second, ?, CURRENT_TIMESTAMP)"
		setArgs.append(timeout)
		
	setArgs.append(resource)
	
	setQuery = """
	UPDATE [dbo].[ent_resource_lock_t]
	SET lock_timeout_timestamp = {timeout}
	WHERE resource_name = ?
	""".format(timeout = lockTimeout)
	
	tx = None
	
	try:
		tx = system.db.beginTransaction(database = lockDB)
		
		current = system.db.runPrepQuery(checkQuery, database = lockDB, tx = tx, args = checkArgs)
		
		if current.getRowCount() == 0:
			# resource not defined
			return -5, None
			
		else:
			if current.getValueAt(0, 'lock_status') > 0:
				if str(current.getValueAt(0, 'lock_owner')) == str(owner) and current.getValueAt(0, 'valid'):
					# update timestamp
					system.db.runPrepUpdate(setQuery, database = lockDB, tx = tx, args = setArgs)
					system.db.commitTransaction(tx)
					return 0, owner
				elif str(current.getValueAt(0, 'lock_owner')) != str(owner):
					# resource locked by another
					if current.getValueAt(0, 'valid'):
						return -1, str(current.getValueAt(0, 'lock_owner'))
					else:
						# lock expired
						return -4, str(current.getValueAt(0, 'lock_owner'))
				else:
					# lock expired
					if allowTimeout == 1:
						system.db.runPrepUpdate(setQuery, database = lockDB, tx = tx, args = setArgs)
						system.db.commitTransaction(tx)
						return 3, owner
					else:
						return -3, owner
				
			else:
				# resource not locked
				return -2, None
				
	except:
		# could not obtain lock
		err = sys.exc_info()
		return -999, str(err[0]) + str([1])
		
	finally:
		try:
			system.db.closeTransaction(tx)
		except:
			pass


def releaseLock(resource, owner, lockValue=0):
	
	checkQuery = """
	SELECT *
	FROM [dbo].[ent_resource_lock_t] WITH (UPDLOCK)
	WHERE resource_name = ?
	"""
	
	setQuery = """
	UPDATE [dbo].[ent_resource_lock_t]
	SET lock_status = ?,
		lock_owner = '',
		lock_timeout_timestamp = NULL
	WHERE resource_name = ?
	"""
	
	checkArgs = [resource]
	
	setArgs = [lockValue, resource]
	
	tx = None
	
	try:
		tx = system.db.beginTransaction(database = lockDB)
		
		current = system.db.runPrepQuery(checkQuery, database = lockDB, tx = tx, args = checkArgs)
		
		if current.getRowCount() == 0:
			# resource not defined
			return -5, None
			
		else:
			if current.getValueAt(0, 'lock_status') <= 0:
				# resource not locked
				return 1, None
				
			elif str(current.getValueAt(0, 'lock_owner')) != str(owner):
				# lock owned by another
				return -1, current.getValueAt(0, 'lock_owner')
				
			else:
				# resource available
				system.db.runPrepUpdate(setQuery, database = lockDB, tx = tx, args = setArgs)
				system.db.commitTransaction(tx)
				return 0, None
				
	except:
		# could not release lock
		return -999, None
		
	finally:
		try:
			system.db.closeTransaction(tx)
		except:
			pass


def addResource(resource):
	
	checkQuery = """
	SELECT *
	FROM [dbo].[ent_resource_lock_t]
	WHERE resource_name = ?
	"""
	
	insert = """
	INSERT INTO [dbo].[ent_resource_lock_t]
	(resource_name, lock_status, lock_owner)
	VALUES (?, 0, '')
	"""
	
	try:
		result = system.db.runPrepQuery(checkQuery,
										database = lockDB,
										args = [resource])
		if result.getRowCount() == 0:
			# define resource
			system.db.runPrepUpdate(insert,
									database = lockDB,
									args = [resource])
			return 0
		else:
			# resource already defined
			return 1
	except:
		# error
		return -999


def removeResource(resource):
	
	checkQuery = """
	SELECT *
	FROM [dbo].[ent_resource_lock_t]
	WHERE resource_name = ?
	"""
	
	delete = """
	DELETE FROM [dbo].[ent_resource_lock_t]
	WHERE resource_name = ?
	"""
	
	try:
		result = system.db.runPrepQuery(checkQuery,
										database = lockDB,
										args = [resource])
		if result.getRowCount() == 1:
			# remove resource
			system.db.runPrepUpdate(delete,
									database = lockDB,
									args = [resource])
			return 0
		else:
			# resource not found
			return 1
	except:
		# error
		return -999