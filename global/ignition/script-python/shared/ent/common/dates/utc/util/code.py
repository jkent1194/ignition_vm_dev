def convertTimestampToUTC(inDate):

	if inDate is None:
		return None
		
	utcOffset = system.date.getTimezoneOffset(inDate)
	millisOffset = utcOffset * 60 * 60 * 1000
	return system.date.addMillis(inDate, 0-int(millisOffset))
	