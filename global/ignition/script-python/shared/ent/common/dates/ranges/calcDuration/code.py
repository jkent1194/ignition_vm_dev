# ----------------------------------------------------------------------------------------------	
#
# Author: Janardan Kortikere
# Date: 02/20/2020
#
# File Description: Contains functions to handle date time ranges and duraton.
#
# ----------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
#
# Funtion for calculating total duration from date time ranges ignoring overlaps.
#
# <Aruguments> 	- listDateRangesDict (list) - List of date time ranges, each range is a dictionary
#				
#
# <Returns>		- totalDuration (float) - Duration in seconds ignoring overlaps of date ranges
#
#				Note - This is meant to be used for date and time ranges within one hour.
#				
# -----------------------------------------------------------------------------------------------------------------------			
def calcDuration(listDateRangesDict):

	totalDuration = 0.0
	
	if len(listDateRangesDict) < 1:
		return totalDuration
	
	dateCoverArray = []
	for dateRangeDict in listDateRangesDict:

		startTime = dateRangeDict["start_time"]
		startTimeMillis = system.date.toMillis(startTime)
		endTime = dateRangeDict["end_time"]
		endTimeMillis = system.date.toMillis(endTime)
		
		dateCoverArray.append([startTimeMillis, 1])
		dateCoverArray.append([endTimeMillis + 1, -1])
		
	dateCoverDatasetHeader = ["dateMillis", "cover"]
	dateCoverDataset = system.dataset.toDataSet(dateCoverDatasetHeader, dateCoverArray)
	
	dateCoverDatasetSorted = system.dataset.sort(dateCoverDataset, "dateMillis")
	
	pyDateCoverSorted = system.dataset.toPyDataSet(dateCoverDatasetSorted)
	
	distinctDates = []
	listDateCoverGrouped = []
	
	for pyDateCoverSortedRow in pyDateCoverSorted:
	
		dateMillis = pyDateCoverSortedRow["dateMillis"]
		dateStr = str(dateMillis)
		
		coverSum = 0
		
		if not (dateStr in distinctDates):
		
			distinctDates.append(dateStr)
			for pyDateCoverSortedRow1 in pyDateCoverSorted:
				dateMillis1 = pyDateCoverSortedRow1["dateMillis"]
				dateStr1 = str(dateMillis1)
				if dateStr1 == dateStr:
					coverSum = coverSum + pyDateCoverSortedRow1["cover"]
			listDateCoverGrouped.append([dateMillis, coverSum])
	
	listDateCoverAccum = []
	coverAccum = 0
	
	listDateCoverGroupedLen = len(listDateCoverGrouped)
	
	for i in range(listDateCoverGroupedLen):
	
		dateCoverGrp = listDateCoverGrouped[i]
		currDateAccum = dateCoverGrp[0]
		currCover = dateCoverGrp[1]
		coverAccum = coverAccum + currCover
		
		if coverAccum == 0:
			continue
		
		if i >= (listDateCoverGroupedLen -1):
			continue
			
		nextDateCoverGrp = listDateCoverGrouped[i+1]
		nextDateAccum = nextDateCoverGrp[0]

		listDateCoverAccum.append([currDateAccum, nextDateAccum, coverAccum])
		durationMS = (nextDateAccum - currDateAccum) - 1
		totalDuration = totalDuration + (durationMS/1000.0)
					
	return round(totalDuration)
