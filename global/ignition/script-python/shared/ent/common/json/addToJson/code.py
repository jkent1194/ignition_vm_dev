# ----------------------------------------------------------------------------------------------	
#
# Author: Janardan Kortikere
# Date: 02/05/2020
#
# File Description: Contains functions to add element/subelement to json string/object
#
# ----------------------------------------------------------------------------------------------

# -----------------------------------------------------------------------------------------------------------------------
#
#  Adds a new path/value to a dictionary decoded from JSON
#
# <Aruguments> 	- jsonDict (dictionary) 		- dictionary to which new path/value should be added
#				- pathToAdd (string)
#				- valueToAdd (string)
#
# <Returns>     - result (boolean) - True or False
#				- dictionary (dictionary) with added new path/value
#
#	NOTE:  	will not add new path/value if error occurs
#
# -----------------------------------------------------------------------------------------------------------------------
def addToJson(jsonDict, pathToAdd, valueToAdd):

	result = False

	# convert the path string input into a list using the '/' as a delimiter
	pathToAddList = pathToAdd.split("/")

	# initlialize
	subElement = jsonDict	
	subElementLevel = 0
	fullPathReached = False

	# loop though sub levels
	for currentPath in pathToAddList:
	
		if len(currentPath) > 0:
 
			subElementLevel += 1
			
			if subElementLevel == len(pathToAddList):
				fullPathReached = True
		
			try:
	
				subElement = subElement[currentPath]
	
				if fullPathReached and not isinstance(subElement, dict):
					# value already exixts
					result = False
					break
					
				result = True
			
			# dictionary key missing, handle error
			except:
	
				if fullPathReached:
					subElement[currentPath] = valueToAdd
	
					result = True
					
				else:
					subElement[currentPath] = {}
					
				subElement = subElement[currentPath]
		
		# the current path was not valid, do not proceed
		else:
		
			break
			
	return result, jsonDict
	
# -----------------------------------------------------------------------------------------------------------------------
#
#  Adds a new path/value to a JSON string
#
# <Aruguments> 	- jsonString (string) 		- the JSON dataset as a string to which new path/value should be added
#				- pathToAdd (string)
#				- valueToAdd (string)
#
# <Returns>     - result (boolean) - True or False
#				- newJsonString (string) with added new path/value
#
#	NOTE:  	will not add new path/value if error occurs
#
# -----------------------------------------------------------------------------------------------------------------------
def addToJsonString(jsonString, pathToAdd, valueToAdd):

	newJsonString = jsonString
	jsonDict = system.util.jsonDecode(jsonString)
	
	result, returnJsonDict = addToJson(jsonDict, pathToAdd, valueToAdd)	
	
	if result:
		newJsonString = system.util.jsonEncode(returnJsonDict)
	
	return result, newJsonString