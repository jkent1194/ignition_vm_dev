# ----------------------------------------------------------------------------------------------	
#
# Author: Janardan Kortikere
# Date: 02/05/2020
#
# File Description: Contains functions to compare Json strings/objects
#
# ----------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
#
# Class for comparing Json
#
# -----------------------------------------------------------------------------------------------------------------------
class Diff(object):
	def __init__(self, first, second, with_values=False):
		self.difference = []
		self.pathsAndValues = []
		self.check(first, second, with_values=with_values)

	def check(self, first, second, path='', with_values=False):
		if with_values and second != None:
			if not isinstance(first, type(second)):
				message = '%s - %s, %s' % (path, type(first).__name__, type(second).__name__)
				self.save_diff(message, 'TYPE_MISMATCH', path, first, second)
	
		if isinstance(first, dict):
			for key in first:
				# the first part of path must not have trailing dot.
				if len(path) == 0:
					new_path = key
				else:
					new_path = "%s.%s" % (path, key)

				sec = None
				
				if isinstance(second, dict):
					if second.has_key(key):
						sec = second[key]
					else:
						#  there are key in the first, that is not presented in the second
						self.save_diff(new_path, 'PATH_REMOVED', new_path, first, 'None')
	
						# prevent further values checking.
						sec = None
			
				# recursive call
				if sec != None:					
					self.check(first[key], sec, path=new_path, with_values=with_values)
				else:
					# second is not dict. every key from first goes to the difference#
					self.save_diff(new_path, 'PATH_REMOVED', new_path, first, 'None')
					# uncomment next line if all children of missing path is required			
					#self.check(first[key], second, path=new_path, with_values=with_values)
	
		# if object is list, loop over it and check.
		elif isinstance(first, list):
			for (index, item) in enumerate(first):
				new_path = "%s[%s]" % (path, index)
				# try to get the same index from second
				sec = None
				if second != None:
					try:
						sec = second[index]
					except (IndexError, KeyError):
						# goes to difference
						self.save_diff('%s - %s' % (new_path, type(item).__name__), 'TYPE_MISMATCH', new_path, first, sec)
	
			# recursive call
			self.check(first[index], sec, path=new_path, with_values=with_values)
	
		# not list, not dict. check for equality (only if with_values is True) and return.
		else:
			if with_values and second != None:
				if first != second:
					self.save_diff('%s - %s | %s' % (path, first, second), 'VALUE_CHANGED', path, first, second)

		return
	
	def save_diff(self, diff_message, type_, path, first, second):
		if (type_, diff_message) not in self.difference:
			diffEntryDict = {}
			diffEntryDict['path'] = path.replace(".", "/")
			diffEntryDict['firstValue'] = first
			diffEntryDict['secondValue'] = second
			diffEntryDict['type'] = type_
			diffEntryDict['message'] = diff_message			
			
			self.pathsAndValues.append(diffEntryDict)
			self.difference.append((type_, diff_message))

# -----------------------------------------------------------------------------------------------------------------------
#
# Gets differences between 2 json strings
#
# <Arguments>	- strJson1 (string) first json in string form
#				- strJson1 (string) second json in string form 					
#
# <Returns>		- diffs (list) - all differences in a list, contains mismatch type and message
#				- diffsPathsAndValues (list of dictionary) - all differences in a list, contains 
#					dictionary of path, first jason value, second json value, mismatch type and message
# -----------------------------------------------------------------------------------------------------------------------	
def compareJsonStrings(strJson1, strJson2):

	json1 = system.util.jsonDecode(strJson1)
	json2 = system.util.jsonDecode(strJson2)
	
	return compareJsonDicts(json1, json2)
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Gets differences between 2 json dictionaries
#
# <Arguments>	- json1 (dictionary) first json in dictionary form
#				- json2 (dictionary) second json in dictionary form 					
#
# <Returns>		- diffs (list) - all differences in a list, contains mismatch type and message
#				- diffsPathsAndValues (list of dictionary) - all differences in a list, contains 
#					dictionary of path, first jason value, second json value, mismatch type and message
# -----------------------------------------------------------------------------------------------------------------------
def compareJsonDicts(json1, json2):
		
	diff1 = Diff(json1, json2, True)
	diff2 = Diff(json2, json1, False)
	diffs = []
	diffsPathsAndValues = []
	for type, message in diff1.difference:
		diffs.append({'type': type, 'message': message})
			
	for type, message in diff2.difference:
		diffs.append({'type': 'PATH_ADDED', 'message': message})
		
	for diff in diffs:
		print diff['type'] + ': ' + diff['message']
		
	for diffEntryDict in diff1.pathsAndValues:
		diffsPathsAndValues.append(diffEntryDict)
		
	for diffEntryDict in diff2.pathsAndValues:
		diffEntryDict['type'] = "PATH_ADDED"				
		diffsPathsAndValues.append(diffEntryDict)
	
#	for diffPathsAndValues in diffsPathsAndValues:
#		print (str(diffPathsAndValues['path']) + ': ' + str(diffPathsAndValues['firstValue']) + ': ' 
#			+ str(diffPathsAndValues['secondValue']) + ': ' + str(diffPathsAndValues['type']) + ': ' 
#			+ str(diffPathsAndValues['message']))
		
	return diffs, diffsPathsAndValues