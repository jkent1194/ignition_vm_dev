# ----------------------------------------------------------------------------------------------	
#
# Author: Janardan Kortikere
# Date: 02/06/2020
#
# File Description: Contains functions to remove element from json string/object
#
# ----------------------------------------------------------------------------------------------

# -----------------------------------------------------------------------------------------------------------------------
#
#  Removes a path from a JSON string
#
# <Aruguments> 	- jsonString (string) 		- the JSON dataset as a string from which path should be removed
#				- pathToRemove (string)
#
# <Returns>     - result (boolean) - True or False
#				- dictionary (dictionary) with removed path
#
#	NOTE:  	will not remove path if error occurs
#
# -----------------------------------------------------------------------------------------------------------------------
def removeFromJson(jsonDict, pathToRemove):

	result = False

	# convert the path string input into a list using the '/' as a delimiter
	pathToRemoveList = pathToRemove.split("/")
	
	lenPathToRemoveList = len(pathToRemoveList);
	
	if (lenPathToRemoveList == 1):
		if pathToRemove in jsonDict:

			del jsonDict[pathToRemove]
			result = True		
	else:
		lastSubElementName = pathToRemoveList[lenPathToRemoveList - 1]
		
		# remove last sub element
		pathToRemoveList.pop(lenPathToRemoveList - 1)
	
		subElement = jsonDict
		subElementLevel = 0
		fullPathReached = False
	
		# loop through sub levels
		for currentPath in pathToRemoveList:
			
			subElementLevel += 1
			
			if subElementLevel == (lenPathToRemoveList - 1):
				fullPathReached = True
	
			try:
	
				subElement = subElement[currentPath]
				
				if fullPathReached:
					if lastSubElementName in subElement:
						del subElement[lastSubElementName]
						result = True
						break
					
			# dictionary key missing, handle error
			except:		
				break
	
	return result, jsonDict


# -----------------------------------------------------------------------------------------------------------------------
#
#  Removes a path from a JSON string
#
# <Aruguments> 	- jsonString (string) 		- the JSON dataset as a string from which path should be removed
#				- pathToRemove (string)
#
# <Returns>     - result (boolean) - True or False
#				- newJsonString (string) with added new path/value
#
#	NOTE:  	will not remove path if error occurs
#
# -----------------------------------------------------------------------------------------------------------------------
def removeFromJsonString(jsonString, pathToRemove):

	newJsonString = jsonString
	jsonDict = system.util.jsonDecode(jsonString)
	
	result, returnJsonDict = removeFromJson(jsonDict, pathToRemove)	
	
	if result:
		newJsonString = system.util.jsonEncode(returnJsonDict)
	
	return result, newJsonString