# ----------------------------------------------------------------------------------------------	
#
# Author: Ken burr
# Date: 02/17/2020
#
# File Description: Contains functions to update an element/subelement in a json string/object
#
# ----------------------------------------------------------------------------------------------





# -----------------------------------------------------------------------------------------------------------------------
#
#  Updates the path/value in a dictionary decoded from JSON
#
# <Aruguments> 	- jsonDict (dictionary) 		- dictionary in which the path/value should be updated
#				- pathToUpdate (string)
#				- updatedValue (string)
#
# <Returns>     - result (boolean) - True or False
#				- updatedDict (dictionary) with the updated path/value
#
#
# -----------------------------------------------------------------------------------------------------------------------
def updateJson(jsonDict, pathToUpdate, updatedValue):

	result = False
	updatedDict = jsonDict
	
	# first, remove the existing value at the path/value
	removeJsonResult, jsonDict = shared.ent.common.json.removeFromJson.removeFromJson(jsonDict, updatedValue)
	
	if removeJsonResult:
	
		print jsonDict
	
		# second, add the udpated value back in at the path
		addJsonResult, jsonDict = shared.ent.common.json.addToJson.addToJson(jsonDict, pathToUpdate, updatedValue)
		
		if addJsonResult:
		
			result = True
			updatedDict = jsonDict
		
	return result, updatedDict
	
	
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
#  Updates the path/value in a JSON string
#
# <Aruguments> 	- jsonString (string) 		- the JSON dataset as a string in which the path/value should be updated
#				- pathToUpdate (string)
#				- updatedValue (string)
#
# <Returns>     - result (boolean) - True or False
#				- updatedJsonString (string) with the updated path/value
#
#
# -----------------------------------------------------------------------------------------------------------------------
def updateJsonString(jsonString, pathToUpdate, updatedValue):

	result = False
	updatedJsonString = jsonString
	
	# first, remove the existing value at the path/value
	removeJsonResult, jsonString = shared.ent.common.json.removeFromJson.removeFromJsonString(jsonString, pathToUpdate)
	
	if removeJsonResult:
	
		# second, add the udpated value back in at the path
		addJsonResult, jsonString = shared.ent.common.json.addToJson.addToJsonString(jsonString, pathToUpdate, updatedValue)
		
		if addJsonResult:
		
			result = True
			updatedJsonString = jsonString
	
	return result, updatedJsonString
	
	
	
	
	
	
	
	
	
	
	
	