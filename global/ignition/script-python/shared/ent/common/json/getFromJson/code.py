
# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 2020/2/3
#
# Description: Supporting functions for handling JSON string data in Ignition
#
# ----------------------------------------------------------------------------------------------	








# -----------------------------------------------------------------------------------------------------------------------
#
# Retrieves a property value from a JSON string
#
# <Aruguments> 	- propertyString (string) 	- a string that defines the path of the property in the JSON strign desired
#				- jsonString (string) 		- the JSON dataset as a string
#
# <Returns>     - value (string) 			- the value found
##
# -----------------------------------------------------------------------------------------------------------------------
def getJsonProp(propertyString, jsonString):

	result = False
	resultValue = ''

	dictionary = system.util.jsonDecode(jsonString)

	# convert the property string input into a list using the '/' as a delimiter
	propertyList = propertyString.split("/")

	# create an empty object to retain state
	subElement = dictionary
	
	currentPropPath = ''

	if propertyString != '':

		# retrieve the value
		for property in propertyList:
				
			currentPropPath += '/' + property
			
			try:
				
				subElement = subElement[property]
				
				result = True
				resultValue = subElement
			
			# dictionary key missing, handle error
			except:
			
				print 'could not find the property at path: ' + currentPropPath
				
				result = False
				resultValue = ''
				
				break
				
	else:
		
		result = True
		resultValue = jsonString

	return result, resultValue