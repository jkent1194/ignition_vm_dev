# ----------------------------------------------------------------------------------------------	
#
# Author: Janardan Kortikere
# Date: 02/06/2020
#
# File Description: Contains functions to handle errors.
#
# ----------------------------------------------------------------------------------------------
import sys, traceback
import java.lang.Exception as JavaExcp

# ----------------------------------------------------------------------------------------------------------------------
#
# Funtion for catching errors and logging the details.
#
# <Aruguments> 	- loggerPropetyName (string) - Property name for getting the logger related to the failed process
#				
#
# <Returns>		- logError (string) - Details of error caught in the failure
#				
# -----------------------------------------------------------------------------------------------------------------------			
def logErrorInDiagnostics(loggerPropetyName):
	
		exc_type, exc_value, exc_traceback = sys.exc_info()		
		logError = str(traceback.format_exception(exc_type, exc_value, exc_traceback))
		if logError:
		
			loggerName = shared.ent.common.db_sql.appProperties.getAppProperty('all', loggerPropetyName)
		
			if not loggerName:
				loggerName = loggerPropetyName
			
			logger = system.util.getLogger(loggerName)		
			logger.error(logError)

		
#		JavaExcpetion handling		
#		logError = str(traceback.format_exception(exc_type, exc_value, exc_traceback)).split(',')
		
#		emsg = e.getStackTrace()
#		for m in emsg:
#			print m
#		print "\n\n"
#		print e.getCause()
#		print "\n\n"
#		print e.getLocalizedMessage()
#		print "\n\n"
#		print e.getMessage()
#		print "\n\n"
#		for m in logError:
#			print m	
		
		return logError
		
# ----------------------------------------------------------------------------------------------------------------------
#
# Funtion for logging information.
#
# <Aruguments> 	- loggerPropetyName (string) - Property name for getting the logger
#				
#
# <Returns>		- None
#				
# -----------------------------------------------------------------------------------------------------------------------			
def logInfoInDiagnostics(loggerPropetyName, infoMsg):
	
		if infoMsg and loggerPropetyName:
		
			loggerName = shared.ent.common.db_sql.appProperties.getAppProperty('all', loggerPropetyName)
		
			if not loggerName:
				loggerName = loggerPropetyName
			
			logger = system.util.getLogger(loggerName)		
			logger.info(infoMsg)
	
