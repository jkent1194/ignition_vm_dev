

PARAM_LIMIT = 2000
DEFAULT_BATCH_SIZE = 1000


def batchInsert(baseInsert, insertLine, records, db, batchSize=DEFAULT_BATCH_SIZE):
	
	if len(records) > 0:
		i = 0
		
		insert = ""
		
		for r in records:
		
			if i % batchSize == 0:
				if i > 0:
					system.db.runUpdateQuery(insert, database = db)
				
				insert = baseInsert
				
				insert += insertLine.format(**r) if type(r) == type({}) else insertLine.format(*r)
				
			else:
				insert += "," + insertLine.format(**r) if type(r) == type({}) else insertLine.format(*r)
				
			i += 1
			
		system.db.runUpdateQuery(insert, database = db)


def batchInsertParameterized(baseInsert, insertLine, records, db, batchSize=DEFAULT_BATCH_SIZE):
	
	paramsPerRecord = insertLine.count("?")
	
	if paramsPerRecord * batchSize > PARAM_LIMIT:
		batchSize = PARAM_LIMIT // paramsPerRecord
	
	if len(records) > 0:
		i = 0
		
		insert = ""
		args = []
		
		for r in records:
		
			if i % batchSize == 0:
				if i > 0:
					system.db.runPrepUpdate(insert, args = args, database = db)
				
				insert = baseInsert
				
				insert += insertLine
				
				args = []
				args.extend(r)
				
			else:
				insert += "," + insertLine
				args.extend(r)
				
			i += 1
			
		system.db.runPrepUpdate(insert, args = args, database = db)

