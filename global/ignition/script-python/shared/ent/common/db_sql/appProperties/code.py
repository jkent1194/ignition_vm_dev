# ----------------------------------------------------------------------------------------------	
#
# Author: Janardan Kortikere
# Date: 01/31/2020
#
# File Description: Contains functions to get application properties.
#
# ----------------------------------------------------------------------------------------------

# -----------------------------------------------------------------------------------------------------------------------
#
# Get application properties
#
# <Arguments>	- appName (sring) 		Name of application for filtering properties retrieved
#				- propertyName (sring)	Name of application property for filtering properties retrieved	
#									
#
# <Returns>		- successFlag (bool) - Indicates the existance of the record in the db
#				- appPropsList (list) 	- all application properties as a list of dictionary objects
#
# -----------------------------------------------------------------------------------------------------------------------
def getAppProperties(appName, propertyName):

	successFlag = False
	resultData = {}
	appPropsList = {}
	
	whereClause = ""
	if propertyName:
		whereClause =  " WHERE ap.property_name = '" + propertyName +  "'"
		if appName:
			whereClause +=  " AND ap.app_name = '" + appName +  "'"
	elif appName:
		whereClause =  " WHERE ap.app_name = '" + appName +  "'"
	
	preparedQuery = """

		SELECT 

			ap.app_name,
			ap.property_name,
			ap.property_type,
			ap.property_value

			FROM [dbo].[ent_app_property_t] ap

		""" + whereClause +  """
		
		ORDER BY ap.property_name ASC

	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, "MANUFACTURING_DATA")
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) >= 1:
		
		successFlag = True
		
		# convert the dataset to a dictionary object
		appPropsList = shared.ent.common.db_sql.util.convertDatasetMultiRow(resultData)
		
		
	return successFlag, appPropsList
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Get application property
#
# <Arguments>	- appName (sring) 		Name of application for filtering properties retrieved
#				- propertyName (sring)	Name of application property for filtering properties retrieved								
#
# <Returns>		- appPropertyValue (string) 	- property value
#
# -----------------------------------------------------------------------------------------------------------------------
def getAppProperty(appName, propertyName):

	successFlag, appPropsDict = getAppProperties(appName, propertyName)
	
	appPropertyValue = ""
	
	if successFlag:
		appPropertyValue = appPropsDict[0]["property_value"]
		
	return appPropertyValue
