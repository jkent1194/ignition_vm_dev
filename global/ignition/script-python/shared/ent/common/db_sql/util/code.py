# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 05/01/2019
#
# File Description: Functions that convert between Ignition's Dataset object and Python's Dictionary object
#
# ----------------------------------------------------------------------------------------------	




# -----------------------------------------------------------------------------------------------------------------------
#
# Converts a single row dataset to a Python dictionary object
#
# <Aruguments> 	- dataset - the dataset to convert
#
# <Returns>		- dictObj - the resulting dictionary object
#
# -----------------------------------------------------------------------------------------------------------------------
def convertDatasetSingleRow(dataset):

	dictObj = {}

	# convert the dataset to Python dictionary object for ease of use in calling code
	pyResultData = system.dataset.toPyDataSet(dataset)
	columnNames = system.dataset.getColumnHeaders(pyResultData)
	
	for row in pyResultData:
	
		for column in columnNames:
		
			# print column + ": " + str(row[column])
			
			dictObj[column] = str(row[column])
			
	return dictObj	
	



# -----------------------------------------------------------------------------------------------------------------------
#
# Converts a "flat" (no nested key-value pairs) PyDictionary to an Ignition Dataset object
#
# <Aruguments> 	- dictionary - the dictionary to convert
#
# <Returns>		- datasetObj - the resulting dataset object
#
# -----------------------------------------------------------------------------------------------------------------------
def convertDictionarySingleRow(pyDictionary):

	datasetObj = []
	
	# Generate the rows for the dataset
	rowData = []
	allRowData = []
	headers = []
	
	for prop in pyDictionary:
	
		headers.append(prop)
	
		rowData.append(str(pyDictionary[prop]))

	allRowData.append(rowData)
	  
	# Generate the dataset object
	datasetObj = system.dataset.toDataSet(headers, allRowData)
			
	return datasetObj





# -----------------------------------------------------------------------------------------------------------------------
#
# Converts a multi row dataset to a Python list that contains a Python dictionary object for each row of the dataset
#
# <Aruguments> 	- dataset - the dataset to convert
#
# <Returns>		- listOfDictObjects - the resulting list of dictionary objects
#
# -----------------------------------------------------------------------------------------------------------------------
def convertDatasetMultiRow(dataset):

	listOfDictObjects = []

	# convert the dataset to Python dictionary object for ease of use in calling code
	pyResultData = system.dataset.toPyDataSet(dataset)
	columnNames = system.dataset.getColumnHeaders(pyResultData)
	
	for row in pyResultData:
	
		rowDictObj = {}
	
		for column in columnNames:
		
			# print column + ": " + str(row[column])
			
			rowDictObj[column] = str(row[column])
			
			# print rowDictObj
		
		listOfDictObjects.append(rowDictObj)	
		
		# print listOfDictObjects


	return listOfDictObjects	


# -----------------------------------------------------------------------------------------------------------------------
#
# Converts a multi row dataset to a Python list that contains a Python dictionary object for each row of the dataset
#
# <Aruguments> 	- dataset - the dataset to convert
#
# <Returns>		- listOfDictObjects - the resulting list of dictionary objects
#
# -----------------------------------------------------------------------------------------------------------------------
def convertDatasetMultiRowRetainType(dataset):

	listOfDictObjects = []

	# convert the dataset to Python dictionary object for ease of use in calling code
	pyResultData = system.dataset.toPyDataSet(dataset)
	columnNames = system.dataset.getColumnHeaders(pyResultData)
	
	for row in pyResultData:
	
		rowDictObj = {}
	
		for column in columnNames:
		
			# print column + ": " + str(row[column])
			
			rowDictObj[column] = row[column]
			
			# print rowDictObj
		
		listOfDictObjects.append(rowDictObj)	
		
		# print listOfDictObjects


	return listOfDictObjects	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Converts a single row dataset to a Python dictionary object
#
# <Aruguments> 	- dataset - the dataset to convert
#
# <Returns>		- dictObj - the resulting dictionary object
#
# -----------------------------------------------------------------------------------------------------------------------
def convertDatasetSingleRowRetainType(dataset):

	dictObj = {}

	# convert the dataset to Python dictionary object for ease of use in calling code
	pyResultData = system.dataset.toPyDataSet(dataset)
	columnNames = system.dataset.getColumnHeaders(pyResultData)
	
	for row in pyResultData:
	
		for column in columnNames:
		
			# print column + ": " + str(row[column])
			
			dictObj[column] = row[column]
			
	return dictObj	


# -----------------------------------------------------------------------------------------------------------------------
#
# Creates a new unique string that can be used as a unique identifier
#
# -----------------------------------------------------------------------------------------------------------------------
def generateUid():

	from java.util import UUID
	
	uid = UUID.randomUUID().toString()

	return uid
	
	
	
	
def threadSleep(sleepTime):

	import time
	
	time.sleep(sleepTime)