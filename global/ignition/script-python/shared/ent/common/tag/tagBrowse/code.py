# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 02/05/2020
#
# File Description: Contains functions to add element/subelement to json string/object
#
# ----------------------------------------------------------------------------------------------





# -----------------------------------------------------------------------------------------------------------------------
#
#  Searches a tag directory for all tags in the parent folder path, and all sub folders within
#
# <Aruguments> 	- tagSearchPath	(string)	- the parent tag path to begin the search
#											- example path: "[ZMM_Default]_dev_kab_HermanMiller/Greenhouse/Chair_Assembly/Aeron_Chair_Assembly/Line/Shift"
#
# <Returns>     - tagSet (List) 			- a list of tag paths found
#
# -----------------------------------------------------------------------------------------------------------------------
def manualRecursiveTagSearch(tagSearchPath):

	tagSetString = []
	
	# If troubleshooting, you could print out the initPath here
	
	# NOTE:  The browseTags() function is called with the recursive flag set to false per IA recommendation.  This 
	#			distributes the tag provider workload out across multiple calls when working with deep tag heirarchies.
	
	# Create a result set of just tags. This call could be modified to look for a specific sub of tags,
	# such as just UDT instances
	tagResult = system.tag.browse(path = tagSearchPath, filter = {'valueSource':"memory"}) #browseTags(parentPath = tagSearchPath, tagPath = '*', tagType = 'MEMORY', recursive=False)
		
	tagSet = [tag['fullPath'] for tag in tagResult.results if str(tag['tagType']) != "Folder"]
	
	# print "tags only"
	# for tag in tagSet:
	# print tag
	
	# Create a result set of just tag folders within the searched tag path.               
	
	folderSet = [tag['fullPath'] for tag in tagResult.results if str(tag['tagType']) == "Folder"]                           
	#folderSet = system.tag.browse(path = tagSearchPath, filter = {'tagType':"Folder"}) #browseTags(parentPath = tagSearchPath, tagPath = '*', tagType = 'Folder', recursive=False)
	
	# print "folders only"
	# for folder in folderSet:
	# print folder
	
	# Iterate through the folders paths getting sub folder tags and folders                            
	for folder in folderSet:
	 
		# ...And start the process over again until we run out of folders.
		tagSet += manualRecursiveTagSearch(folder)
	
	# convert the data to raw string paths
	for tag in tagSet:
	
		tagSetString.append(str(tag))
	
	# Return the list of tags.
	return tagSet
	
	
	
	
def getTagsRecursive(tagSearchPath):
	
	tagSet = manualRecursiveTagSearch(tagSearchPath)
	
	tagSetString = []
	
	# convert the data to raw string paths
	for tag in tagSet:
	
		tagSetString.append(tag)
		
	# Return the list of tags.
	return tagSetString
		
	