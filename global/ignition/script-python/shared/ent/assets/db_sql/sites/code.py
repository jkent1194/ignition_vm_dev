# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 11/13/2019
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	



	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# <Aruguments> 	- newSiteObj 			- a dictionary object containing the new site information
#
# <Returns>		- success <bool> 		- returns a true or false result indicating a successful insert
#
# -----------------------------------------------------------------------------------------------------------------------
def insertSite(newSiteObj):

	result = ''
	resultData = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
		
	preparedQuery = """

		INSERT INTO [dbo].[ent_assets_v2_sites_t]
		(
			[site_identifier]
			,[site_description]
			,[last_updated]
		)
		VALUES
		(
			'""" + str(newSiteObj['site_identifier']) + """'
			,'""" + str(newSiteObj['site_description']) + """'
			,GETDATE()
		)
		"""
		
	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)
		
		result = True
		
	except:
	
		print "Insert new site record failed to execute."
		
		print preparedQuery
	
			
	return result
	
	
	
	


	

# -----------------------------------------------------------------------------------------------------------------------
#
# Get all sites from database
#
# <Arguments>	- outputType (string) Indicates whether a dictionary object, or a dataset is desired "1" for dictionary, 
#										any other input value will return a dataset
#
# <Returns>		- result (bool) - Indicates the existance of at least one record in the db
#				- data object 	- resulting data in either a dictionary object or a dataset object, as selected by the input argument
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllSites(outputType):

	result = ''
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	
	preparedQuery = """

		SELECT *
		
		FROM [dbo].[ent_assets_v2_sites_t]

	"""
	
	# print preparedQuery

	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) >= 1:
		
		result = True
		
		# convert the dataset to a dictionary object
		recordDictObj = shared.ent.assets.db_sql.misc.convertDatasetMultiRow(resultData)
		
	else:

		# print "no sites were found."
			
		result = False
		
	
	if outputType == '1':
	
		return result, recordDictObj
	
	else:
	
		return result, resultData
	



# -----------------------------------------------------------------------------------------------------------------------
#
# Gets the parents of an site
#
# <Arguments>	- areaId		- the ID of the line of interest
#
# <Returns>		- result (bool) - Indicates the existance of at least one record in the db
#				- resultData 	- resulting data in a dictionary object
#
# -----------------------------------------------------------------------------------------------------------------------
def getSite(siteId):

	result = False
	resultData = ''
	recordDictObj = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	
	preparedQuery = """

		SELECT 

			s.site_id
			,s.site_identifier

	  FROM [dbo].[ent_assets_v2_sites_t] s
	  
	  WHERE s.[site_id] = '""" + str(siteId) + """'

	"""
	
	# print preparedQuery

	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) == 1:
		
		result = True
		
		# convert the dataset to a dictionary object
		recordDictObj = shared.ent.assets.db_sql.misc.convertDatasetSingleRow(resultData)
		
	else:

		result = False
		
	
	return result, recordDictObj
