

def getAllAssets():
	
	query = """
	SELECT
		S.[site_id],
		S.[site_identifier],
		S.[site_description],
		A.[area_id],
		A.[area_identifier],
		A.[area_description],
		L.[line_id],
		L.[line_identifier],
		L.[line_description],
		C.[cell_id],
		C.[cell_identifier],
		C.[cell_description]
	  FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_sites_t] S
	  LEFT JOIN [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_areas_t] A
		ON S.site_id = A.site_id
	  LEFT JOIN [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_lines_t] L
		ON A.area_id = L.area_id
	  LEFT JOIN [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_cells_t] C
		ON L.line_id = C.line_id
	  ORDER BY S.site_id, A.area_id, L.line_id, C.cell_id
	"""
	
	try:
		resultDS = system.db.runQuery(query, database = "MANUFACTURING_DATA")
	
		return resultDS
	
	except:
		log = system.util.getLogger("global.assets")
		log.error("Error running query: " + query)
		
		return None

	