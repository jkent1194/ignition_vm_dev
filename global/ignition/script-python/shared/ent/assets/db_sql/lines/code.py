# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 11/13/2019
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	



	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# <Aruguments> 	- newLineObj 			- a dictionary object containing the new line information
#
# <Returns>		- success <bool> 		- returns a true or false result indicating a successful insert
#
# -----------------------------------------------------------------------------------------------------------------------
def insertLine(newLineObj):

	result = ''
	resultData = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	
	
	preparedQuery = """
	
		INSERT INTO [dbo].[ent_assets_v2_lines_t]
           (
			   [area_id]
			   ,[line_identifier]
			   ,[line_description]
			   ,[last_updated]
		   )
     VALUES
           (
			   '""" + str(newLineObj['area_id']) + """'
			   ,'""" + str(newLineObj['line_identifier']) + """'
			   ,'""" + str(newLineObj['line_description']) + """'
			   ,GETDATE()
			)		
	"""
		
	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)
		
		result = True
		
	except:
	
		print "Insert new line record failed to execute."
		
		print preparedQuery
			
	return result	
	
	
	
	

		
# -----------------------------------------------------------------------------------------------------------------------
#
# Get all lines from database
#
# <Arguments>	- outputType (string) Indicates whether a dictionary object, or a dataset is desired "1" for dictionary, 
#										any other input value will return a dataset
#
# <Returns>		- result (bool) - Indicates the existance of at least one record in the db
#				- data object 	- resulting data in either a dictionary object or a dataset object, as selected by the input argument
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllLines(outputType):

	result = ''
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	
	preparedQuery = """

		SELECT *
		
		FROM [dbo].[ent_assets_v2_lines_t]

	"""
	
	# print preparedQuery

	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) >= 1:
		
		result = True
		
		# convert the dataset to a dictionary object
		recordDictObj = shared.ent.assets.db_sql.misc.convertDatasetMultiRow(resultData)
		
	else:

		# print "no lines were found."
			
		result = False
		
	
	if outputType == '1':
	
		return result, recordDictObj
	
	else:
	
		return result, resultData
		
		
		


# -----------------------------------------------------------------------------------------------------------------------
#
# Get all lines from database
#
# <Arguments>	- areaId (string)		- the area for which the lines are desired
#
#				- outputType (string) 	- Indicates whether a dictionary object, or a dataset is desired "1" for dictionary, 
#											any other input value will return a dataset
#
# <Returns>		- result (bool) - Indicates the existance of at least one record in the db
#				- data object 	- resulting data in either a dictionary object or a dataset object, as selected by the input argument
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllLinesByArea(areaId, outputType):

	result = ''
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	
	preparedQuery = """

		SELECT *
		
		FROM [dbo].[ent_assets_v2_lines_t]
		
		WHERE [area_id] = '""" + str(areaId) + """'

	"""
	
	# print preparedQuery

	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) >= 1:
		
		result = True
		
		# convert the dataset to a dictionary object
		recordDictObj = shared.ent.assets.db_sql.misc.convertDatasetMultiRow(resultData)
		
	else:

		# print "no lines were found."
			
		result = False
		
	
	if outputType == '1':
	
		return result, recordDictObj
	
	else:
	
		return result, resultData

	
	

	
# -----------------------------------------------------------------------------------------------------------------------
#
# Gets the parents of a line
#
# <Arguments>	- lineId		- the ID of the line of interest
#
# <Returns>		- result (bool) - Indicates the existance of at least one record in the db
#				- resultData 	- resulting data in a dictionary object
#
# -----------------------------------------------------------------------------------------------------------------------
def getParents(lineId):

	result = False
	resultData = ''
	recordDictObj = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	
	preparedQuery = """

		SELECT 

			l.line_id
			,l.line_identifier

			,a.area_id
			,a.area_identifier

			,s.site_id
			,s.site_identifier

	  FROM [dbo].[ent_assets_v2_lines_t] l
	  JOIN [dbo].[ent_assets_v2_areas_t] a ON (l.area_id = a.area_id)
	  JOIN [dbo].[ent_assets_v2_sites_t] s ON (a.site_id = s.site_id)
	  
	  WHERE l.[line_id] = '""" + str(lineId) + """'

	"""
	
	# print preparedQuery

	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) == 1:
		
		result = True
		
		# convert the dataset to a dictionary object
		recordDictObj = shared.ent.assets.db_sql.misc.convertDatasetSingleRow(resultData)
		
	else:

		result = False
		
	
	return result, recordDictObj