# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 2/3/2020
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	



	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Creates an asset properties record for an asset ID and type
#
# <Arguments>	- assetId (string)			- the asset id for the asset of interest
#				- assetType (string)		- the asset type of the asset of interest
#				- newProperties				- a JSON string representing all of the assets properties
#
# <Returns>		- result (bool) 			- Indicates a successful record retrieval		
#
# -----------------------------------------------------------------------------------------------------------------------
def createAssetProperty(assetId, assetType, newProperties, appName = 'all', newPropertyName = 'main'):

	result = False
	resultData = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.assets.db_sql.config.getTablePrefix()
	
	preparedQuery = """
	
		INSERT INTO [dbo].[""" + tablePrefix + """asset_properties_t]
			(
				[asset_type]
				,[asset_id]
				,[app_name]
				,[property_name]
				,[property_string]
				,[last_updated]
			)
		VALUES
			(
				'""" + str(assetType) + """'
				,'""" + str(assetId) + """'
				,'""" + str(appName) + """'
				,'""" + str(newPropertyName) + """'
				,'""" + str(newProperties) + """'
				,GETDATE()
			)

	"""
		
	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)
		
		result = True
		
	except:
	
		print "Failed to create a new asset properties record for asset type: " + str(assetType) + ", id: " + str(assetId)
		
		print preparedQuery
			
	return result	
	
	
	
	
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Creates an an empty asset properties record for an asset ID and type
#
# <Arguments>	- assetId (string)			- the asset id for the asset of interest
#				- assetType (string)		- the asset type of the asset of interest
#
# <Returns>		- result (bool) 			- Indicates a successful record retrieval		
#
# -----------------------------------------------------------------------------------------------------------------------
def createAssetPropertyEmpty(assetId, assetType, appName = 'all', propertyName = 'main'):

	result = False
	resultData = {}
	
	newProperties = '{}'
	
	result = shared.ent.assets.db_sql.asset_properties.createAssetProps(assetId, assetType, appName, propertyName, newProperties)
			
	return result		
	
	
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Updates an existing asset's properties record by asset ID and type
#
# <Arguments>	- assetId (string)			- the asset id for the asset of interest
#				- assetType (string)		- the asset type of the asset of interest
#				- updatedProperties			- a JSON string representing all of the assets properties
#
# <Returns>		- result (bool) 			- Indicates a successful record retrieval		
#
# -----------------------------------------------------------------------------------------------------------------------
def updateAssetProperty(assetId, assetType, updatedProperties, appName = 'all', propertyName = 'main'):

	result = False
	resultData = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.assets.db_sql.config.getTablePrefix()
	
	preparedQuery = """
	
		UPDATE [dbo].[""" + tablePrefix + """asset_properties_t]
		
		SET 
		
			[property_string] = '""" + str(updatedProperties) + """'
			,[last_updated] = GETDATE()
			
		WHERE [asset_type] = '""" + str(assetType) + """'
			AND [asset_id] = '""" + str(assetId) + """'
			AND [property_name] = '""" + str(propertyName) + """'
			AND [app_name] =  '""" + str(appName) + """'

	"""
		
	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)
		
		result = True
		
	except:
	
		print "Failed to update the asset properties record for asset type: " + str(assetType) + ", id: " + str(assetId)
		
		print preparedQuery
			
	return result	


		
		
		


# -----------------------------------------------------------------------------------------------------------------------
#
# Get an asset property
#
# <Arguments>	- assetId (string)			- the asset id for the asset of interest
#				- assetType (string)		- the asset type of the asset of interest
#				- outputType (string)		- the desired format for the returned data, '1' for a dictionary, anything else returns a JSON string
#
# <Returns>		- result (bool) 			- Indicates a successful record retrieval
#				- resultData				- the asset properties in the format requested, either JSON or a Dictionary object			
#
# -----------------------------------------------------------------------------------------------------------------------
def getAssetProperty(assetId, assetType, outputType, appName = 'all', propertyName = 'main'):

	result = False
	resultData = []
	recordDictObj = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.assets.db_sql.config.getTablePrefix()
	
	preparedQuery = """

		SELECT *
		
		FROM [dbo].[""" + tablePrefix + """asset_properties_t]
		
		WHERE [asset_id] = '""" + str(assetId) + """'
			AND [asset_type] = '""" + str(assetType) + """'
			AND [app_name] =  '""" + str(appName) + """'
			AND [property_name] = '""" + str(propertyName) + """'
	"""
	
	# print preparedQuery

	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) == 1:
		
		# convert the dataset to a dictionary object
		recordDictObj = shared.ent.assets.db_sql.misc.convertDatasetSingleRow(resultData)
		
		result = True
		
	else:

		# print "no properties record was found for asset type: " + str(assetType) + ", id: " + str(assetId)
			
		result = False
		
	
	if outputType == '1':
	
		return result, recordDictObj
	
	else:
	
		return result, resultData

	
	

	
# -----------------------------------------------------------------------------------------------------------------------
#
# Get cell asset and it's parent asset heirarchy
#
# <Arguments>	- cellId (string)		- the cell if of the asset of interest
#
#				- outputType (string) 	- Indicates whether a dictionary object, or a dataset is desired "1" for dictionary, 
#											any other input value will return a dataset
#
# <Returns>		- result (bool) - Indicates the existance of at least one record in the db
#				- data object 	- resulting data in either a dictionary object or a dataset object, as selected by the input argument
#
# -----------------------------------------------------------------------------------------------------------------------
def getCellAssetHeirarachy(cellId, outputType):

	result = ''
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.assets.db_sql.config.getTablePrefix()
	
	preparedQuery = """
	
		SELECT 
		
			[cell_id]
			,[cell_identifier]
			,[line_id]
			,[line_identifier]
			,[area_id]
			,[area_identifier]
			,[site_id]
			,[site_identifier]
			
		FROM [dbo].[""" + str(tablePrefix) + """assets_v]
		
		WHERE [cell_id] = '""" + str(cellId) + """'

	"""
	
	# print preparedQuery

	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) == 1:
		
		result = True
		
	else:

		result = False
		
	
	if outputType == '1':
			
		# convert the dataset to a dictionary object
		recordDictObj = shared.ent.assets.db_sql.misc.convertDatasetSingleRow(resultData)
		
		return result, recordDictObj
	
	else:
	
		return result, resultData