# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 2/10/2020
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	



	
	
# -----------------------------------------------------------------------------------------------------------------------
#
#	Adds a single Baan user association with a cell to the database
#
# <Aruguments> 	- newBaanUser 		- the new Baan user name to add
#				- cellId 			- the cell id to associate the new Baan user with
#
# <Returns>		- success <bool> 	- returns a true or false result indicating a successful insert
#
# -----------------------------------------------------------------------------------------------------------------------
def insertBaanUser(newBaanUser, cellId):

	result = False
	resultData = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.assets.db_sql.config.getTablePrefix()
	
	preparedQuery = """
	
		INSERT INTO [dbo].[""" + tablePrefix + """baan_users_t]
			(
				[baan_user_name]
				,[cell_id]
				,[last_updated]
			)
		VALUES
			(
				'""" + str(newBaanUser) + """'
				 ,'""" + str(cellId) + """'
				,GETDATE()
			)
	
	"""
		
	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)
		
		result = True
		
	except:
	
		print "Insert new Bann user record failed to execute."
		
		print preparedQuery
			
	return result	
	
	
	
	



# -----------------------------------------------------------------------------------------------------------------------
#
#	Adds multiple Baan user associations with a cell to the database
#
# <Aruguments> 	- newBaanUserList <List> 	- a list of new Baan user names to add
#				- cellId 					- the cell id to associate the new Baan user with
#
# <Returns>		- success <bool> 	- returns a true or false result indicating a successful insert
#
# -----------------------------------------------------------------------------------------------------------------------
def insertMultipleBaanUsers(newBaanUserList, cellId):

	result = False
	resultData = {}
	successfulInserts = 0
	
	for user in newBaanUserList:
	
		insertResult = insertBaanUser(user, cellId)
		
		if insertResult:
			
			successfulInserts += 1
		
		else:
		
			print 'failed to insert the Baan user: ' + str(user) + ' for cell id: ' + str(cellId)
			
			
	if successfulInserts == len(newBaanUserList):
	
		result = True

	return result	
	
		





# -----------------------------------------------------------------------------------------------------------------------
#
# 	Updates Baan user associations with a cell.  Can operate on a list of new users.
#
# <Aruguments> 	- newBaanUserList <List> 	- a list of new Baan user names to add
#				- cellId 					- the cell id to associate the new Baan user with
#
# <Returns>		- success <bool> 	- returns a true or false result indicating a successful insert
#
# -----------------------------------------------------------------------------------------------------------------------
def updateBaanUsersByCell(newBaanUserList, cellId):

	result = False

	# first delete the existing Baan user records
	deleteResult = deleteBaanUsersByCell(cellId)
	
	if deleteResult:
	
		# insert the new Baan user records
		insertNewResult = insertMultipleBaanUsers(newBaanUserList, cellId)
	
		if insertNewResult:
		
			result = True

	return result	


		
		



# -----------------------------------------------------------------------------------------------------------------------
#
# 	Deletes all Baan users associated with a cell
#
# <Returns>		- result (bool) - Indicates the successful update of the db
#
# -----------------------------------------------------------------------------------------------------------------------
def deleteBaanUsersByCell(cellId):

	result = False
	resultData = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.assets.db_sql.config.getTablePrefix()

	preparedQuery = """

		DELETE FROM [dbo].[""" + tablePrefix + """baan_users_t]

		WHERE cell_id = '""" + str(cellId) + """'
	
	"""
	
	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)
		
		result = True
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		

	return result






# -----------------------------------------------------------------------------------------------------------------------
#
# Get all Baan users associated with a cell from the database
#
# <Arguments>	- cellId (string)		- the cell for which the Baan user associations are desired
#
#				- outputType (string) 	- Indicates whether a dictionary object, or a dataset is desired "1" for dictionary, 
#											any other input value will return a dataset
#
# <Returns>		- result (bool) - Indicates the existance of at least one record in the db
#				- data object 	- resulting data in either a dictionary object or a dataset object, as selected by the input argument
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllBaanUsersByCell(cellId, outputType):

	result = ''
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	tablePrefix = shared.ent.assets.db_sql.config.getTablePrefix()
	
	preparedQuery = """

		SELECT *
		
		FROM [dbo].[""" + tablePrefix + """baan_users_t]
		
		WHERE [cell_id] = '""" + str(cellId) + """'

	"""
	
	# print preparedQuery

	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) >= 1:
		
		result = True
		
		# convert the dataset to a dictionary object
		recordDictObj = shared.ent.assets.db_sql.misc.convertDatasetMultiRow(resultData)
		
	else:

		# print "no cells were found."
			
		result = False
		
	
	if outputType == '1':
	
		return result, recordDictObj
	
	else:
	
		return result, resultData

	
	

	

	