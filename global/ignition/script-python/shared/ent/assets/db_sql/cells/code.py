# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 2/20/2020
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	



	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# <Aruguments> 	- newCellObj 			- a dictionary object containing the new cell information
#
# <Returns>		- success <bool> 		- returns a true or false result indicating a successful insert
#
# -----------------------------------------------------------------------------------------------------------------------
def insertCell(newCellObj):

	result = ''
	resultData = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	
	preparedQuery = """
	
		INSERT INTO [dbo].[ent_assets_v2_cells_t]
           (
			   [line_id]
			   ,[cell_identifier]
			   ,[cell_description]
			   ,[last_updated]
		   )
     VALUES
           (
			   '""" + str(newCellObj['line_id']) + """'
			   ,'""" + str(newCellObj['cell_identifier']) + """'
			   ,'""" + str(newCellObj['cell_description']) + """'
			   ,GETDATE()
			)		
	"""
		
	# send the query to the database
	try:
	
		resultData = system.db.runUpdateQuery(preparedQuery, database)
		
		result = True
		
	except:
	
		print "Insert new cell record failed to execute."
		
		print preparedQuery
			
	return result	
	
	
	
	




		
# -----------------------------------------------------------------------------------------------------------------------
#
# Get all cells from database
#
# <Arguments>	- outputType (string) Indicates whether a dictionary object, or a dataset is desired "1" for dictionary, 
#										any other input value will return a dataset
#
# <Returns>		- result (bool) - Indicates the existance of at least one record in the db
#				- data object 	- resulting data in either a dictionary object or a dataset object, as selected by the input argument
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllCells(outputType):

	result = ''
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	
	preparedQuery = """

		SELECT *
		
		FROM [dbo].[ent_assets_v2_cells_t]

	"""
	
	# print preparedQuery

	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) >= 1:
		
		result = True
		
		# convert the dataset to a dictionary object
		recordDictObj = shared.ent.assets.db_sql.misc.convertDatasetMultiRow(resultData)
		
	else:

		# print "no cells were found."
			
		result = False
		
	
	if outputType == '1':
	
		return result, recordDictObj
	
	else:
	
		return result, resultData
		
		
		


# -----------------------------------------------------------------------------------------------------------------------
#
# Get all cells from database
#
# <Arguments>	- lineId (string)		- the line for which the cells are desired
#
#				- outputType (string) 	- Indicates whether a dictionary object, or a dataset is desired "1" for dictionary, 
#											any other input value will return a dataset
#
# <Returns>		- result (bool) - Indicates the existance of at least one record in the db
#				- data object 	- resulting data in either a dictionary object or a dataset object, as selected by the input argument
#
# -----------------------------------------------------------------------------------------------------------------------
def getAllCellsByLine(lineId, outputType):

	result = ''
	resultData = {}
	recordDictObj = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	
	preparedQuery = """

		SELECT *
		
		FROM [dbo].[ent_assets_v2_cells_t]
		
		WHERE [line_id] = '""" + str(lineId) + """'

	"""
	
	# print preparedQuery

	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) >= 1:
		
		result = True
		
		# convert the dataset to a dictionary object
		recordDictObj = shared.ent.assets.db_sql.misc.convertDatasetMultiRow(resultData)
		
	else:

		# print "no cells were found."
			
		result = False
		
	
	if outputType == '1':
	
		return result, recordDictObj
	
	else:
	
		return result, resultData

	
	

	


# -----------------------------------------------------------------------------------------------------------------------
#
# Get all cells from database
#
# <Arguments>	- cellId		- the ID of the cell of interest
#
# <Returns>		- result (bool) - Indicates the existance of at least one record in the db
#				- resultData 	- resulting data in a dictionary object
#
# -----------------------------------------------------------------------------------------------------------------------
def getParents(cellId):

	result = False
	resultData = ''
	recordDictObj = {}
	
	database = shared.ent.assets.db_sql.config.getDatabaseName()
	
	preparedQuery = """

		SELECT 

			c.[cell_id]
			,c.[cell_identifier]

			,l.line_id
			,l.line_identifier

			,a.area_id
			,a.area_identifier

			,s.site_id
			,s.site_identifier

	  FROM [dbo].[ent_assets_v2_cells_t] c

	  JOIN [dbo].[ent_assets_v2_lines_t] l ON (l.line_id = c.line_id)
	  JOIN [dbo].[ent_assets_v2_areas_t] a ON (l.area_id = a.area_id)
	  JOIN [dbo].[ent_assets_v2_sites_t] s ON (a.site_id = s.site_id)
	  
	  WHERE c.[cell_id] = '""" + str(cellId) + """'

	"""
	
	# print preparedQuery

	# send the query to the database
	try:
	
		resultData = system.db.runQuery(preparedQuery, database)
		
		# print "Database query successfully executed:"
		# print resultData
		
	except:
	
		print "Database query failed to execute:"
		print preparedQuery
		
		
	# validate the length of the result data
	if len(resultData) == 1:
		
		result = True
		
		# convert the dataset to a dictionary object
		recordDictObj = shared.ent.assets.db_sql.misc.convertDatasetSingleRow(resultData)
		
	else:

		result = False
		
	
	return result, recordDictObj
