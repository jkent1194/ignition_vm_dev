from shared.ent.assets.interface import asset_properties


def getCellTaktTime(cellID, shiftCode=None):
	
	code, props = asset_properties.getAssetPropByAssetId(assetId=cellID, assetType='cell', propertyPath='oee/takt_time', appName = 'all', propertyName = 'main')
		
	if code:
		
		taktTimes = {}
		if 'setpoint_1st_shift' in props:
			taktTimes[1] = float(props['setpoint_1st_shift'])
		if 'setpoint_2nd_shift' in props:
			taktTimes[2] = float(props['setpoint_2nd_shift'])
		if 'setpoint_3rd_shift' in props:
			taktTimes[0] = float(props['setpoint_3rd_shift'])
		
		if shiftCode is None:
			return taktTimes  # system.util.jsonDecode(props)
		elif shiftCode not in taktTimes:
			return 0
		else:
			return taktTimes[shiftCode]
	else:
		return 0


#def getCellTaktTime(cellID):
#
#	code, props = asset_properties.getAssetPropByAssetId(assetId=cellID, assetType='cell', propertyPath='oee/takt_time/setpoint_1st_shift', appName = 'all', propertyName = 'main')
#	
#	if code:
#		return system.util.jsonDecode(props)
#	else:
#		return 0



def getCellOEEProps(cellID):
	code, props = asset_properties.getAssetPropByAssetId(assetId=cellID, assetType='cell', propertyPath='', appName = 'all', propertyName = 'oee')
	
	if code:
		return system.util.jsonDecode(props)
	else:
		return {'type':''}


def getKeyCells(lineID):

	keyQuery = """
	SELECT property_string
	FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_asset_properties_t]
	WHERE 
		asset_id = ?
		AND asset_type = 'line'
		AND app_name = 'all'
		AND property_name = 'key_cells'
	"""
	
	result = system.db.runPrepQuery(keyQuery, database = "MANUFACTURING_ASSETS", args = [lineID])
	
	if result.getRowCount() > 0:
		if result.getValueAt(0,0) != "":
			return [int(x) for x in result.getValueAt(0,0).split(',')]
		else:
			return []
	else:
		return []
	

def getLineOR(lineID, start, end):
	
	keyCells = getKeyCells(lineID)
	
	if len(keyCells) > 0:
		return getCellOR(keyCells[0], start, end)
	else:
		return 0
		
	
def getLineActualCompletions(lineID, start, end):
	
	keyCells = getKeyCells(lineID)
	
	if len(keyCells) > 0:
		return getCellActualCompletions(keyCells[0], start, end)
	else:
		return 0

	
def getLineExpectedCompletions(lineID, start, end):
	
	keyCells = getKeyCells(lineID)
	
	if len(keyCells) > 0:
		return getCellExpectedCompletions(keyCells[0], start, end)
	else:
		return 0
		
		
def getLineWorkingMinutes(lineID, start, end):
	
	args = [start, start, end, end, lineID, start, end, start, end, start, end, 
			start, start, end, end, lineID, start, end, start, end, start, end]
	
	query = """
	SELECT A.duration - B.duration
	FROM
	(
		SELECT 
			SUM(DATEDIFF(minute, IIF(P.start_date>?, P.start_date, ?), IIF(P.end_date<?, P.end_date, ?))) AS duration
		FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_periods_t] P
		WHERE
			P.enabled = 1
			AND P.line_id = ?
			AND ((P.start_date >= ? AND P.start_date <= ?)
				OR (P.end_date >= ? AND P.end_date <= ?)
				OR (P.start_date <= ? AND P.end_date >= ?)
				)
	) A 
	JOIN (
		SELECT 
			COALESCE(SUM(DATEDIFF(minute, IIF(P.start_date>?, P.start_date, ?), IIF(P.end_date<?, P.end_date, ?))), 0) AS duration
		FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_idle_periods_t] P
		WHERE
			P.enabled = 1
			AND P.line_id = ?
			AND ((P.start_date >= ? AND P.start_date <= ?)
				OR (P.end_date >= ? AND P.end_date <= ?)
				OR (P.start_date <= ? AND P.end_date >= ?)
				)
	) B
	ON 1=1;
	"""
	
	result = system.db.runPrepQuery(query, database = "MANUFACTURING_DATA", args = args)
	
	if result.getRowCount() > 0 and result.getValueAt(0,0) is not None:
		return result.getValueAt(0,0)
	else:
		return 0


def getCellOR(cellID, start, end):
	
	oeeProps = getCellOEEProps(cellID)
#	print(oeeProps['type'])
	if oeeProps['type'] in ['completions', 'critical_scans']:
		
		query = """
		SELECT 'actual' AS label,
			SUM(completion_count) AS completions
		FROM [MANUFACTURING_DATA].[dbo].[ent_cell_history_t]
		WHERE
			cell_id = ?
			AND start_time >= ?
			AND start_time < ?
		UNION
		SELECT 'expected' AS label,
			SUM(completion_count) AS completions
		FROM [MANUFACTURING_DATA].[dbo].[ent_cell_expected_completion_t]
		WHERE
			cell_id = ?
			AND completion_time > ?
			AND completion_time <= ?
		ORDER BY label ASC
		"""
		
		result = system.db.runPrepQuery(query, database = "MANUFACTURING_DATA", args = [cellID, start, end, cellID, start, end])
		
		if result.getRowCount() < 2:
			return 0
		
		actual = result.getValueAt(0,'completions') if result.getValueAt(0,'completions') is not None else 0
		expected = result.getValueAt(1,'completions') if result.getValueAt(1,'completions') is not None else 0
		
		if expected != 0:
			return (actual * 100.0) / expected
		else:
			return -1
			
	elif oeeProps['type'] in ['MMR', 'MDE', 'andiPLC']:
		
		try:
		
			query = """
			SELECT prod / total * 100.0 FROM (
			SELECT CAST(SUM(datediff(second, start_time, end_time)) AS FLOAT(53)) AS prod
			FROM dbo.ent_cell_history_t
			WHERE   cell_id = ?
				AND start_time >= ?
				AND start_time < ?
				AND ([state] = 'Production' OR [state] = 'Handling')
				AND [is_planned_downtime] = 0) AS A
			JOIN (
			SELECT SUM(datediff(second, start_time, end_time)) AS total
			FROM dbo.ent_cell_history_t
			WHERE   cell_id = ?
				AND start_time >= ?
				AND start_time < ?
				AND [is_planned_downtime] = 0) AS B
				ON total != 0
			"""
			
			ORds = system.db.runPrepQuery(query, database = "MANUFACTURING_DATA", args = [cellID, start, end, cellID, start, end])
	#		print([cellID, start, end, cellID, start, end])
			if ORds.getRowCount() > 0:
	#			print(ORds.getValueAt(0,0))
				return ORds.getValueAt(0,0)
			else:
				return 0
		except:
			return -1
		
	else:
		return 0




def getCellActualCompletions(cellID, start, end):
	
	query = """
	SELECT SUM(completion_count)
	FROM [MANUFACTURING_DATA].[dbo].[ent_cell_history_t]
	WHERE
		cell_id = ?
		AND start_time >= ?
		AND start_time < ?
	"""
	
	result = system.db.runPrepQuery(query, database = "MANUFACTURING_DATA", args = [cellID, start, end])
	
	if result.getRowCount() > 0 and result.getValueAt(0,0) is not None:
		return result.getValueAt(0,0)
	else:
		return 0
	



def getCellExpectedCompletions(cellID, start, end):
	
	query = """
	SELECT SUM(completion_count)
	FROM [MANUFACTURING_DATA].[dbo].[ent_cell_expected_completion_t]
	WHERE
		cell_id = ?
		AND completion_time > ?
		AND completion_time <= ?
	"""
	
	result = system.db.runPrepQuery(query, database = "MANUFACTURING_DATA", args = [cellID, start, end])
	
	if result.getRowCount() > 0 and result.getValueAt(0,0) is not None:
		return result.getValueAt(0,0)
	else:
		return 0
	
	
	
def getCellWorkingMinutes(cellID, start, end):
	
	args = [start, start, end, end, cellID, start, end, start, end, start, end, 
			start, start, end, end, cellID, start, end, start, end, start, end]
	
	query = """
	SELECT A.duration - B.duration
	FROM
	(
		SELECT 
			SUM(DATEDIFF(minute, IIF(P.start_date>?, P.start_date, ?), IIF(P.end_date<?, P.end_date, ?))) AS duration
		FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_cells_t] C
			JOIN [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_periods_t] P
				ON C.line_id = P.line_id
		WHERE
			P.enabled = 1
			AND C.cell_id = ?
			AND ((P.start_date >= ? AND P.start_date <= ?)
				OR (P.end_date >= ? AND P.end_date <= ?)
				OR (P.start_date <= ? AND P.end_date >= ?)
				)
	) A 
	JOIN (
		SELECT 
			SUM(DATEDIFF(minute, IIF(P.start_date>?, P.start_date, ?), IIF(P.end_date<?, P.end_date, ?))) AS duration
		FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_cells_t] C
			JOIN [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_idle_periods_t] P
				ON C.line_id = P.line_id
		WHERE
			P.enabled = 1
			AND C.cell_id = ?
			AND ((P.start_date >= ? AND P.start_date <= ?)
				OR (P.end_date >= ? AND P.end_date <= ?)
				OR (P.start_date <= ? AND P.end_date >= ?)
				)
	) B
	ON 1=1;
	"""
	
	result = system.db.runPrepQuery(query, database = "MANUFACTURING_DATA", args = args)
	
	if result.getRowCount() > 0 and result.getValueAt(0,0) is not None:
		return result.getValueAt(0,0)
	else:
		return 0
		
		
def getCellTimeInState(cellID, start, end, state):
	
	query = """
	SELECT CAST(SUM(datediff(second, start_time, end_time)) AS FLOAT(53)) AS time
		FROM dbo.ent_cell_history_t
		WHERE   cell_id = ?
			AND start_time >= ?
			AND start_time < ?
			AND [state] = ?
			AND [is_planned_downtime] = 0
	"""
	
	result = system.db.runPrepQuery(query, database = "MANUFACTURING_DATA", args = [cellID, start, end, state])
	
	if result.getRowCount() > 0 and result.getValueAt(0,0) is not None:
		return result.getValueAt(0,0)
	else:
		return 0


def updateLineOEETags(line, lineTagPath, shift):
		
	log = system.util.getLogger('Line_OEE')
#	log.info("processing line: " + str(line))

	keyCells = []

	try:
		keyCells = getKeyCells(line['line_id'])
	except:
		log.error("error getting key cells for line: " + str(line))

	cellOEE = {
		'actualCompletions' : {
			'currentHour' : [],
			'previousHour' : [],
			'shift' : []
		},
		'expectedCompletions' : {
			'currentHour' : [],
			'previousHour' : [],
			'shift' : []
		},
		'OR' : {
			'currentHour' : [],
			'previousHour' : [],
			'shift' : []
		},
	}

	# -------------- FETCH VALUES ------------------

	for keyCellID in keyCells:
		
		getPathResult, cellTagPath = shared.ent.assets.interface.misc.getAssetTagPath(keyCellID, 'Cell')
		
		cellTags = system.tag.readAll([cellTagPath + '/Cell/OEE/Accum/Completions_Actual/currentHour',
									   cellTagPath + '/Cell/OEE/Accum/Completions_Actual/previousHour',
									   cellTagPath + '/Cell/OEE/Accum/Completions_Actual/currentShift',
									   cellTagPath + '/Cell/OEE/Accum/Completions_Expected/currentHour',
									   cellTagPath + '/Cell/OEE/Accum/Completions_Expected/previousHour',
									   cellTagPath + '/Cell/OEE/Accum/Completions_Expected/currentShift',
									   cellTagPath + '/Cell/OEE/OR/currentHour',
									   cellTagPath + '/Cell/OEE/OR/previousHour',
									   cellTagPath + '/Cell/OEE/OR/currentShift'])
									  
		cellOEE['actualCompletions']['currentHour'].append(cellTags[0].value)
		cellOEE['actualCompletions']['previousHour'].append(cellTags[1].value)
		cellOEE['actualCompletions']['shift'].append(cellTags[2].value)
		cellOEE['expectedCompletions']['currentHour'].append(cellTags[3].value)
		cellOEE['expectedCompletions']['previousHour'].append(cellTags[4].value)
		cellOEE['expectedCompletions']['shift'].append(cellTags[5].value)
		cellOEE['OR']['currentHour'].append(cellTags[6].value)
		cellOEE['OR']['previousHour'].append(cellTags[7].value)
		cellOEE['OR']['shift'].append(cellTags[8].value)
		
	# -------------- PROCESS (AGGREGATE) VALUES ------------------
	
#	log.info("lineID: " + line['line_id'] + "\ncellOEE: " + str(cellOEE))
	
	# Sum completion values from key cells
	for period in cellOEE['actualCompletions']:
		cellOEE['actualCompletions'][period] = sum(cellOEE['actualCompletions'][period])
		
	for period in cellOEE['expectedCompletions']:
		cellOEE['expectedCompletions'][period] = sum(cellOEE['expectedCompletions'][period])
		
	# Average OR values from key cells
	for period in cellOEE['OR']:
		cellOEE['OR'][period] = sum(cellOEE['OR'][period]) / (len(cellOEE['OR'][period]) if len(cellOEE['OR'][period]) != 0 else 1)
	
	# -------------- WRITE VALUES ------------------
	
	system.tag.writeAll([lineTagPath + '/Line/OEE/Accum/Completions_Actual/currentHour',
						 lineTagPath + '/Line/OEE/Accum/Completions_Actual/previousHour',
						 lineTagPath + '/Line/OEE/Accum/Completions_Actual/currentShift',
						 lineTagPath + '/Line/OEE/Accum/Completions_Expected/currentHour',
						 lineTagPath + '/Line/OEE/Accum/Completions_Expected/previousHour',
						 lineTagPath + '/Line/OEE/Accum/Completions_Expected/currentShift',
						 lineTagPath + '/Line/OEE/OR/currentHour',
						 lineTagPath + '/Line/OEE/OR/previousHour',
						 lineTagPath + '/Line/OEE/OR/currentShift'],
						[cellOEE['actualCompletions']['currentHour'],
						 cellOEE['actualCompletions']['previousHour'],
						 cellOEE['actualCompletions']['shift'],
						 cellOEE['expectedCompletions']['currentHour'],
						 cellOEE['expectedCompletions']['previousHour'],
						 cellOEE['expectedCompletions']['shift'],
						 cellOEE['OR']['currentHour'],
						 cellOEE['OR']['previousHour'],
						 cellOEE['OR']['shift']])
	
	