# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 2/3/2020
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	



	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Gets an asset property value record for an asset ID and type
#
# <Arguments>	- assetId (string)			- the asset id for the asset of interest
#				- assetType (string)		- the asset type of the asset of interest (site, areea, line, cell)
#				- propertyPath (string)		- the property for which the value is desired
#
# <Returns>		- result (bool) 			- Indicates a successful record retrieval		
#
# -----------------------------------------------------------------------------------------------------------------------
def getAssetPropByAssetId(assetId, assetType, propertyPath, appName = 'all', propertyName = 'main'):

	result = False
	resultValue = ''
	
	# get the asset properties from the database
	getPropsResult, propsDictObj = shared.ent.assets.db_sql.asset_properties.getAssetProperty(assetId, assetType, '1', appName, propertyName)
	
	if getPropsResult:
	
		# get the specific property value from the asset properties
		propsJson = propsDictObj['property_string']
		
		# print propsJson
		
		# get the desired property value from the JSON sring
		getValueResult, propertyValue = shared.ent.common.json.getFromJson.getJsonProp(propertyPath, propsJson)
	
		if getValueResult:
		
			result = getValueResult
			resultValue = propertyValue
	
	# return the result
	return result, resultValue




# -----------------------------------------------------------------------------------------------------------------------
#
# Sets an asset property value record for an asset ID and type
#
# <Arguments>	- assetId (string)			- the asset id for the asset of interest
#				- assetType (string)		- the asset type of the asset of interest (site, areea, line, cell)
#				- propertyPath (string)		- the property to set the value of
#				- newValue (string)			- the new value to set the property to
#
# <Returns>		- result (bool) 			- Indicates a successful record retrieval		
#
# -----------------------------------------------------------------------------------------------------------------------
def setAssetPropByAssetId(assetId, assetType, propertyPath, newValue, appName = 'all', propertyName = 'main'):

	result = False
	
	# get the asset properties from the database
	getPropsResult, propsDictObj = shared.ent.assets.db_sql.asset_properties.getAssetProperty(assetId, assetType, '1', appName, propertyName)
	
	# if the property already exists... update it
	if getPropsResult:
	
		print 'the asset properties record does exist'

		# get the specific property value from the asset properties
		propsJson = propsDictObj['property_string']
		
		# attemtp to read the property at the property path to confirm the presensce of the specific property
		readPropResult, propValue = shared.ent.common.json.getFromJson.getJsonProp(propertyPath, propsJson)
		
		if readPropResult:
		
			print 'the specific property path: ' + str(propertyPath) + ' exists in the existing cell props'
		
			print propsJson
			print newValue
			
			# set the desired property value in the JSON sring
			setValueResult, updatedPropsJson = shared.ent.common.json.updateJson.updateJsonString(propsJson, propertyPath, newValue)
			
			print setValueResult
			print updatedPropsJson
			
			if setValueResult:
			
				# update the asset properties record in the database
				result = shared.ent.assets.db_sql.asset_properties.updateAssetProperty(assetId, assetType, updatedPropsJson, appName, propertyName)
				
		else:
		
			print 'the specific property path: ' + str(propertyPath) + ' does NOT exist in the existing cell props'
			
			print propsJson
			print newValue
			
			# set the desired property value in the JSON sring
			addValueResult, updatedPropsJson = shared.ent.common.json.addToJson.addToJsonString(propsJson, propertyPath, newValue)
			
			print addValueResult
			print updatedPropsJson
			
			if addValueResult:
			
				# update the asset properties record in the database
				result = shared.ent.assets.db_sql.asset_properties.updateAssetProperty(assetId, assetType, updatedPropsJson, appName, propertyName)
			
	# if the property does not yet exist, create it...		
	else:
	
		print 'properties record does not yet exist'
		
		# this is where the default properties set(sets) will be created...  comign soon...

	# return the result
	return result
