

import shared.ent.assets.db_sql.assets as db


def getAllAssets():
	
	assetsDS = db.getAllAssets()
	
	if assetsDS is None:
		return []
	
	assets = []
	
	lastSite = None
	siteIndex = -1
	lastArea = None
	areaIndex = -1
	lastLine = None
	lineIndex = -1
	
	for row in range(assetsDS.getRowCount()):
		
		siteID = assetsDS.getValueAt(row, "site_id")
		
		# add site
		if siteID != lastSite:
			
			siteIdentifier = assetsDS.getValueAt(row, "site_identifier")
			siteDescription = assetsDS.getValueAt(row, "site_description")
			
			siteIndex += 1
			assets.append({'site_id':siteID,
						   'site_identifier':siteIdentifier,
						   'site_description':siteDescription,
						   'areas':[]})
			
			areaIndex = -1
			lastSite = siteID
		
		# add area
		areaID = assetsDS.getValueAt(row, "area_id")
		
		if areaID is None:
			continue
		elif areaID != lastArea:
			
			areaIdentifier = assetsDS.getValueAt(row, "area_identifier")
			areaDescription = assetsDS.getValueAt(row, "area_description")
			
			areaIndex += 1
			assets[siteIndex]['areas'].append({'area_id':areaID,
											   'area_identifier':areaIdentifier,
											   'area_description':areaDescription,
											   'lines':[]})
			
			lineIndex = -1
			lastArea = areaID
		
		# add line
		lineID = assetsDS.getValueAt(row, "line_id")
		
		if lineID is None:
			continue
		elif lineID != lastLine:
			
			lineIdentifier = assetsDS.getValueAt(row, "line_identifier")
			lineDescription = assetsDS.getValueAt(row, "line_description")
			
			lineIndex += 1
			assets[siteIndex]['areas'][areaIndex]['lines'].append({'line_id':lineID,
																   'line_identifier':lineIdentifier,
																   'line_description':lineDescription,
																   'cells':[]})
			
			lastLine = lineID
					
		# add cell
		cellID = assetsDS.getValueAt(row, "cell_id")
		
		if cellID is not None:
			
			cellIdentifier = assetsDS.getValueAt(row, "cell_identifier")
			cellDescription = assetsDS.getValueAt(row, "cell_description")
			
			assets[siteIndex]['areas'][areaIndex]['lines'][lineIndex]['cells'].append({'cell_id':cellID,
																					   'cell_identifier':cellIdentifier,
																					   'cell_description':cellDescription})
		
	return assets

