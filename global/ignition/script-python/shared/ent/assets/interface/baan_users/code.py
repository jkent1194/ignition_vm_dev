



# -----------------------------------------------------------------------------------------------------------------------
#
# Get the tag path for a cell in the Ignition asset tag model
#
# <Arguments>	- cellId (string)		- the cell if of the asset of interest
#
# <Returns>		- cellTagPath (bool) 	- the derived tag path
#
# -----------------------------------------------------------------------------------------------------------------------
def getCellBaseTagPath(cellId):

	cellTagPath = ''
	
	baseTagPath = shared.ent.assets.config.getAssetBaseTagPath()
	
	result, assetData = shared.ent.assets.db_sql.asset_properties.getCellAssetHeirarachy(cellId, '1')
	
	if result:
	
		# establish the tag directory path for this cell
		cellTagPath = baseTagPath + '/' + str(assetData['site_identifier']) + '/' + str(assetData['area_identifier']) + '/' + str(assetData['line_identifier']) + '/' + str(assetData['cell_identifier'])

		# # for the Ignition tag paths, swap the spaces out with underscores
		# cellTagPath = cellTagPath.replace(' ', '_') 
	
	return cellTagPath
	
	
	
	
	
	
# -----------------------------------------------------------------------------------------------------------------------
#
# Get the tag path for an asset in the Ignition asset tag model
#
# <Arguments>	- assetId (string)		- the id of the asset of interest
# 				- assetType (string)	- the type of asset of interest.  ('Cell', 'Line', 'Area', 'Site')
#
# <Returns>		- cellTagPath (bool) 	- the derived tag path
#
# -----------------------------------------------------------------------------------------------------------------------	
def getAssetTagPath(assetId, assetType):
	
	result = False
	tagPath = ''
	
	tagBasePath = shared.ent.assets.config.getAssetBaseTagPath()

	if assetType == 'Cell':
	
		getParentsResult, parentData = shared.ent.assets.db_sql.cells.getParents(assetId)
	
	elif assetType == 'Line':
	
		getParentsResult, parentData = shared.ent.assets.db_sql.lines.getParents(assetId)
	
	elif assetType == 'Area':
	
		getParentsResult, parentData = shared.ent.assets.db_sql.areas.getParents(assetId)
	
	elif assetType == 'Site':
	
		getParentsResult, parentData = shared.ent.assets.db_sql.sites.getSite(assetId)
	
	else:
	
		# invalid assetType passed in
		return result, tagPath
	
		
	if str(getParentsResult) == 'True':
	
		try:
		
			tagPath = tagBasePath + '/' + parentData['site_identifier']
			
			result = True
		
		except:
	
			return result, tagPath
			
		try:
		
			tagPath += '/' + parentData['area_identifier']
		
		except:
	
			return result, tagPath
			
		try:
		
			tagPath += '/' + parentData['line_identifier']
		
		except:
	
			return result, tagPath
			
		try:
		
			tagPath += '/' + parentData['cell_identifier']
		
		except:
	
			return result, tagPath
	
	
	return result, tagPath