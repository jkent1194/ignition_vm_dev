
import shared.ent.common.locks.interface as mutex
from shared.ent.assets.interface import OEE
from shared.ent.assets.interface import asset_properties
import string
import sys
import java.lang.Exception as jException
import shared.ent.common.dates.utc.util.convertTimestampToUTC as convertTimestampToUTC



PROCESS_NAME = "Expected_Completion_Calc"
MUTEX_PREFIX = "Cell_"
MUTEX_SUFFIX = "_Expected_Completions"

#FUTURE_HOURS = 24
BATCH = 1000



def getShiftPeriods(cellID, rebuild=0):
	
	now = system.date.now()
	
#	threshold = system.date.addHours(now, FUTURE_HOURS)
	
	start = getLastBuild(cellID) if rebuild == 0 else now
	
	query = """
	SELECT 
		P.start_date AS shift_start,
		P.end_date AS shift_end,
		P.shift_code,
		I.start_date AS idle_start,
		I.end_date AS idle_end
		FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_cells_t] C
			JOIN [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_periods_t] P
				ON C.line_id = P.line_id
			LEFT JOIN [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_idle_periods_t] I
				ON P.line_id = I.line_id
				AND P.shift_date = I.shift_date
				AND P.shift_code = I.shift_code
				AND I.enabled = 1
	WHERE 
		P.enabled = 1
--		AND P.start_date <= ?
		AND P.end_date > ?
		AND C.cell_id = ?
	ORDER BY
		P.start_date ASC,
		I.start_date ASC
	"""
	
	result = system.db.runPrepQuery(query,
									database = "MANUFACTURING_ASSETS",
									args = [start, cellID])
	
	shifts = []
	last = None
	j = -1
	
	for i in range(result.getRowCount()):
		
		current = [result.getValueAt(i,"shift_start"), result.getValueAt(i,"shift_end")]
		
		if last != current:
			shifts.append({'shift':current, 'shiftCode':result.getValueAt(i,"shift_code"), 'idlePeriods':[]})
			j += 1
		if result.getValueAt(i,"idle_start") is not None:
			shifts[j]['idlePeriods'].append([result.getValueAt(i,"idle_start"), result.getValueAt(i,"idle_end")])
			
		last = current
			
	return shifts


def divideShifts(shifts):

	shiftPeriods = []
	i = -1

	for shift in shifts:
		
		i += 1
		j = 1
		shiftPeriods.append([shift['shiftCode'] , [shift['shift'][0],shift['shift'][1]]])
		
		for idlePeriod in shift['idlePeriods']:
			
			shiftTemp = shiftPeriods[i][j][1]
			shiftPeriods[i][j][1] = idlePeriod[0]
			if shiftTemp > idlePeriod[1]:
				shiftPeriods[i].append([idlePeriod[1],shiftTemp])
				j += 1
	
	return shiftPeriods
	
	
def calcExpectedCompletions(shiftPeriods, taktTime):

	records = []
	#log = system.util.getLogger("exp_cmpl_gen")
	
	for shift in shiftPeriods:
		#log.info("taktTime = " + str(taktTime[shift[0]]))
	
		ts = None
		eTakt = taktTime[shift[0]]
		
		for period in shift[1:]:
			
			ts = period[0]
			
			while system.date.millisBetween(ts, period[1]) >= eTakt*1000:
				
				ts = system.date.addMillis(ts, int(eTakt*1000))
				records.append([ts, 1])
				
				eTakt = taktTime[shift[0]]
			
			eTakt = eTakt - (system.date.millisBetween(ts, period[1])/1000.0)
			
	return records			
	

def insertExpectedCompletions(cellID, records, tx, closeTx = True):
		
	if len(records) > 0:
		i = 0
		
		insert = ''
		
		for r in records:
		
			if i % BATCH == 0:
				if i > 0:
					system.db.runUpdateQuery(insert, database = "MANUFACTURING_DATA", tx = tx)
				
				insert = """
				INSERT INTO [MANUFACTURING_DATA].[dbo].[ent_cell_expected_completion_t]
					([cell_id], [completion_time], [completion_count], [completion_time_utc])
				VALUES
					(%d, '%s', %d, '%s')
				""" % (int(cellID), system.date.format(r[0], 'yyyy-MM-dd HH:mm:ss.SSS'), r[1], system.date.format(convertTimestampToUTC(r[0]), 'yyyy-MM-dd HH:mm:ss.SSS'))
				
			else:
				insert += """,(%d, '%s', %d, '%s')""" % (int(cellID), system.date.format(r[0], 'yyyy-MM-dd HH:mm:ss.SSS'), r[1], system.date.format(convertTimestampToUTC(r[0]), 'yyyy-MM-dd HH:mm:ss.SSS'))
				
			i += 1
			
		system.db.runUpdateQuery(insert, database = "MANUFACTURING_DATA", tx = tx)
	
	if closeTx:
		system.db.commitTransaction(tx)
		system.db.closeTransaction(tx)
	
	
def deleteExpectedCompletions(cellID, shifts, rebuild):

	tx = system.db.beginTransaction(database = "MANUFACTURING_DATA")
	
	if rebuild != 0:
	
		start = shifts[0]['shift'][0] if len(shifts) > 0 else system.date.now()
	
		delete = """
		DELETE FROM [dbo].[ent_cell_expected_completion_t]
		WHERE
			cell_id = ?
			AND completion_time > ?
		"""
		
		system.db.runPrepUpdate(delete,
								database = "MANUFACTURING_DATA",
								args = [cellID, start],
								tx = tx)
	
	return tx


def buildCellExpectedCompletions(cellID, taktTime, rebuild=0):
	
#	code = getLock(cellID)
#	if code != 0:
#		return code
	
	try:
		shifts = getShiftPeriods(cellID, rebuild)
	
		shiftPeriods = divideShifts(shifts)
	
		records = calcExpectedCompletions(shiftPeriods, taktTime)
	
		tx = deleteExpectedCompletions(cellID, shifts, rebuild)
	
		insertExpectedCompletions(cellID, records, tx)
	
#		if len(shifts) > 0:
#			setLastBuild(cellID, shifts[-1]['shift'][1])
	
	except Exception, err:
		err = sys.exc_info()
		#log.warn(' | '.join(err))
		log = system.util.getLogger("exp_compl_gen")
		log.warn("error")
		errMsg = ""
		for part in err:
			errMsg += str(part) + ' | '
		log.warn(errMsg)
		
#	finally:
#		releaseLock(cellID)
	
	return 0
	
	

# shared.ent.shifts.db_sql.shifts.upsertShift line 
def deleteShiftExpectedCompletions(lineID, shiftDate, shiftCode, shift = None, tx = None):

	lineID = int(lineID)
	if isinstance(shiftDate, basestring):
		shiftDate = system.date.parse(shiftDate, "yyyy-MM-dd HH:mm:ss")
	if isinstance(shiftCode, basestring):
		shiftCode = int(shiftCode)
	
	# check if shift enabled
	shiftDetails = None
	if shift is None:
		shiftDetails = getShiftDetails(lineID, shiftDate, shiftCode)
	else:
		shiftDetails = shift
	
	if len(shiftDetails) > 0 and shiftDetails['enabled']:
		
		oeeCells = getLineOEECells(lineID)
		
		delete = """
		DELETE FROM [MANUFACTURING_DATA].[dbo].[ent_cell_expected_completion_t]
		WHERE [completion_time] > ?
			AND [completion_time] <= ?
			AND cell_id IN (%s)
		""" % (cellIdString(oeeCells))
		
		try:
			system.db.runPrepUpdate(delete,
									database = "MANUFACTURING_DATA",
									args = [shiftDetails['start_date'], shiftDetails['end_date']],
									tx = tx)
		
		except:
			return 1
	
	return 0
	

def getSingleShiftPeriods(cellID, shiftDate, shiftCode):
	
	query = """
	SELECT 
		P.start_date AS shift_start,
		P.end_date AS shift_end,
		P.shift_code,
		I.start_date AS idle_start,
		I.end_date AS idle_end
		FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_cells_t] C
			JOIN [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_periods_t] P
				ON C.line_id = P.line_id
			LEFT JOIN [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_idle_periods_t] I
				ON P.line_id = I.line_id
				AND P.shift_date = I.shift_date
				AND P.shift_code = I.shift_code
				AND I.enabled = 1
	WHERE 
		P.enabled = 1
		AND P.shift_date = ?
		AND P.shift_code = ?
		AND C.cell_id = ?
	ORDER BY
		P.start_date ASC,
		I.start_date ASC
	"""
	
	result = system.db.runPrepQuery(query,
									database = "MANUFACTURING_ASSETS",
									args = [shiftDate, shiftCode, cellID])
	
	shifts = []
	last = None
	j = -1
	
	for i in range(result.getRowCount()):
		
		current = [result.getValueAt(i,"shift_start"), result.getValueAt(i,"shift_end")]
		
		if last != current:
			shifts.append({'shift':current, 'shiftCode':result.getValueAt(i,"shift_code"), 'idlePeriods':[]})
			j += 1
		if result.getValueAt(i,"idle_start") is not None:
			shifts[j]['idlePeriods'].append([result.getValueAt(i,"idle_start"), result.getValueAt(i,"idle_end")])
			
		last = current
			
	return shifts


def parseShift(shift):
	
	if not shift['enabled']:
		return []
	
	parsedShift = [{"shift":[shift['start_date'], shift['end_date']], "idlePeriods":[], "shiftCode":shift['shift_code']}]
	
	for idleIndex in shift['idle_periods']:
		if shift['idle_periods'][idleIndex]['enabled']:
			parsedShift[0]['idlePeriods'].append([shift['idle_periods'][idleIndex]['start_date'], shift['idle_periods'][idleIndex]['end_date']])
			
	return parsedShift
	

# shared.ent.shifts.services.generate_shifts.generateDailyShifts line 697
def generateShiftExpectedCompletions(lineID, shiftDate, shiftCode, shift = None, taktTimeList = None, tx = None):

	log = system.util.getLogger("expected_completions")

	if not isinstance(lineID, int):
		lineID = int(lineID)
	if isinstance(shiftDate, basestring):
		shiftDate = system.date.parse(shiftDate, "yyyy-MM-dd HH:mm:ss")
	if isinstance(shiftCode, basestring):
		shiftCode = int(shiftCode)
	
	# check if shift enabled
	oeeCells = getLineOEECells(lineID)
	
	for cell in oeeCells:
		
		cellID = cell['cell_id']
		
		try:
			if taktTimeList is None:
				taktTime = OEE.getCellTaktTime(cellID)
			else:
				taktTime = taktTimeList[cellID]
		
			shifts = None
			if shift is None:
				shifts = getSingleShiftPeriods(cellID, shiftDate, shiftCode)
			else:
				shifts = parseShift(shift)
		
			shiftPeriods = divideShifts(shifts)
		
			records = calcExpectedCompletions(shiftPeriods, taktTime)
		
			if tx is None:
				tx = deleteExpectedCompletions(cellID, shifts, 0)
				insertExpectedCompletions(cellID, records, tx)
			else:
				insertExpectedCompletions(cellID, records, tx, False)
	
		except Exception, e:
			try:
				err = sys.exc_info()
				#log.warn(' | '.join(err))
				if len(err) >= 2:
					log.warn(str(err[0]) + ' | ' + str(err[1]))
				else:
					log.warn(str(err[0]))
			finally:
				return 1
		except jExeption, e:
			log.warn("jException in updateShift")
			log.warn(str(e.cause) + "   " + str(e))
			
			return 1
		
	return 0


# taktTime change
def rebuildFutureShiftExpectedCompletions(cellID, taktTime=None):
	# same rebuild process
	if taktTime is None:
		taktTime = OEE.getCellTaktTime(cellID)
	
	buildCellExpectedCompletions(cellID, taktTime, 1)


# shared.ent.shifts.db_sql.shifts.deleteFutureShifts_byLine
def deleteFutureShiftExpectedCompletions(lineID):

	startTime = getCurrentStartTime(lineID)

	oeeCells = getLineOEECells(lineID)
		
	delete = """
	DELETE FROM [MANUFACTURING_DATA].[dbo].[ent_cell_expected_completion_t]
	WHERE [completion_time] > ?
		AND cell_id IN (%s)
	""" % (cellIdString(oeeCells))
	
	system.db.runPrepUpdate(delete,
							database = "MANUFACTURING_DATA",
							args = [startTime])


def getCurrentStartTime(lineID):
	
	code, tagPath = shared.ent.assets.interface.misc.getAssetTagPath(lineID, 'Line')
	
	qualifiedValues = system.tag.readAll([tagPath + "/Line/Shift/Active/inActiveShiftTime",
										  tagPath + "/Line/Shift/Active/startDate"])
	
	shiftActive = qualifiedValues[0].value
	startDate = qualifiedValues[1].value if shiftActive else system.date.now()
	
	return startDate


def cellIdString(cellList):
	
	cellString = ''
	
	if len(cellList) == 0:
		cellString = '0'
	else:
		cellString = string.join([str(cell['cell_id']) for cell in cellList], ',')
		
	return cellString


def getShiftDetails(lineID, shiftDate, shiftCode):

	query = """
	SELECT  [line_id],
			[shift_date],
			[overtime_start_date],
			[start_date],
			[end_date],
			[overtime_end_date],
			[shift_code],
			[day_of_week],
			[enabled]
	FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_shift_periods_t]
	WHERE [line_id] = ?
		AND [shift_date] = ?
		AND [shift_code] = ?
	"""
	
	shiftDetails = system.db.runPrepQuery(query,
										  database = "MANUFACTURING_ASSETS",
										  args = [lineID, shiftDate, shiftCode])
	
	return dsToList(shiftDetails)[0] if shiftDetails.getRowCount() > 0 else {}


def getLineOEECells(lineID):
	
	query = """
	SELECT cell_id
	FROM [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_cells_t] C
	JOIN [MANUFACTURING_ASSETS].[dbo].[ent_assets_v2_asset_properties_t] P
		ON C.cell_id = P.asset_id
		AND P.asset_type = 'cell'
		AND P.app_name = 'all'
		AND P.property_name = 'oee'
	WHERE
		[line_id] = ?
	"""
	
	oeeCells = system.db.runPrepQuery(query,
									  database = "MANUFACTURING_ASSETS",
									  args = [lineID])
	
	return dsToList(oeeCells)
	
	
def dsToList(ds):
	list = []
	for i in range(ds.getRowCount()):
		list.append({})
		for j in range(ds.getColumnCount()):
			list[i][ds.getColumnName(j)] = ds.getValueAt(i, j)
		
	return list
