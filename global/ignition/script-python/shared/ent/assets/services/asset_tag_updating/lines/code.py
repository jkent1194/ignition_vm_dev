













def calcLineShift(line, lineTagPath, logAllExecutions):

	# create an event log object
	newEventObj = shared.ent.event_logging.db_sql.misc.createEventLogObject('Calc Line Shift Service')

	newEventObj['script_output'] += '\n' +  'running line shift calcs for line: ' + line['line_identifier']
	
	tagWriteError = False
	tagWriteAttemtped = False
	clearToWriteOeeValuesToTags = False
	
	# create a shift object, this will be returned to the calling process, and can be used in sub line level asset processing
	shift = {}
	result = False
	
	# create objects to store tag write paths and values for a single tag write at the end of the script execution
	tagWritePaths = []
	tagWriteValues = []
	



	# --------------------------- get the active shift context for this line ---------------------------------
	
	allShiftDataFound = False

	# get the active shift data for this line
	getActiveShiftResult, activeShift = shared.ent.shifts.db_sql.active_shift_data.getActiveShiftDataByLine(line['line_id'], '1')

	# get the idle periods for this line
	getIdlePeriodsResult, idlePeriods = shared.ent.shifts.db_sql.shift_idle_periods.getActiveShiftIdlePeriodsByLine(line['line_id'], '1')

	if getActiveShiftResult:

		if getIdlePeriodsResult:
				
			# ------------------------ first calculate some base date/time elements --------------------------
		
			# establish times of interest for updating the tags
			currentDate = system.date.now()
			currentHour = system.date.getHour24(currentDate)

			startOfCurrentHour = system.date.setTime(currentDate, currentHour, 0, 0)
			endOfCurrentHour = system.date.addHours(startOfCurrentHour, 1)

			startOfPreviousHour = system.date.addHours(startOfCurrentHour, -1)
			endOfPreviousHour = startOfCurrentHour
			
			newEventObj['script_output'] += '\n' +  'current date: ' + str(currentDate)

			newEventObj['script_output'] += '\n' +  'start of current hour: ' + str(startOfCurrentHour)
			newEventObj['script_output'] += '\n' +  'end of current hour: ' + str(endOfCurrentHour)

			newEventObj['script_output'] += '\n' +  'start of previous hour: ' + str(startOfPreviousHour)
			newEventObj['script_output'] += '\n' +  'end of previous hour: ' + str(endOfPreviousHour)
			
			
		
			# ------------------------ second collect the required line's active shift info --------------------------
			
			# shift date
			shift['shiftDate'] = activeShift['shift_date'] if activeShift['shift_date'] != 'None' else ''
			
			# shift day of week
			shift['dayOfWeek'] = activeShift['day_of_week'] if activeShift['day_of_week'] != 'None' else ''
			
			# shift code
			shift['shiftCode'] = activeShift['shift_code'] if activeShift['shift_code'] != 'None' else ''
			
			# shift overtime start
			shift['overtimeStartDate'] = activeShift['overtime_start_date'] if activeShift['overtime_start_date'] != 'None' else ''
			
			# shift start
			shift['startDate'] = activeShift['start_date'] if activeShift['start_date'] != 'None' else ''
			
			# shift end date
			shift['endDate'] = activeShift['end_date'] if activeShift['end_date'] != 'None' else ''
			
			# shift overtime end
			shift['overtimeEndDate'] = activeShift['overtime_end_date'] if activeShift['overtime_end_date'] != 'None' else ''
			
			# a shift is currently active
			if activeShift['shift_code'] == 'None':
			
				shift['inActiveShiftTime'] = False
			
			else:
			
				shift['inActiveShiftTime'] = True
			
			# a shift idle period is not active (in productive time)
			if activeShift['active_idle_period_id'] == 'None':
			
				shift['inPlannedProdTime'] = True
			
			else:
			
				shift['inPlannedProdTime'] = False
						
			# a time stamp for last time the shift data was updated
			shift['lastUpdated'] = system.date.now()
			
			
			
			# only calculate elapsed time if the line is in an active shift
			if shift['inActiveShiftTime'] == True:

				# ------------------------ second calculate the accumulated times for the lines active shift --------------------------
													
				# calculate the elapsed shift times for the current shift
											
				shift['shiftTime_currentShift']= 0
				shift['shiftTime_currentHour'] = 0
				shift['shiftTime_previousHour'] = 0

				shiftStartDate = system.date.parse(activeShift['start_date'], 'yyyy-MM-dd HH:mm:ss')
				shiftEndDate = system.date.parse(activeShift['end_date'], 'yyyy-MM-dd HH:mm:ss')

				newEventObj['script_output'] += '\n' + 'shift start date: ' + str(shiftStartDate)
				newEventObj['script_output'] += '\n' + 'shift end date: ' +  str(shiftEndDate)
					
				shift['shiftTime_currentShift']= shared.ent.assets.services.asset_tag_updating.lines.calcElapsedTimeWithinWindow(shiftStartDate, shiftEndDate, shiftStartDate, shiftEndDate)
				shift['shiftTime_currentHour'] += shared.ent.assets.services.asset_tag_updating.lines.calcElapsedTimeWithinWindow(startOfCurrentHour, endOfCurrentHour, shiftStartDate, shiftEndDate)
				shift['shiftTime_previousHour'] += shared.ent.assets.services.asset_tag_updating.lines.calcElapsedTimeWithinWindow(startOfPreviousHour, endOfPreviousHour, shiftStartDate, shiftEndDate)

				newEventObj['script_output'] += '\n' +  'elapsed shift time current shift: ' + str(shift['shiftTime_currentShift'])
				newEventObj['script_output'] += '\n' +  'elapsed shift time current hour: ' + str(shift['shiftTime_currentHour'])
				newEventObj['script_output'] += '\n' +  'elapsed shift time previous hour: ' + str(shift['shiftTime_previousHour'])



				# calculate the elapsed idle times for the current shift
											
				shift['idleTime_currentShift'] = 0
				shift['idleTime_currentHour'] = 0
				shift['idleTime_previousHour'] = 0

				for period in idlePeriods:
				
					newEventObj['script_output'] += '\n' + 'next idle period'

					periodStartDate = system.date.parse(period['start_date'], 'yyyy-MM-dd HH:mm:ss')
					periodEndDate = system.date.parse(period['end_date'], 'yyyy-MM-dd HH:mm:ss')
					
					newEventObj['script_output'] += '\n' + 'idle period start date: ' + str(periodStartDate)
					newEventObj['script_output'] += '\n' + 'idle period end date: ' + str(periodEndDate)
							
					shift['idleTime_currentShift'] += shared.ent.assets.services.asset_tag_updating.lines.calcElapsedTimeWithinWindow(shiftStartDate, shiftEndDate, periodStartDate, periodEndDate)
					shift['idleTime_currentHour'] += shared.ent.assets.services.asset_tag_updating.lines.calcElapsedTimeWithinWindow(startOfCurrentHour, endOfCurrentHour, periodStartDate, periodEndDate)
					shift['idleTime_previousHour'] += shared.ent.assets.services.asset_tag_updating.lines.calcElapsedTimeWithinWindow(startOfPreviousHour, endOfPreviousHour, periodStartDate, periodEndDate)


				newEventObj['script_output'] += '\n' +  'elapsed idle time current shift: ' + str(shift['idleTime_currentShift'])
				newEventObj['script_output'] += '\n' +  'elapsed idle time current hour: ' + str(shift['idleTime_currentHour'])
				newEventObj['script_output'] += '\n' +  'elapsed idle time previous hour: ' + str(shift['idleTime_previousHour'])
				

				
				# add the calculated times to the shift conetxt for use by calling function
				
				shift['startOfCurrentHour'] = startOfCurrentHour
				shift['endOfCurrentHour'] = endOfCurrentHour

				shift['startOfPreviousHour'] = startOfPreviousHour
				shift['endOfPreviousHour'] = endOfPreviousHour
				
				shift['currentDate'] = currentDate
				
				

				
				# --------------------------- all necessary OEE values were successfuly acquired or calculated ---------------------------------

				# if the script has executed this far with no errors, move forward with the tag write for this cell
				clearToWriteOeeValuesToTags = True



		else:

			newEventObj['script_output'] += '\n' +  'failed to retrieve idle period records for the line id: ' + str(line['line_id'])
			
			shift['inActiveShiftTime'] = False
		

	else:
	
		newEventObj['script_output'] += '\n' +  'failed to retrieve an active shift record for the line id: ' + str(line['line_id'])
		
		shift['inActiveShiftTime'] = False



		
	# --------------------------- write all values to all cell tags ---------------------------------
	
	# if all values were successfully calculated and cell is in an active shift, write the caluclated values for this cell
	if clearToWriteOeeValuesToTags:

		# shift date
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/shiftDate')
		tagWriteValues.append(shift['shiftDate'])

		# shift day of week
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/dayOfWeek')
		tagWriteValues.append(shift['dayOfWeek'])

		# shift code
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/shiftCode')
		tagWriteValues.append(shift['shiftCode'])

		# shift overtime start
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/overtimeStartDate')
		tagWriteValues.append(shift['overtimeStartDate'])

		# shift start
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/startDate')
		tagWriteValues.append(shift['startDate'])

		# shift end date
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/endDate')
		tagWriteValues.append(shift['endDate'])

		# shift overtime end
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/overtimeEndDate')
		tagWriteValues.append(shift['overtimeEndDate'])

		# a shift is currently active
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/inActiveShiftTime')
		tagWriteValues.append(shift['inActiveShiftTime'])

		# a shift idle period is not active (in productive time)
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/inPlannedProdTime')
		tagWriteValues.append(shift['inPlannedProdTime'])
			

		# a time stamp for last time the shift data was updated
		tagWritePaths.append(lineTagPath + '/Line/Shift/lastUpdated')
		tagWriteValues.append(system.date.now())


		# shift['shiftTime_currentShift']									
		tagWritePaths.append(lineTagPath + '/Line/Shift/Accum/ShiftTime/currentShift')
		tagWriteValues.append(shift['shiftTime_currentShift'])

		# shift['shiftTime_currentHour']					
		tagWritePaths.append(lineTagPath + '/Line/Shift/Accum/ShiftTime/currentHour')
		tagWriteValues.append(shift['shiftTime_currentHour'])

		# shift['shiftTime_previousHour']	
		tagWritePaths.append(lineTagPath + '/Line/Shift/Accum/ShiftTime/previousHour')
		tagWriteValues.append(shift['shiftTime_previousHour'])


		# shift['idleTime_currentShift']								
		tagWritePaths.append(lineTagPath + '/Line/Shift/Accum/IdleTime/currentShift')
		tagWriteValues.append(shift['idleTime_currentShift'])

		# shift['idleTime_currentHour']							
		tagWritePaths.append(lineTagPath + '/Line/Shift/Accum/IdleTime/currentHour')
		tagWriteValues.append(shift['idleTime_currentHour'])

		# shift['idleTime_previousHour']				
		tagWritePaths.append(lineTagPath + '/Line/Shift/Accum/IdleTime/previousHour')
		tagWriteValues.append(shift['idleTime_previousHour'])


	# clear this line's shift tags if there was an issue with calcs, or the line is not in an active shift
	else:

		# shift date
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/shiftDate')
		tagWriteValues.append('')

		# shift day of week
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/dayOfWeek')
		tagWriteValues.append('')

		# shift code
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/shiftCode')
		tagWriteValues.append(0)

		# shift overtime start
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/overtimeStartDate')
		tagWriteValues.append('')

		# shift start
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/startDate')
		tagWriteValues.append('')

		# shift end date
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/endDate')
		tagWriteValues.append('')

		# shift overtime end
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/overtimeEndDate')
		tagWriteValues.append('')

		# a shift is currently active
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/inActiveShiftTime')
		tagWriteValues.append(0)

		# a shift idle period is not active (in productive time)
		tagWritePaths.append(lineTagPath + '/Line/Shift/Active/inPlannedProdTime')
		tagWriteValues.append(0)
			
		# a time stamp for last time the shift data was updated
		tagWritePaths.append(lineTagPath + '/Line/Shift/lastUpdated')
		tagWriteValues.append(system.date.now())

		# shift['shiftTime_currentShift']									
		tagWritePaths.append(lineTagPath + '/Line/Shift/Accum/ShiftTime/currentShift')
		tagWriteValues.append(0)

		# shift['shiftTime_currentHour']					
		tagWritePaths.append(lineTagPath + '/Line/Shift/Accum/ShiftTime/currentHour')
		tagWriteValues.append(0)

		# shift['shiftTime_previousHour']	
		tagWritePaths.append(lineTagPath + '/Line/Shift/Accum/ShiftTime/previousHour')
		tagWriteValues.append(0)

		# shift['idleTime_currentShift']								
		tagWritePaths.append(lineTagPath + '/Line/Shift/Accum/IdleTime/currentShift')
		tagWriteValues.append(0)

		# shift['idleTime_currentHour']							
		tagWritePaths.append(lineTagPath + '/Line/Shift/Accum/IdleTime/currentHour')
		tagWriteValues.append(0)

		# shift['idleTime_previousHour']				
		tagWritePaths.append(lineTagPath + '/Line/Shift/Accum/IdleTime/previousHour')
		tagWriteValues.append(0)
	
		

	# execute the write to all tags
	tagWriteResults = system.tag.writeAll(tagWritePaths, tagWriteValues)

	newEventObj['script_output'] += '\n' +  'all tags write for cell executed'
	
	tagWriteAttemtped = True
	
	
	# diagnostics for tag writes
	for i in range(0, len(tagWriteResults)):
	
		if tagWriteResults[i] != 2:
		
			newEventObj['script_output'] += '\n' +  'there was a problem writing the value of: ' + str(tagWriteValues[i]) + ' to the tag: ' + str(tagWritePaths[i])
			
			tagWriteError = True

		
		
		
	# --------------------------- evaluate the overall script result ---------------------------------	
	
	if tagWriteAttemtped == True and tagWriteError == False:
	
		newEventObj['event_result'] = 'SUCCESS'
		
		if str(logAllExecutions) == 'True':
		
			# create a new event log event record
			createEventLogResult = shared.ent.event_logging.db_sql.events.insertNewEvent(newEventObj)
		
		result = True
		
	else:
	
		newEventObj['event_result'] = 'ERROR'
		
		# create a new event log event record
		createEventLogResult = shared.ent.event_logging.db_sql.events.insertNewEvent(newEventObj)
		
		result = False
	

	# # diagnostic printing of event object
	# for prop in newEventObj:
	
		# print str(prop) + ' value: ' + str(newEventObj[prop])
		
		
	return result, shift
	
	
	
	
	
	

def calcElapsedTimeWithinWindow(timeWindowStart, timeWindowEnd, timePeriodStart, timePeriodEnd):

	currentDate = system.date.now()
	
	start = currentDate
	end = currentDate
	result = -1
	
	
	# determine overlap range start time
	if timeWindowStart > timePeriodStart:
		
		start = timeWindowStart
	
	else:
	
		start = timePeriodStart
		
	# determine overlap range end time
	if currentDate <= timePeriodEnd and currentDate <= timeWindowEnd:
	
		end = currentDate
		
	elif timePeriodEnd <= currentDate and timePeriodEnd <= timeWindowEnd:
	
		end = timePeriodEnd
		
	else:
	
		end = timeWindowEnd
		
	# print 'start: ' + str(start)
	# print 'send: ' + str(end)
		
	# calculate the overlapped time
	elapsedMinutes = system.date.secondsBetween(start, end)
	
	# a negative time elpased result indicates a past or future time period, return a zero
	if elapsedMinutes < 0:
		
		result = 0
		
	else:
	
		result = elapsedMinutes
		
	return result
		
		
