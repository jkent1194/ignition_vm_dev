# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 02/01/2020
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	


# temp location for asset model tags
baseTagPath = 'Enterprise'


# -----------------------------------------------------------------------------------------------------------------------
#
# Checks for, and creates a tag path directory
#
# <Arguments>	- path (string) - the desired location of the directory
#
# <Returns>		- result (bool) - Indicates the successful execution of the function, and that the directory exists
#
# -----------------------------------------------------------------------------------------------------------------------
def addTagDirectory(path, newEventObj):

	# # replace spaces in the path with underscores
	# path = path.replace(' ', '_')
	
	
	# check to see if the directory already exists
	if system.tag.exists(path):

		newEventObj['script_output'] += '\n' + 'path: ' + path + ' already exists'
		
		return True, newEventObj
	
	else:
	
		# the directory does not already exist, create it
		
		# build a parent level directory path, and a final directory name to create, from the passed in complete tag path
		pathDirectoryNames = path.split('/')
		
		newEventObj['script_output'] += '\n' + str(pathDirectoryNames)
		newEventObj['script_output'] += '\n' + str(pathDirectoryNames[-1])
	
		# build out the parent path
		parentPath = ''
		
		for i in range(0, len(pathDirectoryNames) - 1):
		
			if i == 0:
				
				parentPath += pathDirectoryNames[i]
				
			else:
			
				parentPath += '/' + pathDirectoryNames[i]

		# build the folder name to create at the lowest level of the path
		finalDirectoryName = pathDirectoryNames[len(pathDirectoryNames) - 1]
		
		newEventObj['script_output'] += '\n' + parentPath
		newEventObj['script_output'] += '\n' + finalDirectoryName
	
		# create the folder at the parent path
		result = system.tag.addTag(parentPath = parentPath, name = finalDirectoryName, tagType = "Folder")
		
		if result == False:

			newEventObj['script_output'] += '\n' + 'failed to create the folder path: ' + path
			
			return False, newEventObj



# -----------------------------------------------------------------------------------------------------------------------
#
# Checks for, and creates the asset model "cell" tag in the parent path directory
#
# <Arguments>	- path (string) - the desired path of the tag
#
# <Returns>		- result (bool) - Indicates the successful execution of the function, and that the directory exists
#
# -----------------------------------------------------------------------------------------------------------------------
def addCellBaseTag(parentPath, newEventObj):

	# # replace spaces in the path with underscores
	# parentPath = parentPath.replace(' ', '_')
	
	completePath = parentPath + '/' + 'Cell'
	
	# check to see if the directory already exists
	if system.tag.exists(completePath):

		newEventObj['script_output'] += '\n' + 'path: ' + completePath + ' already exists'
		
		return True, newEventObj
	
	else:
	
		# create the tag in the parent path
				
		result = system.tag.addTag(parentPath=parentPath, name="Cell", tagType="UDT_INST", attributes={"UDTParentType":"Enterprise/assets/cell/cell"})
				
		
		if result == False:

			newEventObj['script_output'] += '\n' + 'failed to create the tag in path: ' + path
			
			return False, newEventObj
			
			

# -----------------------------------------------------------------------------------------------------------------------
#
# Checks for, and creates the asset model "line" tag in the parent path directory
#
# <Arguments>	- path (string) - the desired path of the tag
#
# <Returns>		- result (bool) - Indicates the successful execution of the function, and that the directory exists
#
# -----------------------------------------------------------------------------------------------------------------------
def addLineBaseTag(parentPath, newEventObj):

	# # replace spaces in the path with underscores
	# parentPath = parentPath.replace(' ', '_')
	
	completePath = parentPath + '/' + 'Line'
	
	# check to see if the directory already exists
	if system.tag.exists(completePath):

		newEventObj['script_output'] += '\n' + 'path: ' + completePath + ' already exists'
		
		return True, newEventObj
	
	else:
	
		# create the tag in the parent path
				
		result = system.tag.addTag(parentPath=parentPath, name="Line", tagType="UDT_INST", attributes={"UDTParentType":"Enterprise/assets/line/line"})
				
		
		if result == False:

			newEventObj['script_output'] += '\n' + 'failed to create the tag in path: ' + path
						
			return False, newEventObj



# -----------------------------------------------------------------------------------------------------------------------
#
# Checks for, and creates the asset model "area" tag in the parent path directory
#
# <Arguments>	- path (string) - the desired path of the tag
#
# <Returns>		- result (bool) - Indicates the successful execution of the function, and that the directory exists
#
# -----------------------------------------------------------------------------------------------------------------------
def addAreaBaseTag(parentPath, newEventObj):

	# # replace spaces in the path with underscores
	# parentPath = parentPath.replace(' ', '_')
	
	completePath = parentPath + '/' + 'Area'
	
	# check to see if the directory already exists
	if system.tag.exists(completePath):

		newEventObj['script_output'] += '\n' + 'path: ' + completePath + ' already exists'
		
		return True, newEventObj
	
	else:
	
		# create the tag in the parent path
				
		result = system.tag.addTag(parentPath=parentPath, name="Area", tagType="UDT_INST", attributes={"UDTParentType":"Enterprise/assets/area/area"})
				
		
		if result == False:

			newEventObj['script_output'] += '\n' + 'failed to create the tag in path: ' + path
			
			return False, newEventObj



# -----------------------------------------------------------------------------------------------------------------------
#
# Checks for, and creates the asset model "site" tag in the parent path directory
#
# <Arguments>	- path (string) - the desired path of the tag
#
# <Returns>		- result (bool) - Indicates the successful execution of the function, and that the directory exists
#
# -----------------------------------------------------------------------------------------------------------------------
def addSiteBaseTag(parentPath, newEventObj):

	# # replace spaces in the path with underscores
	# parentPath = parentPath.replace(' ', '_')
	
	completePath = parentPath + '/' + 'Site'
	
	# check to see if the directory already exists
	if system.tag.exists(completePath):

		newEventObj['script_output'] += '\n' + 'path: ' + completePath + ' already exists'
		
		return True, newEventObj
	
	else:
	
		# create the tag in the parent path
				
		result = system.tag.addTag(parentPath=parentPath, name="Site", tagType="UDT_INST", attributes={"UDTParentType":"Enterprise/assets/site/site"})
				
		
		if result == False:

			newEventObj['script_output'] += '\n' + 'failed to create the tag in path: ' + path
			
			return False, newEventObj



# -----------------------------------------------------------------------------------------------------------------------
#
# Checks for, and creates the asset id tag in the parent path directory, with the value of the assetId that is passed into this function
#
# <Arguments>	- path (string) - the desired path of the tag
#
# <Returns>		- result (bool) - Indicates the successful execution of the function, and that the directory exists
#
# -----------------------------------------------------------------------------------------------------------------------
def addAssetIdTag(parentPath, assetId, newEventObj):
	
	completePath = parentPath
	
	# check to see if the directory already exists
	if system.tag.exists(completePath + '/Asset_ID'):

		newEventObj['script_output'] += '\n' + 'path: ' + completePath + ' already exists'
		
		return True, newEventObj
	
	else:
	
		# create the tag in the parent path
		result = system.tag.addTag(parentPath=completePath, name="Asset_ID", tagType="EXPRESSION", dataType="String", attributes={"Expression":assetId})	
		
		if result == False:

			newEventObj['script_output'] += '\n' + 'failed to create the tag in path: ' + path
			
			print 'failed to create the tag in path: ' + path

			return False, newEventObj
		
		

# -----------------------------------------------------------------------------------------------------------------------
#
# Checks for, and creates the asset description tag in the parent path directory, with the value of the assetDescription 
# that is passed into this function
#
# <Arguments>	- path (string) - the desired path of the tag
#
# <Returns>		- result (bool) - Indicates the successful execution of the function, and that the directory exists
#
# -----------------------------------------------------------------------------------------------------------------------
def addAssetDescriptionTag(parentPath, assetDescription, newEventObj):
	
	completePath = parentPath
	
	# check to see if the directory already exists
	if system.tag.exists(completePath + '/description'):

		newEventObj['script_output'] += '\n' + 'path: ' + completePath + ' already exists'
		
		return True, newEventObj
	
	else:
	
		# create the tag in the parent path
		result = system.tag.addTag(parentPath=completePath, name="description", tagType="Memory", dataType="String", value=assetDescription)
		
		if result == False:

			newEventObj['script_output'] += '\n' + 'failed to create the tag in path: ' + path
			
			print 'failed to create the tag in path: ' + path

			return False, newEventObj
					


# -----------------------------------------------------------------------------------------------------------------------
#
# Creates the asset tag model in the Ignition tag provider
#
# <Arguments>	- logAllExecutions (bool) - set to True to log an event for each run, or false for errored executions only
#
# <Returns>		- none
#
# -----------------------------------------------------------------------------------------------------------------------
def runTagGeneration(logAllExecutions):

	# start a script execution timer
	startTime = system.date.now() 


	# create an event log object
	newEventObj = shared.ent.event_logging.db_sql.misc.createEventLogObject('Asset Tag Builder')

	newEventObj['script_output'] += '\n' + 'running the asset tag builder'
	

	# get all of the enterprise asset sites
	getSitesResult, sites = shared.ent.assets.db_sql.sites.getAllSites('1')

	if getSitesResult:

		for site in sites:
		
			newEventObj['script_output'] += '\n' + 'site: ' + str(site['site_identifier'])
			
			# establish the tag directory path for this site
			tagPath = baseTagPath + '/' + str(site['site_identifier'])
			
			newEventObj['script_output'] += '\n' + 'tag path: ' + tagPath
			
			# ensure the tag path exists
			addTagDirectory(tagPath, newEventObj)
			
			# ensure the "Site" model tag exists in the directory
			addSiteBaseTag(tagPath, newEventObj)
			
			# add the asset id tag in the asset directory
			addAssetIdTag(tagPath, site['site_id'], newEventObj)
			
			# add the asset description tag in the asset directory
			addAssetDescriptionTag(tagPath, site['site_description'], newEventObj)
		
			# get the areas within each site
			getAreasResult, areas = shared.ent.assets.db_sql.areas.getAllAreasBySite(site['site_id'], '1')
			
			if getAreasResult:
			
				for area in areas:
			
					newEventObj['script_output'] += '\n' + 'area: ' + str(area['area_identifier'])
					
					# establish the tag directory path for this area
					tagPath = baseTagPath + '/' + str(site['site_identifier']) + '/' + str(area['area_identifier'])
					
					newEventObj['script_output'] += '\n' + 'tag path: ' + tagPath
					
					# ensure the tag path exists
					addTagDirectory(tagPath, newEventObj)
					
					# ensure the "Area" model tag exists in the directory
					addAreaBaseTag(tagPath, newEventObj)
					
					# add the asset id tag in the asset directory
					addAssetIdTag(tagPath, area['area_id'], newEventObj)
					
					# add the asset description tag in the asset directory
					addAssetDescriptionTag(tagPath, area['area_description'], newEventObj)
		
					# get the lines within each area
					getLinesResult, lines = shared.ent.assets.db_sql.lines.getAllLinesByArea(area['area_id'], '1')
			
					if getLinesResult:
					
						for line in lines:
				
							newEventObj['script_output'] += '\n' + 'line: ' + str(line['line_identifier'])
							
							# establish the tag directory path for this line
							tagPath = baseTagPath + '/' + str(site['site_identifier']) + '/' + str(area['area_identifier']) + '/' + str(line['line_identifier'])
							
							newEventObj['script_output'] += '\n' + 'tag path: ' + tagPath
							
							# ensure the tag directory exists
							addTagDirectory(tagPath, newEventObj)

							# ensure the "Line" model tag exists in the directory
							addLineBaseTag(tagPath, newEventObj)
							
							# add the asset id tag in the asset directory
							addAssetIdTag(tagPath, line['line_id'], newEventObj)
							
							# add the asset description tag in the asset directory
							addAssetDescriptionTag(tagPath, line['line_description'], newEventObj)							

							# get the cells within each line
							getCellsResult, cells = shared.ent.assets.db_sql.cells.getAllCellsByLine(line['line_id'], '1')
						
							if getCellsResult:
							
								for cell in cells:
						
									newEventObj['script_output'] += '\n' + 'cell: ' + str(cell['cell_identifier'])
									
									# establish the tag directory path for this cell
									tagPath = baseTagPath + '/' + str(site['site_identifier']) + '/' + str(area['area_identifier']) + '/' + str(line['line_identifier']) + '/' + str(cell['cell_identifier'])
									
									newEventObj['script_output'] += '\n' + 'tag path: ' + tagPath
									
									# ensure the tag path exists
									addTagDirectory(tagPath, newEventObj)
									
									# ensure the "Cell" model tag exists in the directory
									addCellBaseTag(tagPath, newEventObj)
									
									# add the asset id tag in the asset directory
									addAssetIdTag(tagPath, cell['cell_id'], newEventObj)
									
									# add the asset description tag in the asset directory
									addAssetDescriptionTag(tagPath, cell['cell_description'], newEventObj)										
									
									# ensure that the "andons" folder exists within the cell directory
									addTagDirectory(tagPath + '/andOns', newEventObj)
												
									# ensure that the "custom" folder exists within the cell directory
									addTagDirectory(tagPath + '/custom', newEventObj)
									
					
							else:
							
								newEventObj['script_output'] += '\n' + 'no cells were found for line: ' + str(line['line_identifier'])
										
					
					else:
					
						newEventObj['script_output'] += '\n' + 'no lines were found for area: ' + str(area['area_identifier'])
						
			else:
			
				newEventObj['script_output'] += '\n' + 'no areas were found for site: ' + str(site['site_identifier'])
			
	else:

		newEventObj['script_output'] += '\n' + 'no sites were found'
		
		
		
		
	# --------------------------- calculate the script execution time ---------------------------------		
	
	# calculate the elapsed processing time of the script	
	endTime = system.date.now()
	timeElapsed = system.date.millisBetween(startTime, endTime)


	# execution time for diagnostics
	# newEventObj['script_output'] += '\n' +  '\n'
	newEventObj['script_output'] += '\n' +  'Total execution time: ' + str(timeElapsed)
	
	newEventObj['event_duration'] = str(timeElapsed)
		
		
		
	# --------------------------- evaluate the overall script result ---------------------------------	

	newEventObj['event_result'] = 'SUCCESS'
	
	# debug printing of event object	
	for prop in newEventObj:
		print prop
		print newEventObj[prop]

		
	if str(logAllExecutions) == 'True':
	
		# create a new event log event record
		createEventLogResult = shared.ent.event_logging.db_sql.events.insertNewEvent(newEventObj)
