













def calcCellOEE(cell, cellTagPath, shift, logAllExecutions):

	# create an event log object
	newEventObj = shared.ent.event_logging.db_sql.misc.createEventLogObject('Calc Cell OEE Service')

	newEventObj['script_output'] += '\n' +  'running cell OEE calcs for cell: ' + cell['cell_identifier']
	
	tagWriteError = False
	tagWriteAttemtped = False
	clearToWriteOeeValuesToTags = False
	result = False
	
	# create objects to store tag write paths and values for a single tag write at the end of the script execution
	cellTagWritePaths = []
	cellTagWriteValues = []

	



	# --------------------------- validate this cell is actively in a shift ---------------------------------

	# make sure the line has an active shift before digging into the individual cells OEE calcs
	if str(shift['inActiveShiftTime']) == 'True':

		newEventObj['script_output'] += '\n' +  'the line this cell belongs to is in an active shift.'
				

		
		
		# --------------------------- get this cell's takt time setpoint ---------------------------------

		# get the cells takt time setpoint for the active shift
		getTaktTimeResult = False
		
		if str(shift['shiftCode']) == '3':
		
			getTaktTimeResult, taktTimeSetpoint = shared.ent.assets.interface.asset_properties.getAssetPropByAssetId(cell['cell_id'], 'Cell', 'oee/takt_time/setpoint_3rd_shift')

		if str(shift['shiftCode']) == '1':
		
			getTaktTimeResult, taktTimeSetpoint = shared.ent.assets.interface.asset_properties.getAssetPropByAssetId(cell['cell_id'], 'Cell', 'oee/takt_time/setpoint_1st_shift')
		
		if str(shift['shiftCode']) == '2':
		
			getTaktTimeResult, taktTimeSetpoint = shared.ent.assets.interface.asset_properties.getAssetPropByAssetId(cell['cell_id'], 'Cell', 'oee/takt_time/setpoint_2nd_shift')
		
		# make sure a takt time was acquired
		if getTaktTimeResult:
		
			# debug print
			newEventObj['script_output'] += '\n' +  'cells active takt time: ' + str(taktTimeSetpoint)



			# --------------------------- baan users ---------------------------------

			# get the baan users assigned to this cell
			getBaanUsersResult, baanUsers = shared.ent.assets.interface.baan_users.getAllBaanUsersByCell(cell['cell_id'])
						
			if getBaanUsersResult:
			
				baanUsers_nameOnly = []

				newEventObj['script_output'] += '\n' +  'baan users found associated with this cell:'
				
				for baanUser in baanUsers:
				
					newEventObj['script_output'] += '\n' +  baanUser['baan_user_name']
					baanUsers_nameOnly.append(baanUser['baan_user_name'])
					



				# --------------------------- actual completions ---------------------------------
				
				# get actual completions for the shift
				getCompletionsResult, actualCompletions_currentShift = calcActualCompletions(baanUsers_nameOnly, shift['overtimeStartDate'], shift['overtimeEndDate'])

				# get actual completions for the current hour
				getCompletionsResult, actualCompletions_currentHour = calcActualCompletions(baanUsers_nameOnly, shift['startOfCurrentHour'], shift['currentDate'])

				# get actual completions for the previous hour
				getCompletionsResult, actualCompletions_previousHour = calcActualCompletions(baanUsers_nameOnly, shift['startOfPreviousHour'], shift['startOfCurrentHour'])

				

				
				# --------------------------- expected completions ---------------------------------
				
				# get expected completions for the shift
				expectedCompletions_currentShift = calcExpectedCompletions(taktTimeSetpoint, shift['shiftTime_currentShift'] - shift['idleTime_currentShift'])

				# get expected completions for the current hour
				expectedCompletions_currentHour = calcExpectedCompletions(taktTimeSetpoint, shift['shiftTime_currentHour'] - shift['idleTime_currentHour'])

				# get expected completions for the previous hour
				expectedCompletions_previousHour = calcExpectedCompletions(taktTimeSetpoint, shift['shiftTime_previousHour'] - shift['idleTime_previousHour'])
		



				# --------------------------- actual OR value calcuations ---------------------------------
				
				# calc the OR for the shift
				ORValue_currentShift = calcOR(actualCompletions_currentShift, expectedCompletions_currentShift)

				# calc the OR for the current hour
				ORValue_currentHour = calcOR(actualCompletions_currentHour, expectedCompletions_currentHour)

				# calc the OR for the previous hour
				ORValue_previousHour = calcOR(actualCompletions_previousHour, expectedCompletions_previousHour)
				


				
				# --------------------------- all necessary OEE values were successfuly acquired or calculated ---------------------------------

				# if the script has executed this far with no errors, move forward with the tag write for this cell
				clearToWriteOeeValuesToTags = True



		
			else:
		
				newEventObj['script_output'] += '\n' +  'failed to find any baan users associated with this cell'
				
		
		else:
		
			newEventObj['script_output'] += '\n' +  'failed to get the cells takt time setpoint'


	else:

		newEventObj['script_output'] += '\n' +  'the line this cell belongs to is not currently in an active shift'		
		
		
		
		
	# --------------------------- write all values to all cell tags ---------------------------------
	
	# if all values were successfully calculated and cell is in an active shift, write the caluclated values for this cell
	if clearToWriteOeeValuesToTags:
	
		# write the active takt time setpoint to the OEE tags
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/TAKT/takt_timeSetpoint')
		cellTagWriteValues.append(taktTimeSetpoint)
	
		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Actual/currentShift')
		cellTagWriteValues.append(actualCompletions_currentShift)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Actual/currentHour')
		cellTagWriteValues.append(actualCompletions_currentHour)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Actual/previousHour')
		cellTagWriteValues.append(actualCompletions_previousHour)
	
		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Expected/currentShift')
		cellTagWriteValues.append(expectedCompletions_currentShift)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Expected/currentHour')
		cellTagWriteValues.append(expectedCompletions_currentHour)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Expected/previousHour')
		cellTagWriteValues.append(expectedCompletions_previousHour)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/OR/currentShift')
		cellTagWriteValues.append(ORValue_currentShift)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/OR/currentHour')
		cellTagWriteValues.append(ORValue_currentHour)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/OR/previousHour')
		cellTagWriteValues.append(ORValue_previousHour)
		
		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/lastUpdated')
		cellTagWriteValues.append(system.date.now())

		
	
	# clear this cell's OEE tags if there was an issue with calcs, or the cell is not in an active shift
	else:
	
		# write the active takt time setpoint to the OEE tags
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/TAKT/takt_timeSetpoint')
		cellTagWriteValues.append(0)
	
		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Actual/currentShift')
		cellTagWriteValues.append(0)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Actual/currentHour')
		cellTagWriteValues.append(0)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Actual/previousHour')
		cellTagWriteValues.append(0)
	
		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Expected/currentShift')
		cellTagWriteValues.append(0)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Expected/currentHour')
		cellTagWriteValues.append(0)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Expected/previousHour')
		cellTagWriteValues.append(0)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/OR/currentShift')
		cellTagWriteValues.append(0)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/OR/currentHour')
		cellTagWriteValues.append(0)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/OR/previousHour')
		cellTagWriteValues.append(0)

		# append the value to write to the list of tags to write
		cellTagWritePaths.append(cellTagPath + '/Cell/OEE/lastUpdated')
		cellTagWriteValues.append(system.date.now())


	# execute the write to all tags
	tagWriteResults = system.tag.writeAll(cellTagWritePaths, cellTagWriteValues)

	newEventObj['script_output'] += '\n' +  'all tags write for cell executed'
	
	tagWriteAttemtped = True
	
	
	# diagnostics for tag writes
	for i in range(0, len(tagWriteResults)):
	
		if tagWriteResults[i] != 2:
		
			newEventObj['script_output'] += '\n' +  'there was a problem writing the value of: ' + str(cellTagWriteValues[i]) + ' to the tag: ' + str(cellTagWritePaths[i])
			
			tagWriteError = True

		
		
		
	# --------------------------- evaluate the overall script result ---------------------------------	
	
	if tagWriteAttemtped == True and tagWriteError == False:
	
		newEventObj['event_result'] = 'SUCCESS'
		
		if str(logAllExecutions) == 'True':
		
			# create a new event log event record
			createEventLogResult = shared.ent.event_logging.db_sql.events.insertNewEvent(newEventObj)
		
		result = True
		
	else:
	
		newEventObj['event_result'] = 'ERROR'
		
		# create a new event log event record
		createEventLogResult = shared.ent.event_logging.db_sql.events.insertNewEvent(newEventObj)
		
		result = False
	
	
	# # diagnostic printing of event object
	# for prop in newEventObj:
	
		# print str(prop) + ' value: ' + str(newEventObj[prop])
		
		
	return result
	
	
	
def calcActualCompletions(baanUsers, startDate, endDate):
	
	result = True
	totalCompletions = 0

	for baanUser in baanUsers:
	
		# print baanUser
		# print startDate
		# print endDate

		getCompletionsResult, completions = shared.ent.common.interface.baan_completions.getNumberOfCompletions(baanUser, startDate, endDate)

		if getCompletionsResult:
			
			totalCompletions += completions
			
		else:
		
			print 'there was an issue retrieving completions for baan user: ' + baanUser + ' for the date range : ' + str(startDate) + ' to: ' + str(endDate)
		
			result = False
			completions = 0
			
			break

	return result, totalCompletions
	



def calcExpectedCompletions(taktTimeSetpoint, elapsedProdTime):

	expectedCompletions = 0

	if taktTimeSetpoint > 0:
		
		expectedCompletions = int(elapsedProdTime / float(taktTimeSetpoint))

	return expectedCompletions
	
	
	

def calcOR(expectedCompletions, actualCompletions):

	result = True
	ORValue = 0

	if actualCompletions > 0:
		
		ORValue = float(expectedCompletions) / float(actualCompletions) * 100

	return ORValue
			