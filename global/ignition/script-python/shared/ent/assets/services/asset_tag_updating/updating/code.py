# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 2/11/2020
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	


# temp location for asset model tags
baseTagPath = 'Enterprise'

	
		
		
		
		


# -----------------------------------------------------------------------------------------------------------------------
#
# Updates all asset tags in the Igntion asset tag model
#
# <Arguments>	- logAllExecutions (boolean) 	- if True, a event log will be created for successfull executions, when False 
#													only errors will be logged.
#
# <Returns>		- none
#
# -----------------------------------------------------------------------------------------------------------------------
def runTagUpdate(logAllExecutions):

	# print 'hello from cell OEE tag updater'

	# start a script execution timer
	startTime = system.date.now() 
	
	
	# create an event log object
	newEventObj = {}
	newEventObj['event_source'] = 'Asset Tag Values Updater'
	newEventObj['input_values'] = ''
	newEventObj['output_values'] = ''
	newEventObj['script_output'] = ''
	newEventObj['event_result'] = ''
	newEventObj['event_duration'] = ''

	overallScriptResult = True

	newEventObj['script_output'] += 'running asset tag data update'

	
	

	# get all of the enterprise asset sites
	getSitesResult, sites = shared.ent.assets.db_sql.sites.getAllSites('1')

	if getSitesResult:

		for site in sites:

			newEventObj['script_output'] += '\n' +  'workign on site: ' + str(site['site_identifier'])
			
			# get the areas within this site
			getAreasResult, areas = shared.ent.assets.db_sql.areas.getAllAreasBySite(site['site_id'], '1')
			
			if getAreasResult:
			
				for area in areas:

					newEventObj['script_output'] += '\n' +  'workign on area: ' + str(area['area_identifier'])

					# get the lines within this area
					getLinesResult, lines = shared.ent.assets.db_sql.lines.getAllLinesByArea(area['area_id'], '1')
			
					if getLinesResult:
					
						for line in lines:
						
							# print line['line_identifier']
							newEventObj['script_output'] += '\n' +  'working on line: ' + str(line['line_identifier'])
							
							# establish the tag directory path for this line
							getPathResult, lineTagPath = shared.ent.assets.interface.misc.getAssetTagPath(line['line_id'], 'Line')
							
							newEventObj['script_output'] += '\n' +  'derived line tag path: ' + lineTagPath

							
							# ------------------------------ line shift updating ---------------------------------------------------------------
							
							# update this line's active shift tags
							lineShiftCalcResult, shift = shared.ent.assets.services.asset_tag_updating.lines.calcLineShift(line, lineTagPath, logAllExecutions)
							
							# print 'line shift calc result: ' + str(lineShiftCalcResult)
							# print shift


							# get the cells within this line
							getCellsResult, cells = shared.ent.assets.db_sql.cells.getAllCellsByLine(line['line_id'], '1')
					
							if getCellsResult:
							
								for cell in cells:
								
									# print cell['cell_identifier']
									newEventObj['script_output'] += '\n' +  'working on cell: ' + str(cell['cell_identifier'])
							
									# establish the tag directory path for this cell
									getPathResult, cellTagPath = shared.ent.assets.interface.misc.getAssetTagPath(cell['cell_id'], 'Cell')

									newEventObj['script_output'] += '\n' +  'derived cell tag path: ' + cellTagPath
								
									
											
									# ------------------------------ cell oee tag updating ---------------------------------------------------------------
									
									# update this cell's oee tags
									cellOeeCalcResult = shared.ent.assets.services.asset_tag_updating.cells.calcCellOEE(cell, cellTagPath, shift, logAllExecutions)

									# print 'oee calc result: ' + str(cellOeeCalcResult)


							else:
							
								newEventObj['script_output'] += '\n' +  'no cells were found for line: ' + str(line['line_identifier'])


					else:
					
						newEventObj['script_output'] += '\n' +  'no lines were found for area: ' + str(area['area_identifier'])
						
						
			else:
			
				newEventObj['script_output'] += '\n' +  'no areas were found for site: ' + str(site['site_identifier'])
			
			
	else:

		newEventObj['script_output'] += '\n' +  'no sites were found'

		
	
	# calculate the elapsed processing time of the script	
	endTime = system.date.now()
	timeElapsed = system.date.millisBetween(startTime, endTime)
	
	# wrap up the event log record
	newEventObj['event_result'] = 'SUCCESS'
	newEventObj['event_duration'] = str(timeElapsed)
	
	# log the event
	if str(logAllExecutions) == 'True':
			
		# create a new event log event record
		createEventLogResult = shared.ent.event_logging.db_sql.events.insertNewEvent(newEventObj)
	




		











		