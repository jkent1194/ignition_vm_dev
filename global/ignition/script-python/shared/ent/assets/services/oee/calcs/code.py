# ----------------------------------------------------------------------------------------------	
#
# Author: Ken Burr
# Date: 2/11/2020
#
# File Description: 
#
# ----------------------------------------------------------------------------------------------	


# temp location for asset model tags
baseTagPath = '_dev_kab_HermanMiller'

	
		
		
		
		


# -----------------------------------------------------------------------------------------------------------------------
#
# Updates the OEE tags for each cell in the asset tag model
#
# <Arguments>	- none
#
# <Returns>		- none
#
# -----------------------------------------------------------------------------------------------------------------------
def runUpdateOeeTags():

	# print 'hello from cell OEE tag updater'

	# start a script execution timer
	startTime = system.date.now() 
	
	
	# create an event log object
	newEventObj = {}
	newEventObj['event_source'] = 'Asset Cell OEE Tag Update'
	newEventObj['input_values'] = ''
	newEventObj['output_values'] = ''
	newEventObj['script_output'] = ''
	newEventObj['event_result'] = ''
	newEventObj['event_duration'] = ''
	
	
	overallScriptResult = True
	
	

	newEventObj['script_output'] += 'running asset cell OEE tag data update'
	
	
	# establish times of interest for updating the tags
	currentDate = system.date.now()
	currentHour = system.date.getHour24(currentDate)

	startOfCurrentHour = system.date.setTime(currentDate, currentHour, 0, 0)
	endOfCurrentHour = system.date.addHours(startOfCurrentHour, 1)

	startOfPreviousHour = system.date.addHours(startOfCurrentHour, -1)
	endOfPreviousHour = startOfCurrentHour

	newEventObj['script_output'] += '\n' +  'start of current hour: ' + str(startOfCurrentHour)
	newEventObj['script_output'] += '\n' +  'end of current hour: ' + str(endOfCurrentHour)

	newEventObj['script_output'] += '\n' +  'start of previous hour: ' + str(startOfPreviousHour)
	newEventObj['script_output'] += '\n' +  'end of previous hour: ' + str(endOfPreviousHour)
	# newEventObj['script_output'] += '\n' +  '\n'
	

	# get all of the enterprise asset sites
	getSitesResult, sites = shared.ent.assets.db_sql.sites.getAllSites('1')

	if getSitesResult:

		for site in sites:

			newEventObj['script_output'] += '\n' +  'site: ' + str(site['site_identifier'])
			
			# get the areas within this site
			getAreasResult, areas = shared.ent.assets.db_sql.areas.getAllAreasBySite(site['site_id'], '1')
			
			if getAreasResult:
			
				for area in areas:

					newEventObj['script_output'] += '\n' +  'area: ' + str(area['area_identifier'])

					# get the lines within this area
					getLinesResult, lines = shared.ent.assets.db_sql.lines.getAllLinesByArea(area['area_id'], '1')
			
					if getLinesResult:
					
						for line in lines:
						
							print line['line_identifier']
						
							newEventObj['script_output'] += '\n' +  'line: ' + str(line['line_identifier'])
							
							
							
							# establish the tag directory path for this line
							lineTagPath = baseTagPath + '/' + str(site['site_identifier']) + '/' + str(area['area_identifier']) + '/' + str(line['line_identifier'])
							
							newEventObj['script_output'] += '\n' +  'line tag path: ' + lineTagPath
							
							# for the Ignition tag paths, swap the spaces out with underscores
							lineTagPath = lineTagPath.replace(' ', '_')
							
							newEventObj['script_output'] += '\n' +  'underscored line tag path: ' + lineTagPath
							
							newEventObj['script_output'] += '\n' +  'line id: ' + str(line['line_id'])
							
							
							
							
							# --------------------------------------------------------------------------------------
							# begin the process of calculating all related OEE attributes for this cell
							# --------------------------------------------------------------------------------------

							# get the related line/shift level properties that apply to this cell
							overtimeStartDate = system.tag.read(lineTagPath + '/Line/Shift/Active/overtimeStartDate').value
							overtimeEndDate = system.tag.read(lineTagPath + '/Line/Shift/Active/overtimeEndDate').value
							shiftCode = system.tag.read(lineTagPath + '/Line/Shift/Active/shiftCode').value
							shiftDate = system.tag.read(lineTagPath + '/Line/Shift/Active/shiftDate').value
							inPlannedProdTime = system.tag.read(lineTagPath + '/Line/Shift/Active/inPlannedProdTime').value 
							inActiveShiftTime =  system.tag.read(lineTagPath + '/Line/Shift/Active/inActiveShiftTime').value
							
							idleTime_currentShift = system.tag.read(lineTagPath + '/Line/Shift/Accum/IdleTime/currentShift').value
							idleTime_currentHour = system.tag.read(lineTagPath + '/Line/Shift/Accum/IdleTime/currentHour').value
							idleTime_previousHour = system.tag.read(lineTagPath + '/Line/Shift/Accum/IdleTime/previousHour').value
							
							shiftTime_currentShift = system.tag.read(lineTagPath + '/Line/Shift/Accum/ShiftTime/currentShift').value
							shiftTime_currentHour = system.tag.read(lineTagPath + '/Line/Shift/Accum/ShiftTime/currentHour').value
							shiftTime_previousHour = system.tag.read(lineTagPath + '/Line/Shift/Accum/ShiftTime/previousHour').value
							


							# get the cells within this line
							getCellsResult, cells = shared.ent.assets.db_sql.cells.getAllCellsByLine(line['line_id'], '1')
					
							if getCellsResult:
							
								for cell in cells:
								
									print cell['cell_identifier']
								
									newEventObj['script_output'] += '\n' +  'cell: ' + str(cell['cell_identifier'])
									
									
									cellTagWritePaths = []
									cellTagWriteValues = []
									
									
									
									# establish the tag directory path for this cell
									cellTagPath = baseTagPath + '/' + str(site['site_identifier']) + '/' + str(area['area_identifier']) + '/' + str(line['line_identifier']) + '/' + str(cell['cell_identifier'])

									newEventObj['script_output'] += '\n' +  'cell tag path: ' + cellTagPath

									# for the Ignition tag paths, swap the spaces out with underscores
									cellTagPath = cellTagPath.replace(' ', '_')

									newEventObj['script_output'] += '\n' +  'underscored cell tag path: ' + cellTagPath

									newEventObj['script_output'] += '\n' +  'line id: ' + str(line['line_id'])
								

	
									# make sure the line has an active shift before digging into the individual cells OEE calcs
									if inActiveShiftTime:

										print 'line is in an active shift: ' + str(inActiveShiftTime)
										
										# -------------------------------------------------------------------
										# get the realted cell level properties for this cell
										
										# takt time setpoint
										cellTagWritePaths.append(cellTagPath + '/Cell/OEE/TAKT/takt_timeSetpoint')
										
										getTaktTimeResult = False
										
										if str(shiftCode) == '3':
										
											getTaktTimeResult, taktTimeSetpoint = shared.ent.assets.interface.asset_properties.getAssetPropByAssetId(cell['cell_id'], 'Cell', 'takt_time/setpoint_3rd_shift')
							
										if str(shiftCode) == '1':
										
											getTaktTimeResult, taktTimeSetpoint = shared.ent.assets.interface.asset_properties.getAssetPropByAssetId(cell['cell_id'], 'Cell', 'takt_time/setpoint_1st_shift')
										
										if str(shiftCode) == '2':
										
											getTaktTimeResult, taktTimeSetpoint = shared.ent.assets.interface.asset_properties.getAssetPropByAssetId(cell['cell_id'], 'Cell', 'takt_time/setpoint_2nd_shift')
										
										
										if getTaktTimeResult:
										
											# debug print
											print 'takt time: ' + str(taktTimeSetpoint)
											cellTagWriteValues.append(taktTimeSetpoint)
											
										
										
										
											# -------------------------------------------------------------------
											# baan users assigned to this cell
											getBaanUsersResult, baanUsers = shared.ent.assets.interface.baan_users.getAllBaanUsersByCell(cell['cell_id'])
											
											if getBaanUsersResult:
											
												baanUsers_nameOnly = []
			
												print 'baan users found:'
												for baanUser in baanUsers:
												
													print baanUser['baan_user_name']
													baanUsers_nameOnly.append(baanUser['baan_user_name'])
													
												
												# get actual completions for the shift
												getCompletionsResult, actualCompletions_currentShift = calcActualCompletions(baanUsers_nameOnly, overtimeStartDate, overtimeEndDate)
												
												cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Actual/currentShift')
												cellTagWriteValues.append(actualCompletions_currentShift)
												
												
												# get actual completions for the current hour
												getCompletionsResult, actualCompletions_currentHour = calcActualCompletions(baanUsers_nameOnly, startOfCurrentHour, currentDate)
												
												cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Actual/currentHour')
												cellTagWriteValues.append(actualCompletions_currentHour)
												
												
												# get actual completions for the previous hour
												getCompletionsResult, actualCompletions_previousHour = calcActualCompletions(baanUsers_nameOnly, startOfPreviousHour, startOfCurrentHour)
												
												cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Actual/previousHour')
												cellTagWriteValues.append(actualCompletions_previousHour)
												
												
												
												# get expected completions for the shift
												expectedCompletions_currentShift = calcExpectedCompletions(taktTimeSetpoint, shiftTime_currentShift - idleTime_currentShift)
												
												cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Expected/currentShift')
												cellTagWriteValues.append(expectedCompletions_currentShift)
												
												
												# get expected completions for the current hour
												expectedCompletions_currentHour = calcExpectedCompletions(taktTimeSetpoint, shiftTime_currentHour - idleTime_currentHour)
												
												cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Expected/currentHour')
												cellTagWriteValues.append(expectedCompletions_currentHour)
												
												
												# get expected completions for the previous hour
												expectedCompletions_previousHour = calcExpectedCompletions(taktTimeSetpoint, shiftTime_previousHour - idleTime_previousHour)
												
												cellTagWritePaths.append(cellTagPath + '/Cell/OEE/Accum/Completions_Expected/previousHour')
												cellTagWriteValues.append(expectedCompletions_previousHour)
												
												
												
												# calc the OR for the shift
												ORValue_currentShift = calcOR(actualCompletions_currentShift, expectedCompletions_currentShift)
												
												cellTagWritePaths.append(cellTagPath + '/Cell/OEE/OR/currentShift')
												cellTagWriteValues.append(ORValue_currentShift)
												
												
												# calc the OR for the current hour
												ORValue_currentHour = calcOR(actualCompletions_currentHour, expectedCompletions_currentHour)
												
												cellTagWritePaths.append(cellTagPath + '/Cell/OEE/OR/currentHour')
												cellTagWriteValues.append(ORValue_currentHour)
												
												
												# calc the OR for the previous hour
												ORValue_previousHour = calcOR(actualCompletions_previousHour, expectedCompletions_previousHour)
													
												cellTagWritePaths.append(cellTagPath + '/Cell/OEE/OR/previousHour')
												cellTagWriteValues.append(ORValue_previousHour)
												
												
												# if the script has executed this far with no errors, moev forward with the tag write for this cell
												clearToWriteTags = True

												if clearToWriteTags:

													# write to the tags to update the shift values to each cell
													tagWriteResults = system.tag.writeAll(cellTagWritePaths, cellTagWriteValues)

															
													newEventObj['script_output'] += '\n' +  'all tags write for cell executed'
													# newEventObj['script_output'] += '\n' +  str(tagWriteResults)
													
													for i in range(0, len(tagWriteResults)):
													
														if tagWriteResults[i] != 2:
														
															newEventObj['script_output'] += '\n' +  'there was a problem writing the value of: ' + str(cellTagWriteValues[i]) + ' to the tag: ' + str(cellTagWritePaths[i])
															
															overallScriptResult = False
															
															
													newEventObj['script_output'] += '\n' +  'moving on to next cell'
													# newEventObj['script_output'] += '\n' +  '\n'	
													
																									
												
											else:
											
												print 'failed to get any baan users associated with this cell'
												
										
										else:
										
											print "failed to get the cell's takt time setpoint"


									else:
									
										print 'the line this cell belongs to is not currently in an active shift'



							else:
							
								newEventObj['script_output'] += '\n' +  'no cells were found for line: ' + str(line['line_identifier'])


					else:
					
						newEventObj['script_output'] += '\n' +  'no lines were found for area: ' + str(area['area_identifier'])
						
						
			else:
			
				newEventObj['script_output'] += '\n' +  'no areas were found for site: ' + str(site['site_identifier'])
			
			
	else:

		newEventObj['script_output'] += '\n' +  'no sites were found'
		
		overallScriptResult = False
		
	
	# calculate the elapsed processing time of the script	
	endTime = system.date.now()
	timeElapsed = system.date.millisBetween(startTime, endTime)


	# execution time for diagnostics
	# newEventObj['script_output'] += '\n' +  '\n'
	newEventObj['script_output'] += '\n' +  'Total execution time: ' + str(timeElapsed)
	
	newEventObj['event_duration'] = str(timeElapsed)
	
	
	# determine the overall script result
	if overallScriptResult:
	
			newEventObj['event_result'] = 'SUCCESS'
			
	else:
			
			newEventObj['event_result'] = 'ERROR'
			
			
	# # for now, only log errored executions		
	# if overallScriptResult == False:
	
		# # create the event log record in the database
		# eventInsertResult = shared.ent.event_logging.db_sql.events.insertNewEvent(newEventObj)
		
	# print newEventObj['script_output']
		










def calcActualCompletions(baanUsers, startDate, endDate):

	result = True
	totalCompletions = 0

	for baanUser in baanUsers:

		getCompletionsResult, completions = shared.ent.common.interface.baan_completions.getNumberOfCompletions(baanUser, startDate, endDate)

		if getCompletionsResult:
			
			totalCompletions += completions
			
		else:
		
			print 'there was an issue retrieving completions for baan user: ' + baanUser + ' for the date range : ' + str(startDate) + ' to: ' + str(endDate)
		
			result = False
			completions = 0
			
			break

	return result, totalCompletions
	



def calcExpectedCompletions(taktTimeSetpoint, elapsedProdTime):

	expectedCompletions = 0

	if taktTimeSetpoint > 0:
		
		expectedCompletions = int(elapsedProdTime / float(taktTimeSetpoint))

	return expectedCompletions
	
	
	

def calcOR(expectedCompletions, actualCompletions):

	result = True
	ORValue = 0

	if actualCompletions > 0:
		
		ORValue = float(expectedCompletions) / float(actualCompletions) * 100

	return ORValue
		